//
//  SlideAnimatedTransitioner.swift
//  TVPlayer
//
//  Created by Jérémy Marchand on 29/12/2018.
//  Copyright © 2018 Jérémy Marchand. All rights reserved.
//

import UIKit

private let animatonDuration = 0.3

// MARK: SlideDown
class KSSlideDownAnimatedTransitioner: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return animatonDuration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let secondVCView = transitionContext.view(forKey: .to),
            let secondVC = transitionContext.viewController(forKey: .to) else {
                return
        }
        let firstVCView = transitionContext.containerView
        firstVCView.addSubview(secondVCView)

        secondVCView.frame.origin.y = -secondVC.preferredContentSize.height

        UIView.animate(withDuration: animatonDuration,
                       delay: 0.0,
                       usingSpringWithDamping: 1.0,
                       initialSpringVelocity: 0.0,
                       options: .allowUserInteraction, animations: {
                        secondVCView.frame.origin.y = 0 },
                       completion: { _ in
                        transitionContext.completeTransition(!transitionContext.transitionWasCancelled)})

    }
}
