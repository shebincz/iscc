//
//  SubtitlesViewController.swift
//  TVPlayer
//
//  Created by Jérémy Marchand on 29/12/2018.
//  Copyright © 2018 Jérémy Marchand. All rights reserved.
//

import UIKit
import KSPlayer

class KSSubtitlesViewController: UIViewController {
    weak var player: CustomVideoPlayerView?
    private var trackView: UIView!

    override var preferredFocusEnvironments: [UIFocusEnvironment] {
        return (player?.videoSubtitles != nil) ? super.preferredFocusEnvironments : [trackView]
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        preferredContentSize = CGSize(width: 1920, height: 310)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // TODO: Translate title
        // TODO: enum indentifier name
        if let viewController = segue.destination as? KSSelectorTableViewController {
            if segue.identifier == "list" {
                viewController.collection = player?.videoSubtitles
                viewController.title = "track".localizable
                viewController.emptyText = "No Subtitles".localizable
                trackView = viewController.view

            }
//            else if segue.identifier == "delay" {
//                viewController.collection = player.videoSubtitlesDelay
//                viewController.title = "delay"
//            }
        }
    }
}
