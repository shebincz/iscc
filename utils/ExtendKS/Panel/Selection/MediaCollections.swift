//
//  MediaPlayer+indexed.swift
//  TVPlayer
//
//  Created by Jérémy Marchand on 30/12/2018.
//  Copyright © 2018 Jérémy Marchand. All rights reserved.
//

import Foundation
import AVFoundation
import AudioToolbox
import KSPlayer

struct KSAudioSelectableCollection: KSSelectableCollection {
    private weak var player: CustomVideoPlayerView?
    private let audioTracks: [MediaPlayerTrack]
    private(set) var selectedIndex: Int
    
    func collectionNumberOfRows() -> Int {
        return audioTracks.count
    }
    
    func collection(_ tableView: UITableView, cellForRowAt indexPath:IndexPath) -> KSSelectableTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? KSSelectableTableViewCell,
              indexPath.row < self.audioTracks.count else {
            return KSSelectableTableViewCell()
        }
        let audio = self.audioTracks[indexPath.row]
        cell.checkmarkView.isHidden = !audio.isEnabled
        cell.label.text = audio.language
        cell.update(with: .lightGray)
        return cell
    }
    
    mutating func collection(didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < self.audioTracks.count {
            let selectingTrack = self.audioTracks[indexPath.row]
            self.player?.setAudioTrack(selectingTrack)
            self.selectedIndex = indexPath.row
        }
    }
    
    init(with player: CustomVideoPlayerView) {
        self.player = player
        self.audioTracks = player.getAudioTracks()
        var index = 0
        for i in 0...self.audioTracks.count - 1 {
            if self.audioTracks[i].isEnabled {
                index = i
            }
        }
        self.selectedIndex = index
    }
}

struct KSSubtitleSelectableCollection: KSSelectableCollection {
    private weak var player: CustomVideoPlayerView?
    private let subtitleTracks: [KSPlayer.SubtitleInfo]
    private(set) var selectedIndex: Int
    
    func collectionNumberOfRows() -> Int {
        return subtitleTracks.count
    }
    
    func collection(_ tableView: UITableView, cellForRowAt indexPath:IndexPath) -> KSSelectableTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? KSSelectableTableViewCell,
              indexPath.row < self.subtitleTracks.count else {
            return KSSelectableTableViewCell()
        }
        let srt = self.subtitleTracks[indexPath.row]
        if let current = self.player?.getCurrentSubtitles(),
           current.subtitleID == srt.subtitleID {
            cell.checkmarkView.isHidden = false
        } else {
            cell.checkmarkView.isHidden = true
        }
        
        cell.label.text = srt.name
        cell.update(with: .lightGray)
        return cell
    }
    
    mutating func collection(didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < self.subtitleTracks.count {
            let newSrt = self.subtitleTracks[indexPath.row]
            self.player?.set(srt: newSrt)
            self.selectedIndex = indexPath.row
        }
    }
    
    init(with player: CustomVideoPlayerView) {
        self.player = player
        self.subtitleTracks = player.getSubtitles()
        self.selectedIndex = 0
    }
}

struct KSAudioRoute: KSSelectableCollection {
    private weak var player: CustomVideoPlayerView?
    private(set) var selectedIndex: Int
    private var otuputs: [AVAudioSessionPortDescription]
    
    init(with player: CustomVideoPlayerView) {
        self.player = player
        self.otuputs = AVAudioSession.sharedInstance().currentRoute.outputs
        self.selectedIndex = 0
        
        for output in self.otuputs {
            print(output.portName)
        }
    }
    
    func collectionNumberOfRows() -> Int {
        return AVAudioSession.sharedInstance().currentRoute.outputs.count
    }
    
    func collection(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> KSSelectableTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? KSSelectableTableViewCell,
              indexPath.row < self.otuputs.count else {
            return KSSelectableTableViewCell()
        }
        let audio = self.otuputs[indexPath.row]
        cell.checkmarkView.isHidden = indexPath.row != self.selectedIndex
        cell.label.text = audio.portName

        cell.update(with: .lightGray)
        return cell
    }
    
    func collection(didSelectRowAt indexPath: IndexPath) {
        self.player?.changeAudioRoute()
    }
}

extension CustomVideoPlayerView {
    var equalizer: KSAudioRoute? {
        return KSAudioRoute(with: self)
    }
    var audioTracks: KSAudioSelectableCollection? {
        return KSAudioSelectableCollection(with: self)
    }

    var videoSubtitles: KSSubtitleSelectableCollection? {
        return KSSubtitleSelectableCollection(with: self)
    }
}
