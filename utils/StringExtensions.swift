//
//  StringExtensions.swift
//  StreamCinema.atv
//
//  Created by SCC on 29/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

extension String {
     var localizable: String {
           get {
            let localized = NSLocalizedString(self, comment: "")
            if localized == self {
                print("WARRNING: Missed localization: \"\(self)\"=\"\(localized)\";")
            }
            return localized
           }
    }
    var asURL: URL? {
        return URL(string: self)
    }
    
    var toDate: Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: self)
    }
}
