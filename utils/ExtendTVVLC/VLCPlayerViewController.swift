//
//  VLCPlayerViewController.swift
//  IndexOfTV
//
//  Created by Jérémy Marchand on 24/02/2018.
//  Copyright © 2018 Jérémy Marchand. All rights reserved.
//

import UIKit
import GameController
import TVVLCKit

public enum VLCPlayerAction {
    case didEndPlaying
    case endWithError
    case prepareNextVersion
}

public protocol VLCPlayerViewDelegate: class {
//    func playerView(_ playerView: VLCPlayerViewController, didSave info:SccWatched)
//    func playerView(_ playerView: VLCPlayerViewController, nextMediaContinueAfter media:InfoData ) -> InfoData?
//    func playerView(_ playerView: VLCPlayerViewController, watchedInfoFor data:InfoData ) -> SccWatched?
    
    func playerView(_ playerView: VLCPlayerViewController, didSave info:SccWatched)
    func playerView(_ playerView: VLCPlayerViewController, prepare action:VLCPlayerAction)
    func playerView(_ playerView: VLCPlayerViewController, playerDidTimeChanged: TimeInterval)
    func playerViewWillDisapper(_ playerView: VLCPlayerViewController)
    func playerViewLastAudio(_ playerView: VLCPlayerViewController) -> String
}

public class VLCPlayerViewController: UIViewController {
    public static func instantiate(media: VLCMedia) -> VLCPlayerViewController {
        let player = VLCMediaPlayer()
        player.media = media
        return instantiate(player: player)
    }

    public static func instantiate(player: VLCMediaPlayer) -> VLCPlayerViewController {
        let storyboard = UIStoryboard(name: "TVVLCPlayer", bundle: Bundle(for: VLCPlayerViewController.self))
        guard let controller = storyboard.instantiateInitialViewController() as? VLCPlayerViewController else {
            fatalError()
        }
        controller.player = player
        return controller
    }
    
    public weak var playerDelegate:VLCPlayerViewDelegate?

    @IBOutlet var videoView: UIView!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var remainingLabel: UILabel!
    @IBOutlet weak var transportBar: ProgressBar!
    @IBOutlet weak var scrubbingLabel: UILabel!
    @IBOutlet weak var playbackControlView: GradientView!
    @IBOutlet weak var rightActionIndicator: UIImageView!

    @IBOutlet weak var positionConstraint: NSLayoutConstraint!
    @IBOutlet weak var bufferingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var openingIndicator: UIActivityIndicatorView!

    @IBOutlet var actionGesture: LongPressGestureRecogniser!
    @IBOutlet var playPauseGesture: UITapGestureRecognizer!
    @IBOutlet var cancelGesture: UITapGestureRecognizer!

    @IBOutlet var scrubbingPositionController: ScrubbingPositionController!
    @IBOutlet var remoteActionPositionController: RemoteActionPositionController!

    @IBOutlet var pauseImageView: UIImageView!
    @IBOutlet weak var continueView: ContinueView!
    /// Set the player.
    /// The player should be set before the view is loaded.
    private var timer: Timer?
    public var watched: SccWatched?
    private var mediaInfo: InfoData?
    public var panelInfo: InfoPanelData?

    
    public var player: VLCMediaPlayer = VLCMediaPlayer() {
        didSet {
            if isViewLoaded {
                fatalError("The VLCPlayer player should be set before the view is loaded.")
            }
            isOpening = true
            player.play()
        }
    }

    private var positionController: PositionController? {
        didSet {
            guard positionController !== oldValue else {
                return
            }
            oldValue?.isEnabled = false
            positionController?.isEnabled = true
        }
    }

    private var isOpening: Bool = false {
        didSet {
            guard self.viewIfLoaded != nil else {
                return
            }

            guard isOpening != oldValue else {
                return
            }

            if isOpening {
                openingIndicator.startAnimating()
            } else {
                openingIndicator.stopAnimating()
            }
            self.setUpPositionController()
        }
    }

    private var isBuffering: Bool = false {
        didSet {
            guard isBuffering != oldValue else {
                return
            }

            if isBuffering {
                bufferingIndicator.startAnimating()
            } else {
                bufferingIndicator.stopAnimating()

            }
            rightActionIndicator.isHidden = isBuffering
        }
    }

    private var lastSelectedPanelTabIndex: Int = 0
    private var displayedPanelViewController: UIViewController?
    private var isPanelDisplayed: Bool {
        return displayedPanelViewController != nil
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        guard self.player.media != nil else {
            fatalError("The VLCPlayer player should contain a media before presenting player.")
        }

        self.player.delegate = self
        player.drawable = videoView
        playbackControlView.isHidden = true
        openingIndicator.startAnimating()

        let font = UIFont.monospacedDigitSystemFont(ofSize: 30, weight: UIFont.Weight.medium)
        remainingLabel.font = font
        positionLabel.font = font
        scrubbingLabel.font = font

        scrubbingPositionController.player = player

        setUpGestures()
        setUpPositionController()
        updateViews(with: player.time)
        animateIndicatorsIfNecessary()
    }

    deinit {
        self.updateWatched()
        player.stop()
        Log.write("VLCPlayerViewController deinit")
    }

    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: IB Actions
    @IBAction func click(_ sender: LongPressGestureRecogniser) {
        positionController?.click(sender)
    }
    @IBAction func playOrPause(_ sender: Any) {
        positionController?.playOrPause(sender)
    }

    @IBAction func showPanel(_ sender: Any) {
        self.performSegue(withIdentifier: "panel", sender: sender)
        setUpPositionController()
        hideControl()
    }

    // MARK: Control
    var playbackControlHideTimer: Timer?
    public func showPlaybackControl() {
        playbackControlHideTimer?.invalidate()
        if player.state != .paused {
            autoHideControl()
        }

        guard self.playbackControlView.isHidden else {
            return
        }
        self.cancelGesture.isEnabled = true

        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.playbackControlView.isHidden = false
        })

    }

    private func autoHideControl() {
        playbackControlHideTimer?.invalidate()
        playbackControlHideTimer = Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { _ in
            self.hideControl()
        }
    }

    private func hideControl() {
        playbackControlHideTimer?.invalidate()
        self.cancelGesture.isEnabled = false

        guard !self.playbackControlView.isHidden else {
            return
        }

        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.playbackControlView.isHidden = true
        })
    }

    @IBAction func cancel(_ sender: Any) {
        player.play()
        hideControl()
    }
    
    private func waitToBuuffer() {
        self.pauseImageView.isHidden = true
        self.bufferingIndicator.startAnimating()
        self.player.pause()
        let deadlineTime = DispatchTime.now() + .milliseconds(100)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            self.player.play()
            self.openingIndicator.stopAnimating()
            self.bufferingIndicator.stopAnimating()
        }
    }
}

//MARK: - support methods

extension VLCPlayerViewController {
    public func play(watched info: SccWatched, mediaInfo: InfoData?, isContinue: Bool) {
        self.updateWatched()
        self.mediaInfo = mediaInfo
        self.watched = info
        self.openingIndicator.startAnimating()
        self.player.play()
        self.player.delegate = self
        if isContinue {
            let deadlineTime = DispatchTime.now() + .milliseconds(300)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                self.player.jumpForward(Int32(info.time/1000))
                self.waitToBuuffer()
            }
        } else {
            self.waitToBuuffer()
        }
    }
    
    private func updateWatched() {
        if var wathced = self.watched {
            if let totalMS = self.player.totalTime?.value?.doubleValue {
                let totalS = totalMS / 1000
                let tenPercentOfTotal = totalS * 0.1
                if (tenPercentOfTotal + wathced.time) >= totalS {
                    wathced.isSeen = true
                    wathced.time = 0
                }
            }
            
            self.playerDelegate?.playerView(self, didSave: wathced)
        }
    }
}

// MARK: - Update views
extension VLCPlayerViewController {
    fileprivate func updateViews(with time: VLCTime) {
        positionLabel.text = time.stringValue
        updateRemainingLabel(with: time)
        guard let totalTime = player.totalTime,
            let value = time.value?.doubleValue,
            let totalValue = totalTime.value?.doubleValue else {
            remainingLabel.isHidden = true
            positionConstraint.constant = transportBar.bounds.width / 2
            return
        }

        positionConstraint.constant = round(CGFloat(value / totalValue) * transportBar.bounds.width)
        remainingLabel.isHidden = positionConstraint.constant + positionLabel.frame.width > remainingLabel.frame.minX - 60
    }

    fileprivate func updateRemainingLabel(with time: VLCTime) {
        guard let totalTime = player.totalTime, totalTime.value != nil else {
            return
        }
        remainingLabel.text = (totalTime - time).stringValue
    }

    fileprivate func setUpPositionController() {
        guard player.isSeekable && !isOpening && !isPanelDisplayed else {
            positionController = nil
            return
        }

        if player.state == .paused {
            scrubbingPositionController.selectedTime = player.time
            positionController = scrubbingPositionController
        } else {
            positionController = remoteActionPositionController
        }
    }

    fileprivate func animateIndicatorsIfNecessary() {
        if player.state == .opening {
            openingIndicator.startAnimating()
        }
        if player.state == .buffering && player.isPlaying {
            isBuffering = true
        }
    }

    fileprivate func setUpGestures() {
        playPauseGesture.isEnabled = player.state != .opening && player.state != .stopped
        actionGesture.isEnabled = playPauseGesture.isEnabled
    }

    fileprivate func handlePlaybackControlVisibility() {
        if player.state == .paused {
            showPlaybackControl()
        } else {
            autoHideControl()
        }
    }
    
    private func timerChanged() {
        isOpening = false
        isBuffering = false
        updateViews(with: player.time)
        
        guard let time = self.player.time.value?.doubleValue else { return }
        self.watched?.time = time/1000
    }
    
    // MARK: - nexEpisodeView
    public func isHiddenCountDownView() -> Bool {
        return self.continueView.isHidden
    }
    
    public func hideCountDownView(_ isHiden: Bool) {
        self.continueView.isHidden = isHiden
    }

    public func updateNextVideo(countDown value: Float) {
        self.continueView.set(countDown: value)
    }

    public func setNextVideo(title: String?) {
        self.continueView.set(title: title)
    }

    public func setNextVideo(poster url: String?) {
        if let urlString = url {
            self.continueView.set(poster: urlString)
        }
    }
}

// MARK: - VLC delegate
extension VLCPlayerViewController: VLCMediaPlayerDelegate {
    public func mediaPlayerStateChanged(_ aNotification: Notification!) {
//        if self.timer == nil {
//            self.timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { [weak self] _ in
//                guard let self = self else { return }
//                self.timerChanged()
//            }
//        }
        setUpGestures()
        setUpPositionController()
        animateIndicatorsIfNecessary()
        handlePlaybackControlVisibility()

        if player.state == .error {
            self.playerDelegate?.playerView(self, prepare: .endWithError)
        }
        
        if player.state == .ended || player.state == .error || player.state == .paused {
            self.updateWatched()
        }

        if player.state == .paused, self.isOpening == false {
             self.pauseImageView.isHidden = false
         } else {
             self.pauseImageView.isHidden = true
         }
    }
    
    public func mediaPlayerTimeChanged(_ aNotification: Notification!) {
        self.timerChanged()

        self.playerDelegate?.playerView(self, playerDidTimeChanged: self.player.time.value?.doubleValue ?? 0)
    }
    
    public func mediaPlayerTitleChanged(_ aNotification: Notification!) {
        
    }
    
    public func mediaPlayerChapterChanged(_ aNotification: Notification!) {
        
    }
    
    public func mediaPlayerSnapshot(_ aNotification: Notification!) {
        
    }
    
    public func mediaPlayerStartedRecording(_ player: VLCMediaPlayer!) {
        
    }
    
    public func mediaPlayer(_ player: VLCMediaPlayer!, recordingStoppedAtPath path: String!) {
        
    }
}

// MARK: - Scrubbling Delegate
extension VLCPlayerViewController: ScrubbingPositionControllerDelegate {
    func scrubbingPositionController(_ : ScrubbingPositionController, didScrubToTime time: VLCTime) {
        updateRemainingLabel(with: time)
    }

    func scrubbingPositionController(_ : ScrubbingPositionController, didSelectTime time: VLCTime) {
        player.time = time
        updateViews(with: time) // ?
        player.play()
    }
}

// MARK: - Remote Action Delegate
extension VLCPlayerViewController: RemoteActionPositionControllerDelegate {
    func remoteActionPositionControllerDidDetectTouch(_ : RemoteActionPositionController) {
        showPlaybackControl()
    }
    func remoteActionPositionController(_ : RemoteActionPositionController, didSelectAction action: RemoteActionPositionController.Action) {
        showPlaybackControl()

        switch action {
        case .fastForward:
            player.fastForward(atRate: 20.0)
        case .rewind:
            player.rewind(atRate: 20)
        case .jumpForward:
            player.jumpForward(30)
        case .jumpBackward:
            player.jumpBackward(30)
        case .pause:
            player.pause()
        case .reset:
            player.fastForward(atRate: 1.0)
        }
    }
}

// MARK: - Gesture
extension VLCPlayerViewController: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

// MARK: - Panel
extension VLCPlayerViewController {
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? PanelViewController {
            destination.selectedIndex = lastSelectedPanelTabIndex
            destination.player = player
            destination.transitioningDelegate = self
            destination.delegate = self
            displayedPanelViewController = destination
            if let panel = self.displayedPanelViewController as? PanelViewController,
                let mediaInfo = self.panelInfo {
                panel.mediaInfo = mediaInfo
            }
        }
    }
}

extension VLCPlayerViewController: UIViewControllerTransitioningDelegate {
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return presented is PanelViewController ? SlideDownAnimatedTransitioner() : nil
    }
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return dismissed is PanelViewController ? SlideUpAnimatedTransitioner() : nil
    }
}

extension VLCPlayerViewController: PanelViewControllerDelegate {
    func panelViewController(_ panelViewController: PanelViewController, didSelectTabAtIndex index: Int) {
        lastSelectedPanelTabIndex = index
    }

    func panelViewControllerDidDismiss(_ panelViewController: PanelViewController) {
        displayedPanelViewController = nil
        setUpPositionController()
        handlePlaybackControlVisibility()
    }
}
