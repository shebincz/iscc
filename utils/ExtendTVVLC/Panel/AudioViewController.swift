//
//  AudioViewController.swift
//  TVVLCPlayer
//
//  Created by Jérémy Marchand on 29/12/2018.
//  Copyright © 2018 Jérémy Marchand. All rights reserved.
//

import UIKit
import TVVLCKit

class AudioViewController: UIViewController {
    var player: VLCMediaPlayer!
    private var equalizerView: UIView!

    override var preferredFocusEnvironments: [UIFocusEnvironment] {
        return [equalizerView]
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        preferredContentSize = CGSize(width: 1920, height: 310)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // TODO: Translate title
        // TODO: enum indentifier name
        if let viewController = segue.destination as? SelectorTableViewController {
            if segue.identifier == "track" {
                viewController.collection = player.audioTracks
                viewController.title = "track".localizable
                viewController.emptyText = "No Audio".localizable
                setNeedsFocusUpdate()
            } else if segue.identifier == "equalizer" {
                viewController.collection = player.equalizer
                viewController.title = "sound".localizable
                equalizerView = viewController.view
            } else if segue.identifier == "delay" {
                viewController.collection = player.audioDelay
                viewController.title = "delay".localizable
            }
        }
    }
}
