//
//  SelectableTableViewCell.swift
//  TVVLCPlayer
//
//  Created by Jérémy Marchand on 30/12/2018.
//  Copyright © 2018 Jérémy Marchand. All rights reserved.
//

import UIKit

class SelectableTableViewCell: UITableViewCell {
    @IBOutlet var label: UILabel!
    @IBOutlet var checkmarkView: UIImageView!
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        super.didUpdateFocus(in: context, with: coordinator)
        if context.nextFocusedView === self {
            coordinator.addCoordinatedAnimations({
                self.contentView.backgroundColor = UIColor.darkGray
                self.contentView.layer.cornerRadius = self.contentView.frame.size.height/2
            }, completion: nil)
        }
        else {
            coordinator.addCoordinatedAnimations({
                self.contentView.backgroundColor = UIColor.clear
            }, completion: nil)
        }
    }
}
