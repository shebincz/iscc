//
//  VLCCoordinator.swift
//  StreamCinema.atv
//
//  Created by SCC on 05/08/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import TVVLCKit

protocol VLCCoordinatorDelegate: class {
    func numberOfVideos(_ coordinator:VLCCoordinator) -> Int
    func coordinator(_ coordinator:VLCCoordinator, data atIndex:Int) -> InfoData
    func coordinator(_ coordinator:VLCCoordinator, watched forData:InfoData) -> SccWatched?
    func coordinator(_ coordinator: VLCCoordinator, infoPanel forData: InfoData) -> InfoPanelData
    func coordinator(_ coordinator: VLCCoordinator, update watched: SccWatched)
}

final class VLCCoordinator {
    private let countDownRepet:Int = 20
    public weak var delegate:VLCCoordinatorDelegate?
    
    private var currentIndex: Int = 0
    private var count:Int = 0
    private var movieData: InfoData?
    private var infoPanel: InfoPanelData?
    private var sender:UIViewController?
    private weak var playerVC:VLCPlayerViewController?
    private var nextEpisodneControlTimer: Timer?
    private var continueAudioInLang: String?
    private var continueTitleInLang: String?
    private var watchedData: SccWatched?
    private var osSubtitles: [OSDownload] = [] {
        didSet {
            for item in self.osSubtitles {
                if let link = item.link,
                   let url = URL(string: link){
                    self.playerVC?.player.addPlaybackSlave(url, type: .subtitle, enforce: false)
                }
            }
        }
    }
    private var isContrinue: Bool = false
    
    public func set(_ sender: UIViewController, startAt index: Int, isContrinue:Bool) {
        self.currentIndex = index
        self.sender = sender
        self.isContrinue = isContrinue
        if self.prepareData() {
            self.showStreamList()
        }
    }
    
    public func playLiveStream(_ link:URL, sender:UIViewController, title:String? = nil, logo: String? = nil, desc: String? = nil) {
        if self.playerVC == nil {
            self.playerVC = VLCPlayerViewController.instantiate(media: VLCMedia(url: link))
            playerVC?.playerDelegate = self
            self.sender?.present(playerVC!, animated: false, completion: nil)
        } else {
            self.playerVC?.player.media = VLCMedia(url: link)
        }
        self.playerVC?.panelInfo = InfoPanelData(title: title ?? "", desc: desc ?? "" , posterURL: nil, bannerURL: nil, clearLogoURL: logo)
        self.playerVC?.player.play()
        sender.present(self.playerVC!, animated: true, completion: nil)
    }
    
    private func prepareData() -> Bool {
        guard let count = self.delegate?.numberOfVideos(self) else {
            Log.write("prepareData delegate not exists")
            return false
        }
        self.setContinuteLang()
        self.count = count
        Log.write("prepareData count:\(count) currentIndex:\(self.currentIndex)")
        if self.count > 0,
            self.currentIndex < self.count {
            self.getData(at: self.currentIndex)
            return true
        } else {
            self.movieData = nil
            self.watchedData = nil
            return false
        }
    }
    
    private func setContinuteLang() {
        if let audioIndex = self.playerVC?.player.currentAudioTrackIndex,
           let names = self.playerVC?.player.audioTrackNames,
           let currentName = names[Int(audioIndex)] as? String{
            self.continueAudioInLang = currentName
        }
        
        if let subtitleIndex = self.playerVC?.player.currentVideoSubTitleIndex,
           subtitleIndex > 0,
           let names = self.playerVC?.player.videoSubTitlesNames,
           let curentSrt = names[Int(subtitleIndex)] as? String {
            self.continueTitleInLang = curentSrt
        }
    }
    
    private func getData(at index:Int) {
        guard let delegate = self.delegate else { return }
        let data = delegate.coordinator(self, data: index)
        let infoPanel = delegate.coordinator(self, infoPanel: data)
        guard let watched = delegate.coordinator(self, watched: data) else { return }
        Log.write("getData exist")
        self.movieData = data
        self.watchedData = watched
        print(self.watchedData as Any)
        self.infoPanel = infoPanel
    }
    
    public func showStreamList() {
        guard let data = self.movieData else { return }
        Log.write("showStreamList for originaltitle: \(data.source?.infoLabels?.originaltitle ?? "none")")
        self.getStreams(from: data) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let streams):
                self.showAlert(for: streams)
            case .failure(let error):
                error.handleError(on: self.sender)
            }
        }
    }
    
    private func showAlert(for streams:VideoStreams) {
        Log.write("showAlert streamsCount:\(streams.data.count)")
        let alert = UIAlertController(title: "choose-the-stream".localizable, message: nil, preferredStyle: .alert)
        let selectedStream = streams.isAutoSelectVideo()
        self.prepereSubtitles()
        for stream in streams.getSortedData() {
            let text = stream.getStreamInfo(isSselected: selectedStream?.id == stream.id)
            let action = UIAlertAction(title: text, style: .default) {[weak self] _ in
                guard let self = self else { return }
                Log.write("showAlert selectedStream:\(stream.ident ?? "")")
                stream.getLink { [weak self] result in
                    guard let self = self else { return }
                    self.prepareLinkFrom(result)
                }
            }
            alert.addAction(action)
        }
        alert.addAction(UIAlertAction(title: "button_cancel".localizable, style: .cancel, handler: nil))
        self.sender?.show(alert, sender: self)
    }
    
    private func prepareLinkFrom(_ result: (Result<WSStreamModel, RequestErrors>)) {
        Log.write("prepareLinkFrom")
        switch result {
        case .failure(let error):
            error.handleError(on: self.sender)
        case .success(let data):
            if let link = data.link,
                let videoURL = URL(string: link) {
                self.play(link: videoURL)
            }
        }
        self.invalidateTimer()
    }
    
    private func play(link: URL) {
        Log.write("play(link: \(link)")
        self.playerVC?.hideCountDownView(true)
        if self.playerVC == nil {
            self.playerVC = VLCPlayerViewController.instantiate(media: VLCMedia(url: link))
            playerVC?.playerDelegate = self
            self.sender?.present(playerVC!, animated: false, completion: nil)
        } else {
            self.playerVC?.player.media = VLCMedia(url: link)
        }

        guard let data = self.movieData,
            let watched = self.watchedData,
            let infoPanel = self.infoPanel else { Log.write("play data NOTExist"); return }
        Log.write("play dataExist")
    
        self.playerVC?.play(watched: watched, mediaInfo: data, isContinue: false)
        self.playerVC?.panelInfo = infoPanel
        self.isContrinue = false

        guard let delegate = UIApplication.shared.delegate as? AppDelegate,
                let mediaId = data.id else { return }
        delegate.appData.scService.setAsPlayed(mediaID: mediaId)
    }
    
    private func getStreams(from data:InfoData, completion: @escaping (Result<VideoStreams, RequestErrors>) -> Void) {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate,
            let movieID = data.id else {
                completion(.failure(.noDataForRequest))
                return
        }
        delegate.appData.scService.getStreams(for: movieID, completion: completion)
    }
    
    private func playNext(_ data: InfoData) {
        Log.write("playNext dataExists")
        self.prepereSubtitles()
        self.invalidateTimer()
        self.getStreams(from: data) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let streams):
                Log.write("playNext getStreams result success")
                let stream = streams.getVideoStream(audioLang: AudioLanguage(audioString:self.continueAudioInLang))
                stream?.getLink(completion: { [weak self] result in
                    guard let self = self else { return }
                    self.prepareLinkFrom(result)
                })
            case .failure(let error):
                Log.write("playNext getStreams result failure")
                self.dismiss(error)
            }
        }
    }
    
    private func dismiss(_ withError: RequestErrors? = nil) {
        self.playerVC?.dismiss(animated: true, completion: { [weak self] in
            guard let self = self else { return }
            self.playerVC?.player.stop()
            withError?.handleError(on: self.sender)
        })
    }
    
    private func invalidateTimer() {
        self.nextEpisodneControlTimer?.invalidate()
        self.nextEpisodneControlTimer = nil
    }
    
    private func prepereSubtitles() {
        if LoginManager.osTokenHash != nil {
            self.osSubtitles.removeAll()
            guard let delegate = UIApplication.shared.delegate as? AppDelegate,
                  let imdb = self.movieData?.source?.services?.imdb else { return }
            delegate.appData.osService.find(imdbid: imdb, lang: ["sk","cs","en"]) { result in
                switch result {
                case .success(let data):
                    self.osSubtitles = data
                case .failure(let error):
                    error.handleError(on: self.playerVC)
                }
            }
        }
    }
    
// MARK: - nextEpisode
    private var isTimeToShowNexEpisode:Bool {
        get {
            if let time = self.playerVC?.player.time.value?.intValue,
               time > 0,
               let total = self.playerVC?.player.totalTime?.value?.intValue,
               (self.countDownRepet + time/1000) >= total/1000 {
                Log.write("isTimeToShowNexEpisode")
                return true
            }
            return false
        }
    }
    
    private func showNextEpisodeView() {
        if self.nextEpisodneControlTimer != nil {
            self.invalidateTimer()
        }
        var repeatCount = self.countDownRepet
        self.playerVC?.hideCountDownView(false)
        self.playerVC?.updateNextVideo(countDown: Float(repeatCount)/20)
        self.playerVC?.setNextVideo(title: self.infoPanel?.title)
        self.playerVC?.setNextVideo(poster: self.infoPanel?.posterURL)
        
        self.nextEpisodneControlTimer = .scheduledTimer(withTimeInterval: 1,
                                                        repeats: true,
                                                        block: { [weak self] _ in
                                                            guard let self = self else { return }
                                                            repeatCount = repeatCount - 1
                                                            self.updateCountDownView(repeatCount)
        })
    }
    
    private func updateCountDownView(_ count:Int) {
        self.playerVC?.updateNextVideo(countDown: Float(count)/20)
        
        if count == 1 {
            self.playerVC?.hideCountDownView(true)
            self.invalidateTimer()
            Log.write("showNextEpisodeView hideCountDownView")
        } else if count == 2 {
            self.currentIndex += 1
            if !self.prepareData() {
                self.dismiss()
                Log.write("showNextEpisodeView dismiss")
            } else {
                guard let data = self.movieData else { return }
                self.playNext(data)
                Log.write("showNextEpisodeView playNext")
            }
        }
    }
}

//extension VLCCoordinator: VLCPlayerViewDelegate {
//    func playerView(_ playerView: VLCPlayerViewController, didSave info: SccWatched) {
//        SccWatchedData.save(new: info)
//        self.delegate?.coordinator(self, update: info)
//        Log.write("playerView(_ playerView: PlayerViewController, didSave info: Watched)")
//    }
//
//
//}

extension VLCCoordinator: VLCPlayerViewDelegate {
    func playerView(_ playerView: VLCPlayerViewController, prepare action: VLCPlayerAction) {
        Log.write("playerView(_ playerView: VLCPlayerViewController, prepare action: VLCPlayerAction)")
        switch action {
        case .endWithError:
            self.dismiss()
            self.invalidateTimer()
        case .didEndPlaying:
            self.dismiss()
        case .prepareNextVersion:
            break
        }
    }
    
    func playerViewWillDisapper(_ playerView: VLCPlayerViewController) {
        self.invalidateTimer()
    }
    
    func playerView(_ playerView: VLCPlayerViewController, didSave info: SccWatched) {
        SccWatchedData.save(new: info)
        self.delegate?.coordinator(self, update: info)
        Log.write("playerView(_ playerView: PlayerViewController, didSave info: Watched)")
    }
    
    func playerView(_ playerView: VLCPlayerViewController, playerDidTimeChanged: TimeInterval) {
         Log.write("playerView(_ playerView: PlayerViewController, playerDidTimeChanged: MediaPlayer)")
        if playerView.player.state != .error,
           playerView.isHiddenCountDownView(),
           self.isTimeToShowNexEpisode {
            self.showNextEpisodeView()
        }
    }
    
    func playerViewLastAudio(_ playerView: VLCPlayerViewController) -> String {
        if let lang = self.continueAudioInLang {
            return lang
        }
        
        if let systemLangiso639 = Locale.current.iso639_2LanguageCode {
            return systemLangiso639
        }
        
        return "eng"
    }
}
