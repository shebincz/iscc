//
//  SpeedTest.swift
//  StreamCinema.atv
//
//  Created by SCC on 29/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

class SpeedTest: NSObject, URLSessionDelegate, URLSessionDataDelegate {
    public var lastSpeed: Double {
        get {
            return UserDefaults.standard.double(forKey: "NetworkSpeed")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "NetworkSpeed")
            UserDefaults.standard.synchronize()
        }
    }
    
    public var lastSpeedString: String {
        get {
            return String(format: (lastSpeed > 5) ? "%.0f Mbps" : "%.1f Mbps", lastSpeed)
        }
    }

    typealias speedTestCompletionHandler = (_ megabytesPerSecond: Double? , _ error: Error?) -> Void

    private var speedTestCompletionBlock : speedTestCompletionHandler?

    private var startTime: CFAbsoluteTime!
    private var stopTime: CFAbsoluteTime!
    private var bytesReceived: Int!

    func testSpeed() {
      guard let token = LoginManager.tokenHash,
        let uuid = UIDevice.current.identifierForVendor else { return }
       WSFile.getFile(wsToken: token, ident: "Pv0gnYZvo6", uuid: uuid) { [weak self] result in
        guard let self = self else { return }
            switch result {
            case .failure(_):
                break //TODO: need handle
            case .success(let data):
                if let link = data.link, let url = URL(string: link) {
                    self.checkForSpeedTest(url: url)
                }
        }
        }
    }

    private func checkForSpeedTest(url: URL) {
        testDownloadSpeedWithTimout(url: url,timeout: 5.0) { (speed, error) in
            if let speed = speed {
                self.lastSpeed = speed
                print("Download Speed:", speed)
            }
            
            if let error = error {
                print("Speed Test Error:", error)
            }
            
        }

    }

    private func testDownloadSpeedWithTimout(url: URL, timeout: TimeInterval, withCompletionBlock: @escaping speedTestCompletionHandler) {
        startTime = CFAbsoluteTimeGetCurrent()
        stopTime = startTime
        bytesReceived = 0

        speedTestCompletionBlock = withCompletionBlock

        let configuration = URLSessionConfiguration.ephemeral
        configuration.timeoutIntervalForResource = timeout
        let session = URLSession.init(configuration: configuration, delegate: self, delegateQueue: nil)
        session.dataTask(with: url).resume()
    }

    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        bytesReceived! += data.count
        stopTime = CFAbsoluteTimeGetCurrent()
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        let elapsed = stopTime - startTime
        if let aTempError = error as NSError?, aTempError.domain != NSURLErrorDomain && aTempError.code != NSURLErrorTimedOut && elapsed == 0  {
            speedTestCompletionBlock?(nil, error)
            return
        }
        var speed = elapsed != 0 ? Double(bytesReceived) / elapsed : -1 // Bytes/s
        speed = speed * 8 / 1024.0 / 1024.0 //convert to Mbit/s
        speedTestCompletionBlock?(speed, nil)
    }
}
