//
//  SubtitlesSettings.swift
//  StreamCinema.atv
//
//  Created by Jakub Cizek on 21/08/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

enum SubtitleColor: Int {
    case white
    case yellow
    case blue
    
    var color: UIColor {
        switch self {
        case .white:
            return .white
        case .yellow:
            return .yellow
        case .blue:
            return .blue
        }
    }
}

enum SubtitleSize: Int {
    case small = 36
    case normal = 40
    case large = 44
    
    var description: String {
        switch self {
        case .small:
            return "subtitles-font-size-small".localizable
        case .normal:
            return "subtitles-font-size-normal".localizable
        case .large:
            return "subtitles-font-size-large".localizable
        }
    }
}

struct SubtitlesSettings {
    private static let subtitleSizeKey = "kSubtitlesSize"
    private static let subtitleColorKey = "kSubtitlesColor"
    private static let SubtitlesOptions = "--freetype-rel-fontsize="
    
    private init() { }
    
    static func getSubtitlesSizeOption() -> String {
        let savedSize = SubtitleSize(rawValue: UserDefaults.standard.integer(forKey: SubtitlesSettings.subtitleSizeKey)) ?? .normal
        return SubtitlesOptions + String(savedSize.rawValue)
    }
    
    static var subtitlesSize: SubtitleSize {
        get {
            return SubtitleSize(rawValue: UserDefaults.standard.integer(forKey: SubtitlesSettings.subtitleSizeKey)) ?? .normal
        }
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: SubtitlesSettings.subtitleSizeKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    static var subtitlesColor: SubtitleColor {
        get {
            return SubtitleColor(rawValue: UserDefaults.standard.integer(forKey: SubtitlesSettings.subtitleColorKey)) ?? .yellow
        }
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: SubtitlesSettings.subtitleColorKey)
            UserDefaults.standard.synchronize()
        }
    }
}
