//
//  SyncTrakt.swift
//  StreamCinema.atv
//
//  Created by SCC on 04/12/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import TraktKit



final class SyncTrakt {
    var allObjects:[TraktKit.TraktHistoryItem] = []
    var movieIDs:[Int:Date] = [:]
    var showIDs:[Int:Date] = [:]
    
    private var completition:((Bool) -> Void)?
    
    func start(completition:@escaping ((Bool) -> Void)) {
        self.completition = completition
        self.startFetching()
    }
    
    func startFetching(_ page:TraktKit.Pagination? = nil) {
        TraktManager.sharedManager.getHistory(pagination:page) { traktObjects in
            switch traktObjects {
            case .success(objects: let objects, currentPage: let currentPage, limit: let limit):
                self.allObjects.append(contentsOf: objects)
                
                if limit >= currentPage {
                    let page = TraktKit.Pagination(page: currentPage + 1, limit: limit)
                    self.startFetching(page)
                } else {
                    self.finischFetching()
                }
            case .error(error: let error):
                if let completition = self.completition {
                    completition(false)
                }
                print(error)
            }
        }
    }
    
    func finischFetching() {
        for item in self.allObjects {
            if let movie = item.movie {
                movieIDs[movie.ids.trakt] = item.watchedAt
            }
            else if let tvShow = item.show {
                showIDs[tvShow.ids.trakt] = item.watchedAt
            }
//            else if let season = item.season {
//
//            }
//            else if let episode = item.episode {
//                episodeIDs.append(episode.ids.trakt)
//            }
        }
        
        self.syncMovies(movieIDs: movieIDs)
        self.syncMovies(movieIDs: showIDs, type: "tvshow")
        self.allObjects.removeAll()
    }
    
    private func syncMovies(movieIDs:[Int:Date], type:String = "movie") {
        let traktIDs = movieIDs.map { (key: Int, value: Date) -> String in
            return "\(type):\(key)"
        }
        DispatchQueue.main.async {
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                appDelegate.appData.scService.getMovies(traktID: traktIDs, page: 1) { [weak self] result in
                    guard let self = self else { return }
                    switch result {
                    case .success(let filterResult):
                        self.updateWatchedData(filterResult, movieIDs: movieIDs, type: type)
                    case .failure(_):
                        break
                    }
                }
            }
        }
    }
    
    private func updateWatchedData(_ result: FilterResult, movieIDs:[Int:Date], type:String = "movie") {
        for movie in (result.data ?? []) {
            if let traktString = movie.source?.services?.trakt,
               let traktID = Int(traktString),
               let dateSeen = movieIDs[traktID] {
                let movieWatched = movie.getMovieWatched(isSeen: true, timeSeen: dateSeen, type: type)
                SccWatchedData.save(new: movieWatched, isWithoutUpdateTrakt: true)
            }
        }
        self.clearData(movieIDs)
    }
    
    private func clearData(_ movieIDs:[Int:Date]) {
        if self.movieIDs == movieIDs {
            self.movieIDs.removeAll()
        }
        if self.showIDs ==  movieIDs {
            self.showIDs.removeAll()
        }
        
        if self.movieIDs.count == 0,
           self.showIDs.count == 0,
           let completition = self.completition {
            completition(true)
            self.completition = nil
        }
    }
}
