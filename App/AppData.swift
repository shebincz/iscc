//
//  AppData.swift
//  StreamCinema.atv
//
//  Created by SCC on 29/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

final class AppData {
    private(set) var epg:[String:[TVProgram]]?
    init() {
        self.downloadEPG()
    }
    
    public let scService: SC2Service = SC2Service()
    public let wsService: WSService = WSService()
    public let osService: OSService = OSService()
    public let isccService: ServiceISCC = ServiceISCC()
    public let gitLabService: ServiceGitLab = ServiceGitLab()
    
    public var traktSync: SyncTrakt = SyncTrakt()
    public var speed: SpeedTest = SpeedTest()
    public var token: WSTokenData?
    public var osLogin: OSLoginResult?
    
    public var loginManager: LoginManager = LoginManager()
    
    
    private func downloadEPG() {
        self.isccService.getEPG { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let model):
                var epgData:[String:[TVProgram]] = [:]
                for channel in model.getChannels() {
                    let program = model.getPrograms(channel: channel)
                    epgData[channel.id] = program
                }
                self.epg = epgData
            case .failure(let error):
                print(error)
            }
        }
    }
}
