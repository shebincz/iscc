//
//  AppTabsModel.swift
//  StreamCinema.atv
//
//  Created by SCC on 29/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

enum AppTabs: String {
    case movies
    case tvShows
    case search
    case trakt
    case fullTextSearch
    case tvProgram
    case settings
    case tv
    
    public var description: String {
        switch self {
        case .movies: return "movies".localizable
        case .tvShows: return "series".localizable
        case .fullTextSearch: return "search".localizable
        case .search: return "az_search".localizable
        case .settings: return "settings".localizable
        case .trakt: return "trakt".localizable
        case .tvProgram: return "tvProgram".localizable
        case .tv: return "TV".localizable
        }
    }
    
    public var type: FilterType? {
        switch self {
        case .movies:
            return .movie
        case .tvShows:
            return .tvshow
        case .search:
            return .all
        case .trakt:
            return .trakt
        case .settings,
             .tvProgram,
             .tv,
             .fullTextSearch:
            return nil
        }
    }
}
