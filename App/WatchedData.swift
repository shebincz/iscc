//
//  WatchedData.swift
//  StreamCinema.atv
//
//  Created by SCC on 29/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import CoreData
import TraktKit

//final class WatchedData {
//    private static func getContext() -> NSManagedObjectContext? {
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
//        return appDelegate.persistentContainer.viewContext
//    }
//
//    private static func getContainer() -> NSPersistentStoreCoordinator? {
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
//        return appDelegate.persistentContainer.persistentStoreCoordinator
//    }
//
//    static func save(new: Watched) {
//        if new.isTvShow == true, self.isWatched(by: new.episodeID, isTvShow: true) {
//            WatchedData.updateWatched(new)
//        } else if new.isTvShow == false, self.isWatched(by: new.movieID, isTvShow: false) {
//            WatchedData.updateWatched(new)
//        } else {
//            WatchedData.save(new)
//        }
//
//        if TraktManager.sharedManager.isSignedIn,
//           new.isSeen {
//            var movies:[RawJSON] = []
//            let shows:[RawJSON] = []
//            var episodes:[RawJSON] = []
//            if new.isTvShow, let traktID = new.traktID{
//                let episodeID = ["trakt":traktID]
//                let episode:RawJSON = ["ids":episodeID]
//                episodes.append(episode)
//            }
//            if !new.isTvShow, let traktID = new.traktID {
//                let movieID = ["trakt":traktID]
//                let movie:RawJSON = ["ids":movieID]
//                movies.append(movie)
//            }
//            _ = try? TraktManager.sharedManager.addToHistory(movies: movies, shows: shows, episodes: episodes) { result in
//                switch result {
//                case .success: break
//
//                case .fail: break
//
//                }
//            }
//        }
//
//    }
//
//    private static func save(_ new:Watched) {
//        guard let managedContext = WatchedData.getContext() else { return }
//        let watched = NSEntityDescription.insertNewObject(forEntityName: "Watches", into: managedContext)
//        watched.setValue(new.movieID, forKey: "movieID")
//        watched.setValue(new.service, forKey: "service")
//        watched.setValue(new.serviceID, forKey: "serviceID")
//        watched.setValue(new.title, forKey: "title")
//        watched.setValue(new.time, forKey: "time")
//        watched.setValue(Date(), forKey: "last")
//        watched.setValue(new.isSeen, forKey: "isSeen")
//        watched.setValue(new.isTvShow, forKey: "isTvShow")
//        watched.setValue(new.episodeID, forKey: "episodeID")
//        watched.setValue(new.season, forKey: "season")
//        watched.setValue(new.episode, forKey: "episode")
//        watched.setValue(new.newTime, forKey: "newTime")
//        watched.setValue(new.traktID, forKey: "traktID")
//        do {
//            try managedContext.save()
//            print("Success")
//        } catch {
//            print("Error saving: \(error)")
//        }
//    }
//
//    private static func updateWatched(_ watched: Watched) {
//        if let managedContext = WatchedData.getContext() {
//            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Watches")
//            request.predicate = NSPredicate(format: "movieID = %@", watched.movieID)
//            request.returnsObjectsAsFaults = false
//            do {
//                let result = try managedContext.fetch(request)
//                let objectUpdate = result[0] as! NSManagedObject
//                objectUpdate.setValue(watched.movieID, forKey: "movieID")
//                objectUpdate.setValue(watched.service, forKey: "service")
//                objectUpdate.setValue(watched.serviceID, forKey: "serviceID")
//                objectUpdate.setValue(watched.title, forKey: "title")
//                objectUpdate.setValue(watched.time, forKey: "time")
//                objectUpdate.setValue(Date(), forKey: "last")
//                objectUpdate.setValue(watched.isSeen, forKey: "isSeen")
//                objectUpdate.setValue(watched.isTvShow, forKey: "isTvShow")
//                objectUpdate.setValue(watched.episodeID, forKey: "episodeID")
//                objectUpdate.setValue(watched.season, forKey: "season")
//                objectUpdate.setValue(watched.episode, forKey: "episode")
//                objectUpdate.setValue(watched.newTime, forKey: "newTime")
//                objectUpdate.setValue(watched.traktID, forKey: "traktID")
//                do {
//                    try managedContext.save()
//                }
//                catch
//                {
//                    print(error)
//                }
//
//            } catch let error {
//                print(error)
//            }
//        }
//    }
//
//    static func getTvShowWatched(by id:String?) -> [Watched]? {
//        if let managedContext = WatchedData.getContext(),
//            let id = id {
//            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Watches")
//            request.predicate = NSPredicate(format: "movieID = %@", id)
//            request.returnsObjectsAsFaults = false
//            do {
//                let result = try managedContext.fetch(request)
//                var watcheds:[Watched] = []
//                for data in result as! [NSManagedObject] {
//                    watcheds.append(Watched(with: data))
//                }
//                return watcheds
//            } catch {
//
//            }
//        }
//        return nil
//    }
//
//    static func getWatched(by id:String?) -> Watched? {
//        if let managedContext = WatchedData.getContext(),
//            let id = id {
//            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Watches")
//            request.predicate = NSPredicate(format: "movieID = %@", id)
//            request.returnsObjectsAsFaults = false
//            do {
//                let result = try managedContext.fetch(request)
//                for data in result as! [NSManagedObject] {
//                    if let movieID = data.value(forKey: "movieID") as? String,
//                        movieID == id {
//
//                        let watched = Watched(with: data)
//                        return watched
//                    }
//              }
//            } catch {
//
//            }
//        }
//        return nil
//    }
//
//    private static func isWatched(by id:String?, isTvShow: Bool) -> Bool {
//        if id == nil {
//            return false
//        }
//
//        if let managedContext = WatchedData.getContext(),
//            let id = id {
//            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Watches")
//            if isTvShow {
//                request.predicate = NSPredicate(format: "episodeID = %@", id)
//            } else {
//                request.predicate = NSPredicate(format: "movieID = %@", id)
//            }
//
//            request.returnsObjectsAsFaults = false
//            do {
//                let result = try managedContext.fetch(request)
//                if result.count == 0 {
//                    return false
//                }
//                let data = result[0] as! NSManagedObject
//
//                var movieID = data.value(forKey: "movieID") as? String
//                if isTvShow {
//                    movieID = data.value(forKey: "episodeID") as? String
//                }
//                if let movieID = movieID,
//                    movieID == id{
//                  return true
//                }
//            } catch {
//
//            }
//        }
//        return false
//    }
//
//    static func getAllWatched(by id:String, completion: @escaping (Result<[Watched], Error>) -> Void)  {
//        if let managedContext = WatchedData.getContext() {
//            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Watches")
//            request.predicate = NSPredicate(format: "movieID = %@", id)
//            request.returnsObjectsAsFaults = false
//            var watches:[Watched] = []
//            do {
//                let result = try managedContext.fetch(request)
//                for data in result as! [NSManagedObject] {
//                    if let movieID = data.value(forKey: "movieID") as? String,
//                        movieID == id {
//
//                        let watched = Watched(with: data)
//
//                        watches.append(watched)
//                    }
//              }
//            } catch let error {
//                completion(.failure(error))
//            }
//            completion(.success(watches))
//        } else {
//            completion(.failure(-1 as! Error))
//        }
//    }
//
//    static func getAllWatched(type: FilterType, completion: @escaping (Result<[Watched], Error>) -> Void) {
//        if let managedContext = WatchedData.getContext() {
//            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Watches")
//            if type == .movie {
//                request.predicate = NSPredicate(format: "isTvShow = 0")
//            } else if type == .tvshow {
//               request.predicate = NSPredicate(format: "isTvShow = 1")
//            }
//
//            request.returnsObjectsAsFaults = false
//                do {
//                    var watches:[Watched] = try WatchedData.fetch(from: managedContext, with: request)
//                    watches.sort { (leftW, rightW) -> Bool in
//                        guard let leftLast = leftW.last, let rightLast = rightW.last else { return false }
//                        return  leftLast > rightLast
//                    }
//                    completion(.success(watches))
//                } catch let error {
//                    completion(.failure(error))
//                }
//        }
//    }
//
//    private static func fetch(from context:NSManagedObjectContext, with request: NSFetchRequest<NSFetchRequestResult>) throws -> [Watched] {
//        var watches:[Watched] = []
//        let result = try context.fetch(request)
//          for data in result as! [NSManagedObject] {
//              let watched = Watched(with: data)
//              watches.append(watched)
//        }
//        return watches
//    }
//
//    public static func deleteAllData(_ entity:String = "Watches") {
//        if let managedContext = WatchedData.getContext(),
//            let persistentContainer = WatchedData.getContainer() {
//
//            let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entity)
//            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//
//            do {
//                try persistentContainer.execute(deleteRequest, with: managedContext)
//            } catch let error as NSError {
//                print("Detele all data in \(entity) error :", error)
//            }
//        }
//    }
//}
//
public enum WatchedState {
    case none
    case paused
    case done
}
//
//
//
//public struct Watched {
//    var movieID: String
//    var title: String
//    var service: String
//    var serviceID: String
//    var time: Int64
//    var newTime: Double
//    var last: Date?
//    var episodeID: String?
//    var isSeen: Bool
//    var isTvShow: Bool
//    var season: Int64?
//    var episode: Int64?
//    var traktID: String?
//    var isSaved: Bool = false
//    var progress: Float = 0.0 {
//        didSet {
//            self.update(self.progress)
//        }
//    }
//
//    var state:WatchedState {
//        get {
//            if self.isSeen {
//                return .done
//            } else if time > 1 {
//                return .paused
//            } else if newTime > 0 {
//                return .paused
//            }
//            return .none
//        }
//    }
//
//    public func stop() {
//        if TraktManager.sharedManager.isSignedIn {
//            if self.isTvShow,
//               let traktEpisode = self.traktID {
//                let ids = ["trakt":traktEpisode]
//                let episode:RawJSON = ["ids":ids]
//                _ = try? TraktManager.sharedManager.scrobbleStop(episode: episode, progress: self.progress, appVersion: 2, appBuildDate: Date(),completion: { _ in })
//            } else if let movieID = self.traktID {
//                let ids = ["trakt":movieID]
//                let movie:RawJSON = ["ids":ids]
//                _ = try? TraktManager.sharedManager.scrobbleStop(movie: movie, progress: self.progress, appVersion: 2, appBuildDate: Date(), completion: { _ in })
//            }
//        }
//    }
//
//    private func update(_ progress: Float) {
//        if TraktManager.sharedManager.isSignedIn {
//            if self.isTvShow,
//               let traktEpisode = self.traktID {
//                let ids = ["trakt":traktEpisode]
//                let episode:RawJSON = ["ids":ids]
//                _ = try? TraktManager.sharedManager.scrobbleStart(episode: episode, progress: progress, appVersion: 2, appBuildDate: Date(), completion: { _ in })
//            } else if let movieID = self.traktID {
//                let ids = ["trakt":movieID]
//                let movie:RawJSON = ["ids":ids]
//                _ = try? TraktManager.sharedManager.scrobbleStart(movie: movie, progress: progress, appVersion: 2, appBuildDate: Date(), completion: { _ in })
//            }
//        }
//    }
//
//    init(movieID: String, title: String, service: String, serviceID: String, time: Int64, newTime: Double, last: Date?, isTvShow:Bool, episodeID: String?, isSeen: Bool, season: Int64?, episode: Int64?, trakt: String?) {
//        self.last = last
//        self.episodeID = episodeID
//        self.movieID = movieID
//        self.title = title
//        self.service = service
//        self.serviceID = serviceID
//        self.time = time
//        self.isSeen = isSeen
//        self.isTvShow = isTvShow
//        self.episode = episode
//        self.season = season
//        self.newTime = newTime
//        self.traktID = trakt
//    }
//
//    init(with data:NSManagedObject) {
//        self.last = data.value(forKey: "last") as? Date
//        self.episodeID = data.value(forKey: "episodeID") as? String
//
//        self.movieID = ""
//        self.title = ""
//        self.service = ""
//        self.serviceID = ""
//        self.time = 0
//        self.newTime = 0
//        self.isSeen = false
//        self.isTvShow = false
//
//        if let movieID = data.value(forKey: "movieID") as? String,
//            let title = data.value(forKey: "title") as? String,
//            let service = data.value(forKey: "service") as? String,
//            let serviceID = data.value(forKey: "serviceID") as? String,
//            let time = data.value(forKey: "time") as? Int64,
//            let newTime = data.value(forKey: "newTime") as? Double,
//            let isSeen = data.value(forKey: "isSeen") as? Bool,
//            let isTvShow = data.value(forKey: "isTvShow") as? Bool,
//            let season = data.value(forKey: "season") as? Int64,
//            let episode = data.value(forKey: "episode") as? Int64,
//            let traktID = data.value(forKey: "traktID") as? String
//        {
//            self.movieID = movieID
//            self.title = title
//            self.service = service
//            self.serviceID = serviceID
//            self.time = time
//            self.newTime = newTime
//            self.isSeen = isSeen
//            self.isTvShow = isTvShow
//            self.season = season
//            self.episode = episode
//            self.traktID = traktID
//        }
//    }
//}


public struct SccWatched {
    var isSeen: Bool
    var time: Double
    var lastUpdated: Date?
    
    var traktID: String?
    
    //Movie
    var movieID: String?
    
    //TV Show
    var tvShowID: String?
    var seasonNumber: Int?
    var seasonID: String?
    var episodeNumber: Int?
    var episodeID: String?
    
    var isTvShow: Bool {
        get {
            if self.episodeID != nil ||
                self.seasonID != nil ||
                self.tvShowID != nil {
                return true
            }
            return false
        }
    }
    
    var state:WatchedState {
        get {
            if self.isSeen {
                return .done
            } else if time > 1 {
                return .paused
            } else if time > 0 {
                return .paused
            }
            return .none
        }
    }
    init(isSeen: Bool, time: Double, lastUpdated: Date?,
         traktID: String?, movieID: String?, tvShowID: String?,
         sessionNumber: Int?, seasonID: String?, episodeNumber: Int?,
         episodeID: String?)
    {
        self.isSeen = isSeen
        self.time = time
        self.lastUpdated = lastUpdated
        self.traktID = traktID
        self.movieID = movieID
        self.tvShowID = tvShowID
        self.seasonNumber = sessionNumber
        self.seasonID = seasonID
        self.episodeNumber = episodeNumber
        self.episodeID = episodeID
    }
    
    
    init(with data:NSManagedObject) {
        self.isSeen = data.value(forKey: "isWatched") as? Bool ?? false
        self.time = data.value(forKey: "time") as? Double ?? 0
        self.traktID = data.value(forKey: "traktID") as? String
        self.movieID = data.value(forKey: "movieID") as? String
        self.tvShowID = data.value(forKey: "tvShowID") as? String
        self.seasonNumber = data.value(forKey: "sessionNumber") as? Int
        self.seasonID = data.value(forKey: "seasonID") as? String
        self.episodeNumber = data.value(forKey: "episodeNumber") as? Int
        self.episodeID = data.value(forKey: "episodeID") as? String
        self.lastUpdated = data.value(forKey: "lastUpdated") as? Date
    }
    
    public func stop( progress: Float) {
        if TraktManager.sharedManager.isSignedIn {
            if self.isTvShow,
               let traktEpisode = self.traktID {
                let ids = ["trakt":traktEpisode]
                let episode:RawJSON = ["ids":ids]
                _ = try? TraktManager.sharedManager.scrobbleStop(episode: episode, progress: progress, appVersion: 2, appBuildDate: Date(),completion: { _ in })
            } else if let movieID = self.traktID {
                let ids = ["trakt":movieID]
                let movie:RawJSON = ["ids":ids]
                _ = try? TraktManager.sharedManager.scrobbleStop(movie: movie, progress: progress, appVersion: 2, appBuildDate: Date(), completion: { _ in })
            }
        }
    }
    
    public func update( progress: Float) {
        if TraktManager.sharedManager.isSignedIn {
            if self.isTvShow,
               let traktEpisode = self.traktID {
                let ids = ["trakt":traktEpisode]
                let episode:RawJSON = ["ids":ids]
                _ = try? TraktManager.sharedManager.scrobbleStart(episode: episode, progress: progress, appVersion: 2, appBuildDate: Date(), completion: { _ in })
            } else if let movieID = self.traktID {
                let ids = ["trakt":movieID]
                let movie:RawJSON = ["ids":ids]
                _ = try? TraktManager.sharedManager.scrobbleStart(movie: movie, progress: progress, appVersion: 2, appBuildDate: Date(), completion: { _ in })
            }
        }
    }
    
    public func updateObject(_ watched: NSManagedObject)  {
        watched.setValue(self.isSeen, forKey: "isWatched")
        watched.setValue(self.time, forKey: "time")
        watched.setValue(Date(), forKey: "lastUpdated")
        watched.setValue(self.traktID, forKey: "traktID")
        watched.setValue(self.movieID, forKey: "movieID")
        watched.setValue(self.tvShowID, forKey: "tvShowID")
        watched.setValue(self.seasonNumber, forKey: "sessionNumber")
        watched.setValue(self.seasonID, forKey: "seasonID")
        watched.setValue(self.episodeNumber, forKey: "episodeNumber")
        watched.setValue(self.episodeID, forKey: "episodeID")
    }
    
    static func empty() -> SccWatched {
        return SccWatched(isSeen: false, time: 0, lastUpdated: nil, traktID: nil, movieID: nil, tvShowID: nil, sessionNumber: nil, seasonID: nil, episodeNumber: nil, episodeID: nil)
    }
}


final class SccWatchedData {
    private static func getContext() -> NSManagedObjectContext? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        return appDelegate.persistentContainer.viewContext
    }
    
    private static func getContainer() -> NSPersistentStoreCoordinator? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        return appDelegate.persistentContainer.persistentStoreCoordinator
    }
    
    static func save(new: SccWatched, isWithoutUpdateTrakt: Bool = false) {
        let tvShowID = new.episodeID ?? new.seasonID ?? new.tvShowID
        if new.isTvShow == true, self.isWatched(by: tvShowID, isTvShow: true) {
            SccWatchedData.updateWatched(new)
        } else if new.isTvShow == false, self.isWatched(by: new.movieID, isTvShow: false) {
            SccWatchedData.updateWatched(new)
        } else {
            SccWatchedData.save(new)
        }

        if TraktManager.sharedManager.isSignedIn,
           isWithoutUpdateTrakt == false,
           new.isSeen {
            var movies:[RawJSON] = []
            let shows:[RawJSON] = []
            var episodes:[RawJSON] = []
            if new.isTvShow, let traktID = new.traktID{
                let episodeID = ["trakt":traktID]
                let episode:RawJSON = ["ids":episodeID]
                episodes.append(episode)
            }
            if !new.isTvShow, let traktID = new.traktID {
                let movieID = ["trakt":traktID]
                let movie:RawJSON = ["ids":movieID]
                movies.append(movie)
            }
            _ = try? TraktManager.sharedManager.addToHistory(movies: movies, shows: shows, episodes: episodes) { result in
                switch result {
                case .success: break
                    
                case .fail: break
                    
                }
            }
        }
        
    }
    
    private static func save(_ new:SccWatched) {
        guard let managedContext = SccWatchedData.getContext() else { return }
        let watched = NSEntityDescription.insertNewObject(forEntityName: "SccWatches",
                                                          into: managedContext)
        new.updateObject(watched)
        do {
            try managedContext.save()
            print("Success")
        } catch {
            print("Error saving: \(error)")
        }
    }

    private static func updateWatched(_ watched: SccWatched) {
        if let managedContext = SccWatchedData.getContext() {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SccWatches")
            
            if let movieID = watched.movieID {
                request.predicate = NSPredicate(format: "movieID = %@", movieID)
            } else if let episodeID = watched.episodeID {
                request.predicate = NSPredicate(format: "episodeID = %@", episodeID)
            } else if let tvShowID = watched.tvShowID {
                request.predicate = NSPredicate(format: "tvShowID = %@", tvShowID)
            } else if let seasonID = watched.seasonID {
                request.predicate = NSPredicate(format: "seasonID = %@", seasonID)
            } else {
                return
            }
            
            request.returnsObjectsAsFaults = false
            do {
                let result = try managedContext.fetch(request)
                if let objectUpdate = result[0] as? NSManagedObject {
                    watched.updateObject(objectUpdate)
                } else {
                    return
                }
                
                try managedContext.save()
                
            } catch let error {
                print(error)
            }
        }
    }
    
    static func getSeasonWatched(by id:String?) -> [SccWatched] {
        if let managedContext = SccWatchedData.getContext(),
            let id = id {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SccWatches")
            request.predicate = NSPredicate(format: "seasonID = %@", id)
            request.returnsObjectsAsFaults = false
            do {
                let result = try managedContext.fetch(request)
                var watcheds:[SccWatched] = []
                for data in result as! [NSManagedObject] {
                    watcheds.append(SccWatched(with: data))
                }
                return watcheds
            } catch {
               
            }
        }
        return []
    }
    
    static func getTvShowWatched(by id:String?) -> [SccWatched]? {
        if let managedContext = SccWatchedData.getContext(),
            let id = id {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SccWatches")
            request.predicate = NSPredicate(format: "tvShowID = %@", id)
            request.returnsObjectsAsFaults = false
            do {
                let result = try managedContext.fetch(request)
                var watcheds:[SccWatched] = []
                for data in result as! [NSManagedObject] {
                    watcheds.append(SccWatched(with: data))
                }
                return watcheds
            } catch {
               
            }
        }
        return nil
    }
    
    static func getWatched(by id:String?, isTvShow:Bool = false) -> SccWatched? {
        if let managedContext = SccWatchedData.getContext(),
            let id = id {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SccWatches")
            var key = "movieID"
            if isTvShow {
                key = "tvShowID"
            }
            request.predicate = NSPredicate(format: "\(key) = %@", id)
            
            request.returnsObjectsAsFaults = false
            do {
                let result = try managedContext.fetch(request)
                for data in result as! [NSManagedObject] {
                    if let movieID = data.value(forKey: key) as? String,
                        movieID == id {
                        
                        let watched = SccWatched(with: data)
                        return watched
                    }
              }
            } catch {
               
            }
        }
        return nil
    }
    
    private static func isWatched(by id:String?, isTvShow: Bool) -> Bool {
        if id == nil {
            return false
        }
        
        if let managedContext = SccWatchedData.getContext(),
            let id = id {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SccWatches")
            if isTvShow {
                request.predicate = NSPredicate(format: "episodeID = %@ OR seasonID == %@ OR tvShowID == %@", id,id,id)
            } else {
                request.predicate = NSPredicate(format: "movieID = %@", id)
            }
            
            request.returnsObjectsAsFaults = false
            do {
                let result = try managedContext.fetch(request)
                if result.count == 0 {
                    return false
                }
                let data = result[0] as! NSManagedObject
                
                var movieID = data.value(forKey: "movieID") as? String
                if isTvShow {
                    movieID = data.value(forKey: "episodeID") as? String
                    
                    if movieID == nil {
                        movieID = data.value(forKey: "seasonID") as? String
                    }
                    if movieID == nil {
                        movieID = data.value(forKey: "tvShowID") as? String
                    }
                }
                if let movieID = movieID,
                    movieID == id{
                  return true
                }
            } catch {
               
            }
        }
        return false
    }
    
    static func getAllWatched(by id:String, completion: @escaping (Result<[SccWatched], Error>) -> Void)  {
        if let managedContext = SccWatchedData.getContext() {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SccWatches")
            request.predicate = NSPredicate(format: "movieID = %@", id)
            request.returnsObjectsAsFaults = false
            var watches:[SccWatched] = []
            do {
                let result = try managedContext.fetch(request)
                for data in result as! [NSManagedObject] {
                    if let movieID = data.value(forKey: "movieID") as? String,
                        movieID == id {
                        let watched = SccWatched(with: data)
                        watches.append(watched)
                    }
              }
            } catch let error {
                completion(.failure(error))
            }
            completion(.success(watches))
        } else {
            completion(.failure(-1 as! Error))
        }
    }
    
    static func getAllWatched(type: FilterType, completion: @escaping (Result<[SccWatched], Error>) -> Void) {
        if let managedContext = SccWatchedData.getContext() {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SccWatches")
            
            var predicateString = "movieID != nil"
            if type == .tvshow {
               predicateString = "tvShowID != nil"
            }
            request.predicate = NSPredicate(format: predicateString)
            request.returnsObjectsAsFaults = false
                do {
                    var watches:[SccWatched] = try SccWatchedData.fetch(from: managedContext, with: request)
                    watches.sort { (leftW, rightW) -> Bool in
                        guard let leftLast = leftW.lastUpdated, let rightLast = rightW.lastUpdated else { return false }
                        return  leftLast > rightLast
                    }
                    let filtredResult = watches.filterDuplicates { (lhs, rhs) -> Bool in
                        if type == .movie {
                            return lhs.movieID == rhs.movieID
                        } else if type == .tvshow {
                            return lhs.tvShowID == rhs.tvShowID
                        }
                        return false
                    }
                    completion(.success(filtredResult))
                } catch let error {
                    completion(.failure(error))
                }
        }
    }
    
    private static func fetch(from context:NSManagedObjectContext, with request: NSFetchRequest<NSFetchRequestResult>) throws -> [SccWatched] {
        var watches:[SccWatched] = []
        let result = try context.fetch(request)
          for data in result as! [NSManagedObject] {
              let watched = SccWatched(with: data)
              watches.append(watched)
        }
        return watches
    }
    
    public static func deleteAllData(_ entity:String = "SccWatches") {
        if let managedContext = self.getContext(),
            let persistentContainer = self.getContainer() {
            
            let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entity)
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

            do {
                try persistentContainer.execute(deleteRequest, with: managedContext)
            } catch let error as NSError {
                print("Detele all data in \(entity) error :", error)
            }
        }
    }
}

extension InfoData {
    public func getMovieWatched(isSeen:Bool = false, timeSeen:Date? = Date(), type:String = "movie") -> SccWatched {
        if let movieID = self.id {
            var sccWatched = SccWatched.empty()
            sccWatched.isSeen = isSeen
            sccWatched.lastUpdated = timeSeen
            sccWatched.traktID = self.source?.services?.trakt
            if type == "movie" {
                sccWatched.movieID = movieID
            } else if type == "tvshow" {
                sccWatched.tvShowID = movieID
            }
            
            return sccWatched
        }
        fatalError()
    }
}
