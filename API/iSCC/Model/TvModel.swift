//
//  TvModel.swift
//  StreamCinema.atv
//
//  Created by SCC on 25/11/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

struct TVs: Model {
    let data:[TV]
    
    enum CodingKeys: String, CodingKey {
        case data
    }
    
    static func empty<T>() -> T where T : Model {
        let tvs = TVs(data: [])
        guard let emptyTVs = tvs as?T else {
            fatalError()
        }
        return emptyTVs
    }
    
    public var tvs:[TV] {
        get {
            return self.data.filter { tv -> Bool in
                return tv.url != nil
            }
        }
    }
}

struct TV: Codable, Model {
    let id: Int?
    let tvtitle: String?
    let url: String?
    let extFilter: String?
    let group, tvid: String?
    let tvlogo: String?

    enum CodingKeys: String, CodingKey {
        case id, tvtitle, url
        case extFilter
        case group, tvid, tvlogo
    }
    
    static func empty<T>() -> T where T : Model {
        let tv = TV(id: nil, tvtitle: nil, url: nil, extFilter: nil, group: nil, tvid: nil, tvlogo: nil)
        guard let emptyTV = tv as?T else {
            fatalError()
        }
        return emptyTV
    }
}
