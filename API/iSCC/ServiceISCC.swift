//
//  FindService.swift
//  StreamCinema
//
//  Created by SCC on 08/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import XMLMapper

final class ServiceISCC: Service {
    func processTokenExpireError(_ error: Error) {
        
    }
    
    func start() {

    }

    func stop() {

    }

    func provider() -> MoyaProvider<RequestISCC> {
        return Provider.isccRequest
    }

    var errorBehaviour: BehaviorSubject<RequestErrors> = BehaviorSubject(value: .httpError)
    let disposeBag = DisposeBag()

    //MARK: - Sources Endpoind
    public func connect(completion: @escaping (Result<Bool, RequestErrors>) -> Void) {
        self.requestWithoutMaping(request: RequestISCC.postData, apiClient: self.provider(), completion: completion)
    }
    
    public func getTVdata(completion: @escaping (Result<TVs, RequestErrors>) -> Void) {
        self.requestArr(request: RequestISCC.getTvData,
                        apiClient: self.provider(),
                        model: TVs.self) { result in
            completion(result)
        }
    }
    
    public func getEPG(for station:String, completion: @escaping (Result<StationEPG, RequestErrors>) -> Void) {
        self.request(request: RequestISCC.getEPGData(station: station),
                     apiClient: self.provider(),
                     model: StationEPG.self) { result in
            completion(result)
        }
    }
    
//    public func getEPG(completion: @escaping (Result<XMLTV, RequestErrors>) -> Void) {
//        self.provider().rx
//            .request(RequestISCC.getEPGData, callbackQueue:DispatchQueue.global(qos: .background))
//            .filterSuccessfulStatusCodes()
//            .retry(1)
//            .map { response -> XMLTV in
//
//                do {
//                    return try XMLTV(data: response.data)
//                } catch let error {
//                    print(error)
//                }
//                return XMLTV()
//            }
//            .subscribe(onSuccess: { (models) in
//                completion(.success(models))
//            }, onError: { (error) in
//                completion(.failure(.httpError))
//            })
//            .disposed(by: disposeBag)
//    }
}
