//
//  Download.swift
//  StreamCinema.atv
//
//  Created by SCC on 05/08/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

struct OSDownload: Model {
    let link: String?
    let fname: String?
    let requests, allowed, remaining: Int?
    let message: String?
        
    static func empty<T>() -> T where T: Model {
        let download = OSDownload(link: nil, fname: nil, requests: nil, allowed: nil, remaining: nil, message: nil)
        guard let empytDownload = download as?T else {
            fatalError()
        }
        return empytDownload
    }
}
