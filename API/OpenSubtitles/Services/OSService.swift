//
//  OSService.swift
//  StreamCinema.atv
//
//  Created by SCC on 05/08/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import Moya
import RxSwift


final class OSService: Service {
    func processTokenExpireError(_ error: Error) {
        
    }
    
    func start() {

    }

    func stop() {

    }

    func provider() -> MoyaProvider<OSRequest> {
        return Provider.osRequest
    }

    var errorBehaviour: BehaviorSubject<RequestErrors> = BehaviorSubject(value: .httpError)
    let disposeBag = DisposeBag()
    
    public func login(name:String, pass:String, completion: @escaping (Result<OSLoginResult, RequestErrors>) -> Void) {
        self.request(request: OSRequest.login(name: name, pass: pass),
                     apiClient: self.provider(),
                     model: OSLoginResult.self) { result in
                        completion(result)
        }
    }
    
    public func find(imdbid:String, lang:[String], completion: @escaping (Result<[OSDownload], RequestErrors>) -> Void) {
        guard let imdbID = imdbid.split(separator: "t").last else { completion(.failure(.requestFailed)); return }
        self.request(request: OSRequest.findWith(imdbid: String(imdbID), lang: lang),
                     apiClient: self.provider(),
                     model: OSFindResult.self) { result in
            switch result {
            case .success(let resultData):
                let data = resultData.data.sorted { (lhas, rhs) -> Bool in
                    if let lhsRating = rhs.attributes?.ratings,
                       let rhsRating = rhs.attributes?.ratings {
                        return lhsRating > rhsRating
                    }
                    return false
                }
                let skData = data.filter { data -> Bool in
                    return data.attributes?.language?.lowercased() == "sk"
                }
                let enData = data.filter { data -> Bool in
                    return data.attributes?.language?.lowercased() == "en"
                }
                let csData = data.filter { data -> Bool in
                    return data.attributes?.language?.lowercased() == "cs"
                }
                var links: [OSDownload] = []
                var count = 0
                self.getLinkFrom(skData) { result in
                    count += 1
                    switch result {
                    case .success(let link):
                        links.append(link)
                    case .failure(let err):
                        if count == 3, links.count != 0 {
                            completion(.failure(err))
                        }
                    }
                    if count == 3, links.count != 0 {
                        completion(.success(links))
                    }
                }
                self.getLinkFrom(enData) { result in
                    count += 1
                    switch result {
                    case .success(let link):
                        links.append(link)
                    case .failure(let err):
                        if count == 3, links.count != 0 {
                            completion(.failure(err))
                        }
                    }
                    if count == 3, links.count != 0 {
                        completion(.success(links))
                    }
                }
                self.getLinkFrom(csData) { result in
                    count += 1
                    switch result {
                    case .success(let link):
                        links.append(link)
                    case .failure(let err):
                        if count == 3, links.count != 0 {
                            completion(.failure(err))
                        }
                    }
                    if count == 3, links.count != 0 {
                        completion(.success(links))
                    }
                }

            case .failure(let err):
                completion(.failure(err))
            }
        }
    }
    
    private func getLinkFrom(_ allData:[OSData], completion: @escaping (Result<OSDownload, RequestErrors>) -> Void) {
        if let data = allData.first,
           let id = data.attributes?.files?.first?.id,
           let name = data.attributes?.files?.first?.fileName {
            self.getDownload(id: String(id), name: name) { result in
                completion(result)
            }
        } else {
            completion(.failure(.noDataForRequest))
        }
    }
    
    public func getDownload(id:String, name:String, completion: @escaping (Result<OSDownload, RequestErrors>) -> Void) {
        self.request(request: OSRequest.download(id: id, name: name),
                     apiClient: self.provider(),
                     model: OSDownload.self) { result in
            completion(result)
        }
    }
    
    public func downloadFile(from url:URL, completion: @escaping (Result<OSProgressModel, RequestErrors>) -> Void) {
        
    }
}
