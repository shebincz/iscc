//
//  FilterResultModel.swift
//  StreamCinema
//
//  Created by SCC on 08/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//
import Foundation

// MARK: - Welcome
struct FilterResult: DataConvertible, Hashable, Model {
    var pagination: Pagination?
    let totalCount: Int?
    var data: [InfoData]?
    
    enum CodingKeys: String, CodingKey {
        case pagination = "pagination"
        case totalCount = "totalCount"
        case data = "data"
    }
    
    static func empty<T>() -> T where T: Model {
        let filterResult = FilterResult(pagination: Pagination.empty(), totalCount: 0, data: [])
        guard let emptyFilterResult = filterResult as?T else {
            fatalError()
        }
        return emptyFilterResult
    }
}
