//
//  FindService.swift
//  StreamCinema
//
//  Created by SCC on 08/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import TraktKit

final class SC2Service: Service {
    func processTokenExpireError(_ error: Error) {
        
    }
    
    func start() {

    }

    func stop() {

    }

    func provider() -> MoyaProvider<SC2Request> {
//        Provider.sc2Request.manager.session.configuration.timeoutIntervalForRequest = 200
        return Provider.sc2Request
    }

    var errorBehaviour: BehaviorSubject<RequestErrors> = BehaviorSubject(value: .httpError)
    let disposeBag = DisposeBag()

    //MARK: - Sources Endpoind
    public func setAsPlayed(mediaID: String) {
        self.requestWithoutMaping(request: SC2Request.setAsPlayed(mediaID: mediaID), apiClient: self.provider()) { _ in }
    }
    
    public func getTvProgram(for station: String, date: String, page: Int, completion: @escaping (Result<FilterResult, RequestErrors>) -> Void) {
        self.request(request: SC2Request.tvProgram(station: station, date: date, page:page),
                     apiClient: self.provider(),
                     model: FilterResult.self,
                     completion: completion)
    }
    
    public func getMovies(traktID:[String], page: Int, completion: @escaping (Result<FilterResult, RequestErrors>) -> Void) {
        if traktID.count == 0 {
            completion(.success(FilterResult.empty()))
            return
        }
        self.filter(with: .initTaktFilter(value: traktID, page: page, limit:traktID.count), completion: completion)
    }
    
    public func getStreams(for mediaID: String, completion: @escaping (Result<VideoStreams, RequestErrors>) -> Void) {
        self.requestArr(request: SC2Request.getStream(mediaID: mediaID),
                     apiClient: self.provider(),
                     model: VideoStreams.self) { result in
                        completion(result)
        }
    }
    
    public func getMovie(by movieID:String, type: FilterType, completion: @escaping (Result<SourceInfo, RequestErrors>) -> Void) {
        self.request(request: SC2Request.getMedia(mediaID: movieID),
                     apiClient: self.provider(),
                     model: SourceInfo.self) { result in
                        completion(result)
        }
    }
    
    public func getMovies(type: FilterType, for item: MenuItem, page: Int = 1, completion: @escaping (Result<FilterResult, RequestErrors>) -> Void) {
        switch item {
        case .dubbed:
            self.filter(with: .lastReleasedDubbed(type: type, page: page), completion: completion)
        case .lastAdded:
            self.filter(with: .lastAddedFilter(type: type, page: page), completion: completion)
        case .news:
            self.filter(with: .newsFilter(type: type, page: page), completion: completion)
        case .popular:
            self.filter(with: .popular(type: type, page: page), completion: completion)
        case .watched:
            self.getWatched(type: type, page: page, completion: completion)
        case .mostWatched:
            self.filter(with: .lastMostWatched(type: type, page: page), completion: completion)
        case .traktWatchList:
            self.getTraktWatchList(page: page, completion: completion)
        case .trending:
            self.filter(with: .trending(type: type, page: page), completion: completion)
        case .traktHistory:
            self.getTraktHistori(page: page, completion: completion)
        case .genre:
            completion(.failure(.requestFailed))
        }
    }
    
    private func getTraktHistori(page: Int, completion: @escaping (Result<FilterResult, RequestErrors>) -> Void) {
        self.getHistoryTraktData { result in
            switch result {
            case .success(let ids):
                self.getMovies(traktID: ids, page: page, completion: completion)
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    private func getHistoryTraktData(completion: @escaping (Result<[String], RequestErrors>) -> Void) {
        TraktManager.sharedManager.getHistory { response in
            switch response {
            case .success(objects: let objects, currentPage: let currentPage, limit: let limit):
                Log.write("getHistory.success(objects: \(objects), currentPage: \(currentPage), limit: \(limit)")
                var ids:[String] = []
                for traktData in objects {
                    if let traktID = traktData.movie?.ids.trakt {
                        ids.append("\(traktData.type):\(traktID)")
                    }
                    if let traktID = traktData.show?.ids.trakt {
                        ids.append("\(traktData.type):\(traktID)")
                    }
                }
                DispatchQueue.main.async {
                    completion(.success(ids))
                }
            case .error(error: _):
                DispatchQueue.main.async {
                    completion(.failure(.httpError))
                }
            }
        }
    }
    
    private func getTraktWatchList(page: Int, completion: @escaping (Result<FilterResult, RequestErrors>) -> Void) {
        self.getWatchlistTraktData { result in
            switch result {
            case .success(let ids):
                self.getMovies(traktID: ids, page: page, completion: completion)
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    private func getWatchlistTraktData(completion: @escaping (Result<[String], RequestErrors>) -> Void) {
        TraktManager.sharedManager.getWatchlist(watchType: .Movies) { response in
            switch response {
            case .error(_):
                completion(.failure(.httpError))
            case .success(let objects, _, _):
                var ids:[String] = []
                for traktData in objects {
                    if let traktID = traktData.movie?.ids.trakt {
                        ids.append("\(traktData.type):\(traktID)")
                    }
                    if let traktID = traktData.show?.ids.trakt {
                        ids.append("\(traktData.type):\(traktID)")
                    }
                }
                completion(.success(ids))
            }
        }
    }
    
    public func getSeries(for mediaID:String, page: Int, completion: @escaping (Result<FilterResult, RequestErrors>) -> Void) {
        self.filter(with: FilterModel.seasonFilter(mediaID:mediaID, page: page), completion: completion)
    }
    
    public func getCsfdMedia(for csfdIDs:[String], type: FilterType, page: Int, sort:FilterSort? = nil, completion: @escaping (Result<FilterResult, RequestErrors>) -> Void) {
        self.filter(with: FilterModel.csfdFillter(serviceID: csfdIDs, type: type, page: page, sort:sort)) { result in
            switch result {
                case .success(let resultData):
                    completion(.success(resultData))
                case .failure(let error):
                    completion(.failure(error))
            }
        }
    }
    
    public func getMedia(by ids:[String], type: FilterType, page: Int, sort:FilterSort? = nil, completion: @escaping (Result<FilterResult, RequestErrors>) -> Void) {
        self.filter(with: FilterModel.sccIDsFilter(sccIDs: ids, type: type, page: page, sort:sort, limit:ids.count)) { result in
            switch result {
                case .success(let resultData):
                    completion(.success(resultData))
                case .failure(let error):
                    completion(.failure(error))
            }
        }
    }

    private func getWatched(type: FilterType, page: Int, completion: @escaping (Result<FilterResult, RequestErrors>) -> Void) {
        SccWatchedData.getAllWatched(type: type) { result in
            switch result {
            case .success(let data):
                self.getMedia(with: data, type: type, page: page, completion: completion)
            case .failure(_):
                completion(.failure(.noDataForRequest))
            }
        }
    }
    
    private func getMedia(with data:[SccWatched], type: FilterType, page: Int, completion: @escaping (Result<FilterResult, RequestErrors>) -> Void) {
        if data.count == 0 {
            completion(.success(FilterResult(pagination: nil, totalCount: nil, data: [])))
            return
        }
        var allSccIDs:[String] = []
        for watched in data {
            if let sccID = watched.tvShowID {
                allSccIDs.append(sccID)
            }
            if let sccID = watched.movieID {
                allSccIDs.append(sccID)
            }
        }
        self.getMedia(by: allSccIDs, type: type, page: page, completion: completion)
    }
    
    //MARK: - FilterEndpoint
    public func search(wtih model:FilterModel, completion: @escaping (Result<FilterResult, RequestErrors>) -> Void) {
        self.filter(with: model) { result in
            completion(result)
        }
    }
    
    public func azSearch(model: FilterModel, completion: @escaping (Result<AZResults, RequestErrors>) -> Void) {
        self.request(request: SC2Request.azSearch(filter: model),
                     apiClient: self.provider(),
                     model: AZResults.self) { result in
                        completion(result)
        }
        
    }
    
    private func filter(with model: FilterModel, completion: @escaping (Result<FilterResult, RequestErrors>) -> Void) {
        self.request(request: SC2Request.filter(with: model),
                     apiClient: self.provider(),
                     model: FilterResult.self) { result in
                        completion(result)
        }
    }
}
