//
//  TopShelfSC2Service.swift
//  StreamCinema.atv
//
//  Created by SCC on 26/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import Moya
import RxSwift

final class TopShelfSC2Service: Service {
    func processTokenExpireError(_ error: Error) {
        
    }

    func start() {

    }

    func stop() {

    }

    func provider() -> MoyaProvider<SC2Request> {
        return Provider.sc2Request
    }

    var errorBehaviour: BehaviorSubject<RequestErrors> = BehaviorSubject(value: .httpError)
    let disposeBag = DisposeBag()
    
    func getTopShelfData(wtith type: FilterType, completion: @escaping (Result<FilterResult, RequestErrors>) -> Void) {
        var filter = FilterModel.newsFilter(type: type,page: 1)
        filter.limit = 5
        self.filter(with: filter, completion: completion)
    }
    
    private func filter(with model: FilterModel, completion: @escaping (Result<FilterResult, RequestErrors>) -> Void) {
        self.request(request: SC2Request.filter(with: model),
                     apiClient: self.provider(),
                     model: FilterResult.self) { result in
                        completion(result)
        }
    }
}
