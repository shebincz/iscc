//
//  LoginManager.swift
//  StreamCinema
//
//  Created by SCC on 26/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import CommonCrypto
import Moya
import RxSwift
import KeychainStorage

enum WsSeecret {
    case name
    case pass
    case token
    case passHash
    
    var secreet: String {
        switch self {
        case .name:
            return "com.scc.atv.ws.name"
        case .pass:
            return "com.scc.atv.ws.pass"
        case .token:
            return "com.scc.atv.ws.token"
        case .passHash:
            return "com.scc.atv.ws.passHash"
        }
    }
}

enum OSSeecret {
    case name
    case pass
    case token
    
    var secreet: String {
        switch self {
        case .name:
            return "com.scc.atv.os.name"
        case .pass:
            return "com.scc.atv.os.pass"
        case .token:
            return "com.scc.atv.os.token"
        }
    }
}

extension LoginManager {
    public static var osTokenHash:String? {
        get {
            return try? LoginManager.storage.string(forKey: OSSeecret.token.secreet)
        }
    }
    public static var osName:String? {
           get {
               return try? LoginManager.storage.string(forKey: OSSeecret.name.secreet)
           }
       }
       
    public static var osPass:String? {
        get {
            return try? LoginManager.storage.string(forKey: OSSeecret.name.secreet)
        }
    }
    
    public static func osLogin(name:String?, pass:String?, completion: @escaping (Result<OSLoginResult, RequestErrors>) -> Void) {
        
        guard let delegate = UIApplication.shared.delegate as? AppDelegate,
            let userName = name ?? LoginManager.osName,
            let password = pass ?? LoginManager.pass else {
            completion(.failure(.noDataForRequest))
            return
        }
        delegate.appData.osService.login(name:userName , pass: password) { result in
            switch result {
            case .success(let data):
                try? LoginManager.storage.set(userName, key: OSSeecret.name.secreet)
                try? LoginManager.storage.set(password, key: OSSeecret.pass.secreet)
                if let token = data.token {
                    try? LoginManager.storage.set(token, key: OSSeecret.token.secreet)
                }
                DispatchQueue.main.async {
                    completion(.success(data))
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
        }
    }
}

public final class LoginManager {
    private static let storage:KeychainStorage = KeychainStorage(service: "com.scc.atv")
    
    public static var tokenHash:String? {
        get {
            return try? LoginManager.storage.string(forKey: WsSeecret.token.secreet)
        }
    }
    
    public static var passwordHash:String? {
        get {
            return try? LoginManager.storage.string(forKey: WsSeecret.passHash.secreet)
        }
    }
    
    public static var name:String? {
        get {
            return try? LoginManager.storage.string(forKey: WsSeecret.name.secreet)
        }
    }
    
    public static var pass:String? {
        get {
            return try? LoginManager.storage.string(forKey: WsSeecret.pass.secreet)
        }
    }
    
    func connectWithExistingData(completion: @escaping (Result<Bool, RequestErrors> ) -> Void) {
        guard let name = LoginManager.name, let pass = LoginManager.pass else {
            completion(.failure(.noDataForRequest))
            return
        }
        
        self.connect(name: name, pass: pass, isForceLogin: true, completion: completion)
    }
    
    func connect(name: String?, pass: String?, isForceLogin: Bool = false, completion: @escaping (Result<Bool, RequestErrors> ) -> Void) {
        if self.isLoggedOn(), isForceLogin == false {
            completion(.success(true))
            return
        }

        guard  let name = name ?? LoginManager.name, let pass = pass ?? LoginManager.pass else { completion(.failure(.noDataForRequest)); return }
        
        self.getSalt(name: name) { result in
            switch result {
            case .success(let model):
                self.login(with: name, password: pass, model: model, completion: completion)
            case .failure(let error):
                completion(.failure(error))
            }
            
        }
    }
    
    
    
    private func isLoggedOn() -> Bool {
        if let token = LoginManager.tokenHash,
            let delegate = UIApplication.shared.delegate as? AppDelegate {
            let tokenData = WSTokenData(token: token, passwordHash: LoginManager.passwordHash, name: LoginManager.name)
            delegate.appData.token = tokenData
            return true
        }
        return false
    }
    
    static func logout(isEraseAllData: Bool, completion: @escaping (Result<Bool, RequestErrors> ) -> Void) {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate,
              let token = LoginManager.tokenHash,
              let uuid = UIDevice.current.identifierForVendor else {
                completion(.failure(.noDataForRequest))
                return
        }
        
        delegate.appData.wsService.logout(device_uuid: uuid, token: token) {result in
            switch result {
            case .success(_):
                LoginManager.removeAppCredentials()
                if isEraseAllData {
                    LoginManager.eraseAllData()
                }
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    private static func eraseAllData() {
        //TODO: erase other app data
    }
    
    private static func removeAppCredentials() {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate  else { return }
        delegate.appData.token = nil
        try? LoginManager.storage.removeValue(forKey: WsSeecret.name.secreet)
        try? LoginManager.storage.removeValue(forKey: WsSeecret.pass.secreet)
        try? LoginManager.storage.removeValue(forKey: WsSeecret.passHash.secreet)
        try? LoginManager.storage.removeValue(forKey: WsSeecret.token.secreet)
    }
    
     
    
    private func login(with name:String, password: String, model: SaltModel, completion: @escaping (Result<Bool, RequestErrors> ) -> Void) {
        if let salt = model.salt,
            let passwordHash = self.getHash(password, salt: salt),
            let delegate = UIApplication.shared.delegate as? AppDelegate {
            let data:[String : Any] = ["username_or_email":name, "password":passwordHash, "keep_logged_in":1]
            delegate.appData.wsService.login(params: data) { result in
                switch result {
                case .success(let model):
                    try? LoginManager.storage.set(name, key: WsSeecret.name.secreet)
                    try? LoginManager.storage.set(password, key: WsSeecret.pass.secreet)
                    try? LoginManager.storage.set(passwordHash, key: WsSeecret.passHash.secreet)
                    if let token = model.token {
                       try? LoginManager.storage.set(token, key: WsSeecret.token.secreet)
                    }
                    let tokenData = WSTokenData(token: model.token, passwordHash: passwordHash, name: name)
                    delegate.appData.token = tokenData
                    completion(.success(true))
                case .failure(let error):
                    delegate.appData.token = nil
                    completion(.failure(error))
                }
            }
            
        }
    }

    private func getSalt(name: String, completion: @escaping (Result<SaltModel, RequestErrors> ) -> Void) {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.appData.wsService.getSalt(salt: name) { result in
                completion(result)
            }
        }
    }
    
    private func getHash(_ password: String, salt: String, prefix:String = "$1$") -> String? {
        if let md5Hash = HashPass.md5Crypt(password, salt: salt, prefix: prefix) {
            return md5Hash.sha1()
        }
        return nil
    }
}

extension String {
    func sha1() -> String {
        let data = Data(self.utf8)
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA1($0.baseAddress, CC_LONG(data.count), &digest)
        }
        let hexBytes = digest.map { String(format: "%02hhx", $0) }
        return hexBytes.joined()
    }
}
