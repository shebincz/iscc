//
//  WSStreamModel.swift
//  StreamCinema
//
//  Created by SCC on 26/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import XMLMapper

final class WSStreamModel:XMLMappable {
    var nodeName: String!

    var status: String?
    var link: String?
    required init(map: XMLMap) {

    }

    func mapping(map: XMLMap) {
        self.link <- map["link"]
        self.status <- map["status"]
    }
}
