//
//  WSFile.swift
//  StreamCinema
//
//  Created by SCC on 26/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Moya
import RxSwift
import XMLMapper

public final class WSFile {
    class func getFile(wsToken:String, ident:String, uuid:UUID, type:String = "video_stream", completion: @escaping (Result<WSStreamModel, RequestErrors>) -> Void) {
        var data:[String:Any] = [:]
        data["device_uuid"] = uuid.uuidString
        data["ident"] = ident
        data["download_type"] = type
        data["wst"] = wsToken
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.appData.wsService.getStreamLink(params: data) { result in
                completion(result)
            }
        }
    }
}
