//
//  WSService.swift
//  StreamCinema
//
//  Created by SCC on 26/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import XMLMapper

final class WSService: Service {
    func processTokenExpireError(_ error: Error) {
        
    }
    
    func start() {

    }

    func stop() {

    }

    func provider() -> MoyaProvider<WSRequest> {
        return Provider.ws
    }

    var errorBehaviour: BehaviorSubject<RequestErrors> = BehaviorSubject(value: .httpError)
    let disposeBag = DisposeBag()
    
    public func getStreamLink(params: [String:Any], completion: @escaping (Result<WSStreamModel, RequestErrors>) -> Void) {
        self.requestData(request: WSRequest.getFile(params:params),
                         apiClient: self.provider()) { result in
                            switch result {
                            case .success(let data):
                                 if let respString = String(data: data, encoding: .utf8) {
                                    let login = XMLMapper<WSStreamModel>().map(XMLString: respString)
                                    if let login = login, login.link != nil {
                                            completion(.success(login))
                                            return
                                        }
                                    }
                                 completion(.failure(.decodeError))
                            case .failure(let err):
                                completion(.failure(err))
                            }
        }
    }
    
    public func logout(device_uuid: UUID, token wst:String, completion: @escaping (Result<Bool, RequestErrors>) -> Void) {
        var data:[String:Any] = [:]
        data["device_uuid"] = device_uuid
        data["wst"] = wst
        self.requestData(request: WSRequest.logout(with: data), apiClient: self.provider()) { result in
            switch result {
            case .success(let value): //
                if let respString = String(data: value, encoding: .utf8) {
                   let resultModel = XMLMapper<ResultEmptyDataModel>().map(XMLString: respString)
                    if let result = resultModel, result.status == "OK" {
                           completion(.success(true))
                           return
                       }
                   }
                completion(.failure(.decodeError))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    public func login(params: [String:Any], completion: @escaping (Result<LoginModel, RequestErrors>) -> Void) {
        self.requestData(request: WSRequest.login(params: params),
                         apiClient: self.provider()) { result in
                            switch result {
                            case .success(let data):
                                 if let respString = String(data: data, encoding: .utf8) {
                                        let login = XMLMapper<LoginModel>().map(XMLString: respString)
                                        if let login = login, login.token != nil {
                                            completion(.success(login))
                                            return
                                        }
                                    }
                                 completion(.failure(.decodeError))
                            case .failure(let err):
                                completion(.failure(err))
                            }
        }
    }
    
    public func getSalt(salt: String, completion: @escaping (Result<SaltModel, RequestErrors>) -> Void) {
        self.requestData(request: WSRequest.getSalt(name: salt),
                         apiClient: self.provider()) { result in
                            switch result {
                            case .success(let data):
                                 if let respString = String(data: data, encoding: .utf8) {
                                        let salt = XMLMapper<SaltModel>().map(XMLString: respString)
                                        if let salt = salt, salt.status == "OK", salt.salt != nil {
                                            completion(.success(salt))
                                            return
                                        }
                                    }
                                 completion(.failure(.decodeError))
                            case .failure(let err):
                                completion(.failure(err))
                            }
        }
    }
    
    public func getUserData(params: [String:Any], completion: @escaping (Result<UserModel, RequestErrors>) -> Void) {
        self.requestData(request: WSRequest.userData(with: params),
                         apiClient: self.provider()) { result in
                            switch result {
                            case .success(let data):
                                if let dataStirng =  String(data: data, encoding: .utf8),
                                    let userData = XMLMapper<UserModel>().map(XMLString: dataStirng) {
                                    completion(.success(userData))
                                    return
                                }
                                completion(.failure(.decodeError))
                            case .failure(let err):
                                completion(.failure(err))
                            }
        }
    }
}
