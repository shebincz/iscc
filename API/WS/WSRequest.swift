//
//  WSRequest.swift
//  StreamCinema
//
//  Created by SCC on 26/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Moya

extension Provider {
    static let ws: MoyaProvider<WSRequest> = NetworkingClient.provider()
}

enum WSRequest: TargetType {
    
    case getSalt(name: String)
    case getFile(params: [String:Any])
    case login(params: [String:Any])
    case userData(with: [String:Any])
    case logout(with: [String:Any])
    
    var baseURL: URL {
        return URLSettings.WSUrl
    }
    
    var method: Moya.Method {
        switch self {
        case .getSalt,
             .getFile,
             .login,
             .userData,
             .logout:
            return .post
        }
    }
    
    var headers: [String: String]? {
        if let uuid = UIDevice.current.identifierForVendor?.uuidString,
            let bundle = Bundle.main.bundleIdentifier,
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,
            let authorization = Bundle.main.infoDictionary?["CFBundleSCC"] as? String{
            let header = ["X-Uuid":uuid, "User-Agent":bundle + "_" + appVersion, "Authorization":"Basic " + authorization]
            return header
        }
        return nil
    }
    
    var path: String {
        switch self {
        case .getSalt:
            return "/salt/"
        case .getFile:
            return "/file_link/"
        case .login:
            return "/login/"
        case .userData:
            return "/user_data/"
        case .logout:
            return "/logout/"
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var parameterEncoding: Moya.ParameterEncoding {
        return PropertyListEncoding.default
    }
    
    var task: Task {
        switch self {
        case .getSalt(let name):
            var dict: [String: String] = [:]
            dict["username_or_email"] = name
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .login(let data),
             .userData(let data),
             .getFile(let data),
             .logout(let data):
            return .requestParameters(parameters: data, encoding: URLEncoding.default)
        }
    }
    
}

