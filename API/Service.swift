//
//  MovieService.swift
//  StreamCinema
//
//  Created by SCC on 23/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import Moya
import RxMoya
import RxSwift

protocol Service: class {

    var disposeBag: DisposeBag { get }
    var errorBehaviour: BehaviorSubject<RequestErrors> { get }

    func processTokenExpireError(_ error: Error)
    func start()
    func stop()
}

extension Service {

    func requestData<Target: TargetType>(request: Target,
                                        apiClient: MoyaProvider<Target>,
                                        completion: @escaping (Result<Data, RequestErrors>) -> Void) {
        apiClient.rx
            .request(request)
            .filterSuccessfulStatusCodes()
            .retry(1)
            .subscribe(onSuccess: { response in
                completion(.success(response.data))
            }, onError: { err in
                completion(.failure(.requestFailed))
            })
            .disposed(by: disposeBag)
    }
    
    func request<T: Model, Target: TargetType>(request: Target,
                                               apiClient: MoyaProvider<Target>,
                                               model: T.Type,
                                               completion: @escaping (Result<T, RequestErrors>) -> Void) {
        
        apiClient.rx
            .request(request)
            .filterSuccessfulStatusCodes()
            .retry(1)
            .map { response -> T in
                do {
                    let decoder = JSONDecoder()
                    return try decoder.decode(T.self, from:  response.data)
                } catch let error {
                    print(error)
                    completion(.failure(.decodeError))
                }
                return model.empty()
            }
            .subscribe(onSuccess: { (models) in
                completion(.success(models))
            }, onError: { [weak self] (error) in
                 guard let self = self else { return }
                completion(.failure(.httpError))
                self.processTokenExpireError(error)
            })
            .disposed(by: disposeBag)
    }
    
    func requestArr<T: Model, Target: TargetType>(request: Target,
                                               apiClient: MoyaProvider<Target>,
                                               model: T.Type,
                                               completion: @escaping (Result<T, RequestErrors>) -> Void) {
        apiClient.rx
            .request(request)
            .filterSuccessfulStatusCodes()
            .retry(1)
            .map { response -> T in
                do {
                    guard var convertedString = String(data: response.data, encoding: String.Encoding.utf8) else {
                        return model.empty()
                    }
                    if convertedString.prefix(1) == "[" {
                        convertedString = "{ \"data\": " + convertedString + "}"
                    }
                    guard let data = convertedString.data(using: .utf8) else { return model.empty() }
                    return try JSONDecoder().decode(T.self, from: data)
                } catch let error {
                    print(error)
                    completion(.failure(.decodeError))
                }
                return model.empty()
            }
            .subscribe(onSuccess: { (models) in
                completion(.success(models))
            }, onError: { [weak self] (error) in
                 guard let self = self else { return }
                completion(.failure(.httpError))
                self.processTokenExpireError(error)
            })
            .disposed(by: disposeBag)
    }

    func requestWithoutMaping<Target: TargetType>(request: Target,
                                                  apiClient: MoyaProvider<Target>,
                                                  completion: @escaping (Result<Bool, RequestErrors>) -> Void) {
        apiClient.rx
            .request(request)
            .filterSuccessfulStatusCodes()
            .retry(1)
            .subscribe(onSuccess: { (response) in
                switch response.statusCode {
                case 201:
                    completion(.success(true))
                case 200:
                    completion(.success(true))
                default:
                    completion(.failure(.httpError))
                }
            }) { [weak self] (error) in
                guard let self = self else { return }
                completion(.failure(.httpError))
                self.processTokenExpireError(error)
            }
        .disposed(by: self.disposeBag)
    }
}

import UIKit
public enum RequestErrors: Error {
    case requestFailed
    case httpError
    case decodeError
    case noMovies
    case noDataForRequest
    
    func handleError(on: UIViewController?) {
        if on == nil {
            return
        }
        let alert = UIAlertController(title: "Error".localizable, message: self.localizedDesc, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK".localizable, style: .cancel, handler: nil))
        on?.present(alert, animated: true, completion: nil)
    }
    
    private var localizedDesc:String {
        get {
            switch self {
            case .decodeError:
                return "decodeError".localizable
            case .httpError:
                return "httpError".localizable
            case .noDataForRequest:
                return "noDataForRequest".localizable
            case .noMovies:
                return "noMovies".localizable
            case .requestFailed:
                return "requestFailed".localizable
            }
        }
    }
}
