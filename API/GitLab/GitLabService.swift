//
//  GitLabService.swift
//  StreamCinema.atv
//
//  Created by SCC on 12/11/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import Moya
import RxSwift

final class ServiceGitLab: Service {
    func processTokenExpireError(_ error: Error) {
        
    }
    
    func start() {

    }

    func stop() {

    }

    func provider() -> MoyaProvider<GitRequest> {
        return Provider.gitRequest
    }

    var errorBehaviour: BehaviorSubject<RequestErrors> = BehaviorSubject(value: .httpError)
    let disposeBag = DisposeBag()

    //MARK: - GitLabReleases
    public func getReleases(completion: @escaping (Result<GitLabRelease, RequestErrors>) -> Void) {
        self.provider().rx
            .request(GitRequest.getVersion)
            .filterSuccessfulStatusCodes()
            .retry(1)
            .map { response -> [GitLabRelease] in
                do {
                    let decoder = JSONDecoder()
                    return try decoder.decode([GitLabRelease].self, from:  response.data)
                } catch {
                    completion(.failure(.decodeError))
                }
                return []
            }
            .subscribe(onSuccess: { (models) in
                if let model = models.first {
                    completion(.success(model))
                }
            }, onError: { _ in
                completion(.failure(.requestFailed))
            })
            .disposed(by: disposeBag)
    }
}
