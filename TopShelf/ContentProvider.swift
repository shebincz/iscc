//
//  ContentProvider.swift
//  TopShelf
//
//  Created by SCC on 26/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import TVServices
import UIKit

class ContentProvider: TVTopShelfContentProvider {
    
    private let scService: TopShelfSC2Service = TopShelfSC2Service()
    var movieItems:[TVTopShelfSectionedItem]?
    var tvShowItems:[TVTopShelfSectionedItem]?
    
    var completition:((TVTopShelfContent?) -> Void)?

    override func loadTopShelfContent(completionHandler: @escaping (TVTopShelfContent?) -> Void) {
        // Fetch content and call completionHandler
        self.completition = completionHandler
        self.getData()
    }
    
    func getData() {
        self.scService.getTopShelfData(wtith: .movie) { [weak self] result in
            guard let self = self else { return }
            self.movieItems = []
            switch result {
            case .success(let movies):
                if let data = movies.data {
                    let filtredData = data.filter({ $0.source?.getInfoLabels().art?.getPoster() != nil })
                    self.movieItems = filtredData.map({ $0.makeSectionItem(.movie) })
                }
            case .failure(_):
                break
            }
            self.completitionData()
        }
        
        self.scService.getTopShelfData(wtith: .tvshow) { [weak self]  result in
            guard let self = self else { return }
            self.tvShowItems = []
            switch result {
            case .success(let tvShows):
                if let data = tvShows.data {
                    let filtredData = data.filter({ $0.source?.getInfoLabels().art?.getPoster() != nil })
                    self.tvShowItems = filtredData.map({ $0.makeSectionItem(.tvshow) })
                }
            case .failure(_):
                break
            }
            self.completitionData()
        }
    }
    
    private func completitionData() {
        guard let movieItems = self.movieItems,
            let tvShowItems = self.tvShowItems else { return }
        
        var content:TVTopShelfSectionedContent? = nil
        var array:[TVTopShelfItemCollection<TVTopShelfSectionedItem>] = []
        if movieItems.count > 0 {
            let itemMovieCollection = TVTopShelfItemCollection(items: movieItems)
            itemMovieCollection.title = "Movies".localizable
            array.append(itemMovieCollection)
        }
        
        if tvShowItems.count > 0 {
            let itemTvShowCollection = TVTopShelfItemCollection(items: tvShowItems)
            itemTvShowCollection.title = "TvShows".localizable
            array.append(itemTvShowCollection)
        }
        
        if array.count > 0 {
            content = TVTopShelfSectionedContent(sections: array )
        }
        
        if let completionHandler = self.completition {
            completionHandler(content)
        }
    }
}

extension InfoData {
    func makeSectionItem(_ type: FilterType) -> TVTopShelfSectionedItem {
        guard let identifier = self.id else { fatalError() }
        let item = TVTopShelfSectionedItem(identifier: identifier)
        item.title = self.source?.getInfoLabels().title
        
        if let imagePath = self.source?.getInfoLabels().art?.getPoster(),
            let url = URL(string: imagePath) {
            item.setImageURL(url, for: .screenScale2x)
        }
        
        var services:String = "?services=none"
        
        if let servicesQuery = self.source?.services?.servicesAsQuery {
            services = servicesQuery
        }

        item.playAction = URL(string: "TopShelf://scc/openIn\(services)&type=\(type.rawValue)&movieID=\(identifier)&startPlaying=true").map { TVTopShelfAction(url: $0) }
        item.displayAction = URL(string: "VLC://scc/openIn\(services)&type=\(type.rawValue)&movieID=\(identifier)&startPlaying=false").map { TVTopShelfAction(url: $0) }

        return item
    }
}

