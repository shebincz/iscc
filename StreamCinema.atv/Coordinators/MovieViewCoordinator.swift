//
//  MovieViewCoordinator.swift
//  StreamCinema.atv
//
//  Created by SCC on 14/11/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

final class MovieViewCoordinator {
    public var movie: InfoData
    public var watched: SccWatched?
    
    private var movieViewController: MovieViewController?
    private var playerCoordinator: VLCCoordinator?
//    private var vlcController: VLCPlayerViewController?
    private var castViewController: CastCollectionView?
    
    private var relatedCSFDIDs:[String] = []
    
    init(rootViewController: UIViewController, movie: InfoData) {
        self.movie = movie
        self.watched = self.getWatched()
        
        let board = rootViewController.storyboard ?? UIStoryboard(name: "Main", bundle: nil)
        guard let controller = board.instantiateViewController(withIdentifier: "MovieViewController") as? MovieViewController else {
            fatalError()
        }
        self.movieViewController = controller
        self.movieViewController?.movie = movie
        self.movieViewController?.delegate = self
        if let state = self.watched?.state,
           state == .paused {
            self.movieViewController?.showContinueButton = true
        }
        rootViewController.present(controller, animated: true, completion: nil)
        self.getRelated()
    }
    
    deinit {
        print("MovieViewController deinit")
    }
    
    private func getRelated() {
        guard let csfdID = self.movie.source?.services?.csfd,
              let type = self.movie.source?.infoLabels?.getMediaType() else { return }
        let csfdParser = CSFD()
        csfdParser.getRelated(with: csfdID, isTvShow: type == .tvshow) { result in
            switch result {
            case .success(let csfdIds):
                self.relatedCSFDIDs = csfdIds
            case .failure(let err):
                print(err)
            }
        }
    }
    
    private func prepareStreams(isContinue: Bool) {
        self.watched = self.getWatched()
        self.playerCoordinator = VLCCoordinator()
        self.playerCoordinator?.delegate = self
        if let movieViewController = self.movieViewController {
            self.playerCoordinator?.set(movieViewController, startAt: 0, isContrinue:isContinue)
        }
    }
    
    private func getWatched() -> SccWatched {
        if self.watched == nil,
            let id = self.movie.id {
            self.watched = SccWatchedData.getWatched(by: id)
        }
        if var watched = self.watched {
            watched.traktID = self.movie.source?.services?.trakt
            return watched
        }
        return self.getEmptyWatched()
    }
    
    private func getEmptyWatched() -> SccWatched {
        if let movieID = self.movie.id {
            return SccWatched(isSeen: false, time: 0, lastUpdated: Date(),
                              traktID: self.movie.source?.services?.trakt,
                              movieID: movieID, tvShowID: nil,
                              sessionNumber: nil, seasonID: nil,
                              episodeNumber: nil, episodeID: nil)
        }
        fatalError()
    }
    
    private func showCast() {
        if let cast = self.movie.source?.cast {
            let controller = UIViewController()
            controller.view.backgroundColor = .clear
            controller.modalTransitionStyle = .coverVertical
            controller.modalPresentationStyle = .blurOverFullScreen
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            layout.itemSize = CGSize(width: 240, height: 280)
            layout.scrollDirection = .vertical

            self.castViewController = CastCollectionView(frame:controller.view.bounds, collectionViewLayout: layout)
            self.castViewController?.cast = cast
            self.castViewController?.castDelegate = self
            controller.view.addSubview(self.castViewController!)
            
            self.movieViewController?.present(controller, animated: true, completion: nil)
        }
    }
    
    private func showRelatedMovie() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
           self.relatedCSFDIDs.count > 0 {
            appDelegate.appData.scService.getCsfdMedia(for: relatedCSFDIDs, type: .movie, page: 1, sort: .news) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(var data):
                    data.data?.sort(by: { (left, right) -> Bool in
                        if let leftY = left.source?.infoLabels?.year,
                           let rightY = right.source?.infoLabels?.year {
                            return leftY < rightY
                        }
                        return true
                    })
                    self.showMovieController(with: nil, or: data)
                case .failure(let err):
                    err.handleError(on: self.movieViewController)
                }
            }
        }
    }
    private func showMovieController(with cast:Cast?, or model:FilterResult?) {
        if let actorVC = self.movieViewController?.storyboard!.instantiateViewController(withIdentifier: "ActorMoviesViewController") as? ActorMoviesViewController {
            if let cast = cast {
                actorVC.prepareData(with: cast)
            } else if let model = model,
                      let title = self.movie.source?.getInfoLabels().title {
                actorVC.prepareData(with: model, releated: title)
                self.movieViewController?.stopPlayingTrailer()
            }
            self.movieViewController?.present(actorVC, animated: true) {
                
            }
        }
    }
}

extension MovieViewCoordinator: VLCCoordinatorDelegate {
    func numberOfVideos(_ coordinator: VLCCoordinator) -> Int {
        return 1
    }
    
    func coordinator(_ coordinator: VLCCoordinator, data atIndex: Int) -> InfoData {
        return self.movie
    }
    
    func coordinator(_ coordinator: VLCCoordinator, watched forData: InfoData) -> SccWatched? {
        return self.watched ?? self.getWatched()
    }
    
    func coordinator(_ coordinator: VLCCoordinator, infoPanel forData: InfoData) -> InfoPanelData {
        if let infoLabels = self.movie.source?.getInfoLabels() {
            return InfoPanelData(title: infoLabels.title ?? "",
                                 desc: infoLabels.plot ?? "",
                                 posterURL: infoLabels.art?.poster,
                                 bannerURL: infoLabels.art?.banner,
                                 clearLogoURL: infoLabels.art?.clearlogo)
        }
        return InfoPanelData.empty()
    }
    
    func coordinator(_ coordinator: VLCCoordinator, update watched: SccWatched) {
        self.watched = watched
        self.movieViewController?.showContinueButton = true
    }
}

extension MovieViewCoordinator: CastCollectionViewDelegate {
    func castCollectionView(_ castView: CastCollectionView, didSelect cast: Cast) {
        self.movieViewController?.dismiss(animated: false, completion: {
            self.showMovieController(with: cast, or: nil)
        })
    }
}

extension MovieViewCoordinator: MovieViewDelegate {
    func movieView(_ movieView: MovieViewController, prerform action: MovieViewAction) {
        switch action {
        case .playMovie:
            self.prepareStreams(isContinue: false)
        case .continueMovie:
            self.prepareStreams(isContinue: true)
        case .showCast:
            self.showCast()
        case .showRelated:
            if self.relatedCSFDIDs.count > 0 {
                self.showRelatedMovie()
            }
        }
    }
}
