//
//  AppDelegate.swift
//  StreamCinema.atv
//
//  Created by SCC on 23/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import CoreData
import os.log
import TraktKit
import Bugsnag

extension Notification.Name {
    static let TraktSignedIn = Notification.Name(rawValue: "TraktSignedIn")
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    public let appData = AppData()
    public var loginManager: LoginManager?
    private let tvCoordinator:TVStationsCoordinator = TVStationsCoordinator()
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Bugsnag.start()
        KSPlayerSettings.configure()
        TraktManager.sharedManager.set(clientID: Constants.clientId,
                                        clientSecret: Constants.clientSecret,
                                        redirectURI: Constants.redirectURI)
        let tabBarController = self.window!.rootViewController as! UITabBarController
        var endpoints: [AppTabs] = [.movies, .tvShows, .search, .fullTextSearch, .tvProgram, .tv, .settings]
        if TraktManager.sharedManager.isSignedIn {
            endpoints = [.movies, .tvShows, .trakt, .fullTextSearch, .tvProgram, .tv, .settings]
        }
        let viewControllers = endpoints.map { (endpoint) -> UIViewController in
            if endpoint == .search {
                let movieVC = tabBarController.storyboard!.instantiateViewController(withIdentifier: "AZSearchController") as! AZSearchController
                movieVC.title = endpoint.description
                return movieVC
            }
            if endpoint == .tvProgram {
                return self.tvCoordinator.start(endpoint: endpoint)
            }
            if endpoint == .settings {
                let settings = SettingsViewController(style: .grouped)
                let menu: MenuViewController = MenuViewController()
                menu.endpoit = endpoint
                menu.menuDelegate = settings
                let split = UISplitViewController()
                split.viewControllers = [menu, settings]
                split.title = endpoint.description
                split.preferredPrimaryColumnWidthFraction = 2/7
                return split
            }
            if endpoint == .fullTextSearch {
                let searchController = FullSearchController.createSearch(storyboard: tabBarController.storyboard)
                searchController.title = endpoint.description
                return searchController
            }
            if endpoint == .tv {
                let tvController = TVViewController(style: .grouped)
                tvController.title = endpoint.description
                return tvController
            }
            let movieVC = tabBarController.storyboard!.instantiateViewController(withIdentifier: "MovieListViewController") as! MovieListViewController
            let menu: MenuViewController = MenuViewController()
            menu.endpoit = endpoint
            menu.menuDelegate = movieVC
            let split = UISplitViewController()
            split.viewControllers = [menu, movieVC]
            split.title = endpoint.description
            split.preferredPrimaryColumnWidthFraction = 2/7
            return split
                
        }
        tabBarController.viewControllers = viewControllers
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if TraktManager.sharedManager.isSignedIn {
            TraktManager.sharedManager.checkToRefresh { isRefreshed in
                print("TraktManager checkToRefresh \(isRefreshed ? "TRUE":"FALSE")")
            }
        }
        self.appData.isccService.connect { result in
            switch result {
            case .success(_):
                print("applicationDidBecomeActive isccService success")
            case .failure(_):
                print("applicationDidBecomeActive isccService failure")
            }
        }
        self.appData.loginManager.connectWithExistingData { result in }
        
        appData.gitLabService.getReleases { result in
            switch result {
            case .success(let release):
                print("Last released: \(release)")
                self.chceck(release.name, desc: release.welcomeDescription)
            case .failure(_):
                break
            }
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        os_log("Will open URL from Top Shelf. URL=%@", url as NSURL)
        
        if let params = url.queryParameters,
            let startPlayingString = params["startPlaying"],
            let startPlaying = startPlayingString.bool,
            let typeString = params["type"],
            let type = FilterType(rawValue: typeString),
            let movieID = params["movieID"],
            let tabBar = self.window!.rootViewController as? TabBarController {

            tabBar.presentDetailFormOpenIn(type: type, movieID: movieID, startPlaying: startPlaying)
        }
        return false
    }
    
    lazy var persistentContainer: NSPersistentContainer = {

        let container = NSPersistentContainer(name: "WatchedList")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error {

                fatalError("Unresolved error, \((error as NSError).userInfo)")
            }
        })
        return container
    }()
}

extension AppDelegate {
    private struct Constants {
        static let clientId = "0dc11b34ed91a94fb03ecf3cd2b7d659d2ec1c22e9d78592da76f136320b40ce"
        static let clientSecret = "6e8bb0e23b73f15220c1fcc62b13a00c9bdcba663fe8247632e888d19c387f4c"
        static let redirectURI = "scctrakt://auth/trakt"
    }
    
    private func chceck(_ version:String, desc: String) {
        guard let vc = self.window?.rootViewController,
              let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else { return }
        
        if !version.contains(appVersion) {
            var message = String(format: "new_version_message".localizable, appVersion,version)
            message = message + "\n" + desc.replacingOccurrences(of: "\n\n", with: "\n")
            if MobileProvision.read()?.entitlements.identifier == "5PUJGSG8GM" {
               message = message + "\n http://itsvet.net/KODI/ATV_SCC/"
            }
            let alert = UIAlertController(title: "new_version".localizable, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "button_continue".localizable, style: .cancel, handler: nil))
            
            vc.present(alert, animated: true, completion: nil)
        }
    }
}

extension URL {
    public var queryParameters: [String: String]? {
        guard
            let components = URLComponents(url: self, resolvingAgainstBaseURL: true),
            let queryItems = components.queryItems else { return nil }
        return queryItems.reduce(into: [String: String]()) { (result, item) in
            result[item.name] = item.value
        }
    }
}

extension String {
    var bool: Bool? {
        switch self.lowercased() {
        case "true", "t", "yes", "y", "1":
            return true
        case "false", "f", "no", "n", "0":
            return false
        default:
            return nil
        }
    }
}
