//
//  MenuItem.swift
//  StreamCinema.atv
//
//  Created by SCC on 28/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

enum MenuItem {
    case popular
    case trending
    case mostWatched
    case news
    case dubbed
    case lastAdded
    case watched
    case traktWatchList
    case genre
    case traktHistory

    var description: String {
        switch  self {
        case .mostWatched:
            return "most_watched".localizable
        case .dubbed:
            return "new-dubbed-releases".localizable
        case .trending:
            return "trending".localizable
        case .lastAdded:
            return "recently-added".localizable
        case .news:
            return "new-releases".localizable
        case .popular:
            return "popular".localizable
        case .watched:
            return "watch-history".localizable
        case .traktWatchList:
            return "watch_list".localizable
        case .traktHistory:
            return "watch-history".localizable
        case .genre:
            return "genre".localizable
        }
    }
}

enum SettingsMenuItem {
    case webshare
    case trakt
    case openSubtitles
    case subtitles
    
    var description: String {
        switch self {
        case .webshare:
            return "Webshare"
        case .trakt:
            return "Trakt.tv"
        case .openSubtitles:
            return "OpenSubtitles"
        case .subtitles:
            return "subtitles-menu-item".localizable
        }
    }
}
