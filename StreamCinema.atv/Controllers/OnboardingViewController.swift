//
//  TabBarController.swift
//  StreamCinema.atv
//
//  Created by SCC on 03/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

final class OnboardingViewController: UIViewController {
    @IBOutlet var buttonLogin: UIButton!
    @IBOutlet var titleLabel: UILabel!
    private var alert:UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }

    @IBAction func buttonLoginPressed() {
        self.login()
    }
    
    private func configureView() {
        self.buttonLogin.setTitle("button_logon".localizable, for: .normal)
        self.titleLabel.text = "title_logonScreen".localizable
    }
    
    private func showNeedCredentials(message: String? = "to-watch-the-content-you-need-to-have-a-vip-accoun".localizable) {
        alert = UIAlertController(title: "missing-provider-credentials".localizable,
                                       message: message ,
                                       preferredStyle: .alert)
        alert?.addTextField(configurationHandler: { textField in
            textField.placeholder = "username".localizable
        })
        alert?.addTextField(configurationHandler: { textField in
            textField.isSecureTextEntry = true
            textField.placeholder = "password".localizable
        })
        
        let action = UIAlertAction(title: "button_continue".localizable, style: .default) { [weak self] _ in
            guard let self = self else { return }
            let name = self.alert?.textFields?[0].text
            let pass = self.alert?.textFields?[1].text
            self.login(name: name, pass: pass)
        }
        
        alert?.addAction(action)
//        alert?.addAction(UIAlertAction(title: nil, style: .cancel, handler: nil))
        self.present(alert!, animated: true, completion: nil)
    }
    
    private func login(name: String? = nil, pass: String? = nil) {
        
        LoginManager().connect(name: name, pass: pass, completion: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(_):
                if let alert = self.alert {
                    alert.dismiss(animated: false, completion: {
                        self.dismiss(animated: true, completion: nil)
                    })
                    return
                }
                self.dismiss(animated: true, completion: nil)
            case .failure(let error):
                if error == .noDataForRequest {
                    self.showNeedCredentials()
                } else {
                    self.showNeedCredentials(message: "bad_credentials".localizable)
                }
            }
        })
    }
}
