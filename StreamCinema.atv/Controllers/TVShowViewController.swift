//
//  TWShowViewController.swift
//  StreamCinema.atv
//
//  Created by SCC on 30/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Kingfisher
import QuartzCore
import KSPlayer

enum UIImageViewCashedType {
    case banner
    case poster
    case emptyLoading
}

extension UIImageView {
    func setCashedImage(url:URL, type:UIImageViewCashedType, completionHandler: ((Result<RetrieveImageResult, KingfisherError>) -> Void)? = nil) {
        var image:UIImage?
        if type == .banner {
            image = UIImage(named: "banner")
        } else if type == .poster {
            image = UIImage(named: "poster")
        }
        let processor = DownsamplingImageProcessor(size: self.bounds.size)
        self.kf.indicatorType = .none
        self.kf.setImage(
            with: url,
            placeholder: image,
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(0.5))
            ],
            completionHandler:
                {
                    result in
                    if let completionHandler = completionHandler {
                        completionHandler(result)
                    }
                    switch result {
                    case .success(let value):
                        print("Task done for: \(value.source.url?.absoluteString ?? "")")
                    case .failure(let error):
                        print("Job failed: \(error.localizedDescription)")
                    }
                })
    }
}

extension UIStackView {
    func removeAllArrangedSubviews() {
        let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            return allSubviews + [subview]
        }
        
        // Deactivate all constraints
        NSLayoutConstraint.deactivate(removedSubviews.flatMap({ $0.constraints }))
        
        // Remove the views from self
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
}

struct TvShowModel {
    let tvshowID: String?
    var sessionCount: Int?
    var data:[InfoData]?
    var isInitializetWithoutSession:Bool?
    
    static func initWith( data: [InfoData]?, tvshowID:String?) -> TvShowModel {
        if (data?.count ?? 0) > 0,
            (data?.first?.source?.infoLabels?.episode ?? 0) != 0 {
            return TvShowModel(tvshowID: tvshowID, sessionCount: 1, data: nil, isInitializetWithoutSession:true)
        }
        return TvShowModel(tvshowID: tvshowID, sessionCount: data?.count, data: data, isInitializetWithoutSession:false)
    }
    
    func getName(for index:Int) -> String {
        if let season = self.data?[index].source?.infoLabels?.season {
            return "\(season)"
        }
        return "0"
    }
}

final class TVShowSesionCell: UICollectionViewCell {
    @IBOutlet var sesionTitle: UILabel!
    
    override var isSelected: Bool {
        willSet {
            if newValue {
                self.sesionTitle.backgroundColor = UIColor.white.withAlphaComponent(0.3)
                self.sesionTitle.textColor = .label
            } else {
                self.sesionTitle.backgroundColor = .clear
                self.sesionTitle.textColor = .secondaryLabel
            }
            self.setupLabel()
        }
    }
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if self.isFocused {
            self.sesionTitle.layer.borderWidth = 4
            self.sesionTitle.textColor = .white
        } else if self.isSelected {
            self.sesionTitle.textColor = .label
            self.sesionTitle.layer.borderWidth = 0
        } else {
            self.sesionTitle.textColor = .secondaryLabel
            self.sesionTitle.layer.borderWidth = 0
        }
        self.setupLabel()
    }
    
    private func setupLabel() {
        self.sesionTitle.clipsToBounds = true
        self.sesionTitle.layer.masksToBounds = true
        self.sesionTitle.layer.cornerRadius = 16
        self.sesionTitle.layer.borderColor = UIColor.lightGray.cgColor
    }
}

final class TVShowEpisodeCell: UITableViewCell {
    @IBOutlet var episodeName: UILabel!
    @IBOutlet var stateView: UIImageView!
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        super.didUpdateFocus(in: context, with: coordinator)
                if context.nextFocusedView === self {
                    coordinator.addCoordinatedAnimations({
                        self.episodeName.textColor = .black
                    }, completion: nil)
                }
                else {
                    coordinator.addCoordinatedAnimations({
                        self.episodeName.textColor = .label
                    }, completion: nil)
                }
    }
}

final class TVShowViewController: UIViewController {
    
    @IBOutlet var bannerImageView: UIImageView!
    @IBOutlet var sesionNameTitleLabel: UILabel!
    @IBOutlet var seriesCollectionView: UICollectionView!
    @IBOutlet var contentVerticalStackView: UIStackView!
    @IBOutlet var episodesTebleView: UITableView!
    @IBOutlet var acitivyView: UIActivityIndicatorView!
    
    private var videoCoordinator: VLCCoordinator?
    private let sessionDown:UIFocusGuide = UIFocusGuide()
    private let sessionUP:UIFocusGuide = UIFocusGuide()
    public var selectedSession: Int = 0
    public var selectedEpisode: Int = 0
    
    public var tvShow: InfoData? {
        didSet {
            self.getSessionInfo()
        }
    }
    
    private var seletedSeasonData: [InfoData]?
    private var model: TvShowModel?
    
    var watched: [SccWatched]? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    private func setupView() {
        if let info = self.tvShow?.source?.getInfoLabels(),
            let banner = info.art?.banner ?? info.art?.fanart,
            let url = URL(string: banner) {
            
            self.bannerImageView.setCashedImage(url: url, type: .banner)
        }
        
        self.seriesCollectionView.delegate = self
        self.seriesCollectionView.dataSource = self
        
        self.episodesTebleView.dataSource = self
        self.episodesTebleView.delegate = self
        self.setupFocus()
        self.sesionNameTitleLabel.text = "season".localizable
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func setupFocus() {
        self.view.addLayoutGuide(self.sessionDown)
        self.view.addLayoutGuide(self.sessionUP)
        
        self.sessionDown.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.sessionDown.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.sessionDown.heightAnchor.constraint(equalToConstant: 2).isActive = true
        self.sessionDown.topAnchor.constraint(equalTo: self.seriesCollectionView.bottomAnchor, constant: 0).isActive = true
        
        self.sessionDown.preferredFocusEnvironments = [self.episodesTebleView]
        
        self.sessionUP.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.sessionUP.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.sessionUP.heightAnchor.constraint(equalToConstant: 2).isActive = true
        self.sessionUP.topAnchor.constraint(equalTo: self.sessionDown.bottomAnchor, constant: 0).isActive = true
        
        self.sessionUP.preferredFocusEnvironments = [self.seriesCollectionView]
    }
    
    private func showEpisodeData(at indexPath: IndexPath) {
        if let data = seletedSeasonData {
            let episode = data[indexPath.row]
            let label = UILabel()
            label.numberOfLines = 0
            label.text = episode.source?.getInfoLabels().plot
            self.contentVerticalStackView.removeAllArrangedSubviews()
            self.contentVerticalStackView.addArrangedSubview(label)
        }
    }
    
    private func showAlert(for streams:VideoStreams) {
        let alert = UIAlertController(title: "choose-the-stream".localizable, message: nil, preferredStyle: .alert)
        let autoSelect = streams.isAutoSelectVideo()
        for  stream in streams.getSortedData() {
            let text = stream.getStreamInfo(isSselected: (autoSelect?.id ?? "") == stream.id)
            let action = UIAlertAction(title: text, style: .default) { _ in
                stream.getLink { [weak self] result in
                    guard let self = self else { return }
                    switch result {
                    case .failure(let error):
                        error.handleError(on: self)
                    case .success(let data):
                        if let link = data.link,
                            let videoURL = URL(string: link) {
                            self.playVideo(at: videoURL)
                        }
                    }
                }
            }
            alert.addAction(action)
        }
        alert.addAction(UIAlertAction(title: "button_cancel".localizable, style: .cancel, handler: nil))
        self.show(alert, sender: self)
    }
    
    private func playVideo(at url:URL) {
        let playerViewController = PlayerViewController.instantiate()
        playerViewController.resource = KSPlayerResource(url: url)
        self.present(playerViewController, animated: true) { [weak self] in
            guard let self = self else { return }
            if let episodeData = self.seletedSeasonData?[self.selectedEpisode],
                let season = episodeData.source?.infoLabels?.season,
                let episode = episodeData.source?.infoLabels?.episode {
                playerViewController.play(watched: self.getWatched(for: Int64(season),
                                                                         episode: Int64(episode)),
                                          mediaInfo: episodeData, isContinue: true)
            }
        }
    }
    
    private func getEpisodeState(infoData: InfoData) -> WatchedState {
        if let season = infoData.source?.infoLabels?.season,
            let episode = infoData.source?.infoLabels?.episode {
            let watched = self.getWatched(for: Int64(season), episode: Int64(episode))
            return watched.state
        }
        return .none
    }
    
    private func getWatched(for season:Int64, episode:Int64) -> SccWatched {
        if self.watched == nil {
            let seasionID = self.model?.data?[self.selectedSession].id
            self.watched = SccWatchedData.getSeasonWatched(by: seasionID)
        }
        if let watched = self.watched {
            let currentSeason = watched.filter({ ($0.seasonNumber ?? -1) == season })
            if let current = currentSeason.filter({ ($0.episodeNumber ?? -1) == episode }).first {
                return current
            }
            let empty = self.getEmptyWatched(for: season, episode: episode)
            self.watched?.append(empty)
            return empty
        }
        fatalError()
    }
    
    /// data su OK
    private func getEmptyWatched(for season:Int64, episode:Int64) -> SccWatched {
        if let tvShowID = tvShow?.id,
            let episodeData = self.seletedSeasonData?[self.selectedEpisode],
            let episodeID = episodeData.id {
            
            let index = self.selectedSession
            let seasonInfo = index <= (self.model?.data?.count ?? 0) ? self.model?.data?[index] : nil
            let seasonID = seasonInfo?.id
            
            return SccWatched(isSeen: false, time: 0, lastUpdated: Date(),
                              traktID: episodeData.source?.services?.trakt,
                              movieID: nil, tvShowID: tvShowID,
                              sessionNumber: episodeData.source?.infoLabels?.season,
                              seasonID: seasonID,
                              episodeNumber: episodeData.source?.infoLabels?.episode,
                              episodeID: episodeID)
        }
        fatalError()
    }
    
    private func isExistSession(number:Int) -> Bool {
        if let sessionCount = self.model?.sessionCount,
            number <= sessionCount {
            return true
        }
        return false
    }
}

extension TVShowViewController: VLCCoordinatorDelegate {
    func coordinator(_ coordinator: VLCCoordinator, update watched: SccWatched) {
        guard var watchedArray = self.watched else { return }
        var isUpdated:Bool = false
        for i in 0...watchedArray.count - 1 {
            if watchedArray[i].seasonNumber == watched.seasonNumber,
               watchedArray[i].episodeNumber == watched.episodeNumber {
                watchedArray[i] = watched
                isUpdated = true
                break
            }
        }
        if isUpdated == false {
            watchedArray.append(watched)
        }
        self.watched = watchedArray
        self.episodesTebleView.reloadData()
    }
    
    func numberOfVideos(_ coordinator: VLCCoordinator) -> Int {
        if let count = self.seletedSeasonData?.count {
            return count
        }
        return 0
    }
    
    func coordinator(_ coordinator: VLCCoordinator, data atIndex: Int) -> InfoData {
        if let episodes = self.seletedSeasonData,
            episodes.count > atIndex {
            return episodes[atIndex]
        }
        return InfoData.empty()
    }
    
    func coordinator(_ coordinator: VLCCoordinator, watched forData: InfoData) -> SccWatched? {
        if self.watched == nil {
            let seasionID = self.model?.data?[self.selectedSession].id
            self.watched = SccWatchedData.getSeasonWatched(by: seasionID)
        }
        if var currentWatched = self.watched?.first(where: { $0.episodeID == forData.id }) {
            if currentWatched.traktID == nil {
                currentWatched.traktID = forData.source?.services?.trakt
            }
            return currentWatched
        }

        if let tvShowID = self.tvShow?.id,
            let episodeID = forData.id,
            let season = forData.source?.infoLabels?.season,
            let episode = forData.source?.infoLabels?.episode  {
            
            let index = self.selectedSession
            let seasonInfo = index <= (self.model?.data?.count ?? 0) ? self.model?.data?[index] : nil
            let seasonID = seasonInfo?.id
            
            let watched = SccWatched(isSeen: false,
                              time: 0,
                              lastUpdated: Date(),
                              traktID: self.tvShow?.source?.services?.trakt,
                              movieID: nil,
                              tvShowID: tvShowID,
                              sessionNumber: season, seasonID: seasonID,
                              episodeNumber: episode, episodeID: episodeID)
            self.watched?.append(watched)
            return watched
        }
        return nil
    }
    
    func coordinator(_ coordinator: VLCCoordinator, infoPanel forData: InfoData) -> InfoPanelData {
        var panelData = InfoPanelData(title: "", desc: "", posterURL: nil, bannerURL: nil, clearLogoURL: nil)
        
        if let title = forData.source?.getInfoLabels().title,
            let desc = forData.source?.getInfoLabels().plot {
            panelData.title = title
            panelData.desc = desc
        }
        if let banner = forData.source?.getInfoLabels().art?.banner,
         !banner.isEmpty {
            panelData.bannerURL = banner
        }
        if let poster = forData.source?.getInfoLabels().art?.poster,
         !poster.isEmpty {
            panelData.posterURL = poster
        }
        if let clearLogo = forData.source?.getInfoLabels().art?.clearlogo,
         !clearLogo.isEmpty {
            panelData.clearLogoURL = clearLogo
        }
        if panelData.posterURL == nil {
            panelData.posterURL = self.tvShow?.source?.getInfoLabels().art?.poster
        }
        if panelData.bannerURL == nil {
            panelData.bannerURL = self.tvShow?.source?.getInfoLabels().art?.banner
        }
        if panelData.clearLogoURL == nil {
            panelData.clearLogoURL = self.tvShow?.source?.getInfoLabels().art?.clearlogo
        }
        return panelData
    }
    
    private func getSessiondData(at row:Int) {
        self.acitivyView.startAnimating()
        self.selectedSession = row
        if let series = self.model?.data, series.count > 0,
            let serieID = series[row].id,
            let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.appData.scService.getSeries(for: serieID, page: 1) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .failure(let error):
                    error.handleError(on: self)
                case .success(let filterResult):
                    self.seletedSeasonData = filterResult.data
                    self.episodesTebleView.reloadData()
                    if (filterResult.data?.count ?? 0) > 0 {
                        let path = IndexPath(row: 0, section: 0)
                        self.episodesTebleView.selectRow(at: path, animated: true, scrollPosition: .top)
                        self.showEpisodeData(at: path)
                    }
                }
                self.acitivyView.stopAnimating()
            }
        }
    }
}


extension TVShowViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let data = seletedSeasonData {
            return data.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TVShowEpisodeCell") as? TVShowEpisodeCell else { return UITableViewCell()}
        if let data = seletedSeasonData {
            let episode = data[indexPath.row]
            switch self.getEpisodeState(infoData: episode) {
            case .none:
                cell.stateView.image = nil
            case .paused:
                cell.stateView.image = UIImage(systemName: "pause.fill")
            case .done:
                cell.stateView.image = UIImage(systemName: "checkmark.seal.fill")
            }
            if let titleString = episode.source?.getInfoLabels().title,
                let audioLang = episode.source?.availableStreams?.getAudio(),
                let episode = episode.source?.infoLabels?.episode {
                
                let title = NSMutableAttributedString(string: "\(episode) - \(titleString) ")
                title.setAttributes([NSAttributedString.Key.foregroundColor:UIColor.label],
                                    range: NSRange(location: 0, length: title.length))
                
                let audioString = audioLang.map { (lang) -> String in return lang.name }.sorted { $0 < $1 }.joined(separator: ",")

                let langString = NSMutableAttributedString(string: audioString)
                langString.setAttributes([NSAttributedString.Key.foregroundColor:UIColor.secondaryLabel,
                                          NSAttributedString.Key.font:UIFont.systemFont(ofSize: 24)],
                                    range: NSRange(location: 0, length: langString.length))
                
                title.append(langString)
                
                cell.episodeName.attributedText = title
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.showEpisodeData(at: indexPath)
        self.selectedEpisode = indexPath.row
        self.videoCoordinator = VLCCoordinator()
        self.videoCoordinator?.delegate = self
        self.videoCoordinator?.set(self, startAt: indexPath.row, isContrinue: false)
    }
    
    func tableView(_ tableView: UITableView, didUpdateFocusIn context: UITableViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if let indexPath = context.nextFocusedIndexPath {
            self.showEpisodeData(at: indexPath)
        }
    }
}


extension TVShowViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = self.model?.sessionCount {
            return count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TVShowSesionCell", for: indexPath) as? TVShowSesionCell else {return UICollectionViewCell()}

        if let name = self.model?.getName(for: indexPath.row) {
            cell.sesionTitle.text = name
        }
        cell.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.getSessiondData(at: indexPath.row)
    }
}

//MARK: - tvshow info
extension TVShowViewController {
    private func getSessionInfo() {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate,
            let mediaID = tvShow?.id else { return }
        delegate.appData.scService.getSeries(for: mediaID, page: 1) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                error.handleError(on: self)
            case .success(let filterResult):
                self.model = TvShowModel.initWith(data: filterResult.data, tvshowID: self.tvShow?.id)
                self.seriesCollectionView?.reloadData()
                if self.model?.isInitializetWithoutSession == true {
                    self.seletedSeasonData = filterResult.data
                    self.episodesTebleView.reloadData()
                } else {
                    let path = IndexPath(row: self.selectedSession, section: 0)
                    self.seriesCollectionView?.selectItem(at: path, animated: false, scrollPosition: .centeredHorizontally)
                    self.seriesCollectionView?.delegate?.collectionView?(self.seriesCollectionView, didSelectItemAt: path)
                }
            }
        }
    }
}
