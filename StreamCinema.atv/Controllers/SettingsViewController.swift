//
//  SettingsViewController.swift
//  StreamCinema.atv
//
//  Created by SCC on 02/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import TraktKit

protocol SettingsInputTextCellDelegate {
    func settingsInput(_ cell: SettingsInputTextCell, type:CellTypes, set value: String)
}

class SettingsCell: UITableViewCell {
    public var type:CellTypes?
    public func become() { }
}

final class SettingsInputTextCell: SettingsCell {
    private let name: UILabel =  UILabel()
    private let valueLabel: UILabel = UILabel()
    private let textField: UITextField = UITextField()
    
    public var inputDelegate:SettingsInputTextCellDelegate?
    override public var type:CellTypes? {
        willSet {
            if let type = newValue {
                switch type {
                case .password:
                    self.textField.isEnabled = true
                    self.textField.isSecureTextEntry = true
                case .userName:
                    self.textField.isEnabled = true
                    self.textField.isSecureTextEntry = false
                default:
                    self.textField.isEnabled = false
                }
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupView()
    }
    
    public func set(name: String) {
        self.name.text = name
    }
    
    public func set(value: String) {
        if let type = self.type, type == .password {
            self.valueLabel.text = String(value.map { _ in return "•" })
        } else {
            self.valueLabel.text = value
        }
    }
    
    private func setupView() {
        self.addSubview(self.name)
        self.addSubview(self.valueLabel)
        self.addSubview(self.textField)
        self.textField.delegate = self
        
        self.name.translatesAutoresizingMaskIntoConstraints = false
        self.valueLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.name.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        self.name.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        self.name.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        self.name.widthAnchor.constraint(equalToConstant: 600).isActive = true
        
        self.valueLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        self.valueLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 8 ).isActive = true
        self.valueLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8).isActive = true
        self.valueLabel.leftAnchor.constraint(equalTo: self.name.rightAnchor, constant: 8).isActive = true

    }
    
    override public func become() {
        self.textField.becomeFirstResponder()
    }
    
    static func getCell(_ tableView: UITableView) -> SettingsInputTextCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsInputTextCell") as? SettingsInputTextCell {
            return cell
        }
        return SettingsInputTextCell(style: .default, reuseIdentifier: "SettingsInputTextCell")
    }
}

extension SettingsInputTextCell: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let type = self.type, let text = textField.text {
            self.inputDelegate?.settingsInput(self, type: type, set: text)
        }
    }
}

final class ButtonCell: SettingsCell {
    private let title: UILabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupView()
    }
    
    private func setupView() {
        self.addSubview(self.title)
        self.title.textAlignment = .center
        self.title.translatesAutoresizingMaskIntoConstraints = false
        self.title.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        self.title.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        self.title.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        self.title.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
    }
    
    public func set(text:String) {
        self.title.text = text
    }
    
    static func getCell(_ tableView: UITableView) -> ButtonCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell") as? ButtonCell {
            return cell
        }
        return ButtonCell(style: .default, reuseIdentifier: "ButtonCell")
    }
}

final class InfoCell: SettingsCell {
    private let title: UILabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupView()
    }
    
    private func setupView() {
        self.addSubview(self.title)
        self.title.translatesAutoresizingMaskIntoConstraints = false
        self.title.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        self.title.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        self.title.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        self.title.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
    }
    
    public func set(text:String) {
        self.title.text = text
    }
    
    static func getCell(_ tableView: UITableView) -> InfoCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "InfoCell") as? InfoCell {
            return cell
        }
        return InfoCell(style: .default, reuseIdentifier: "InfoCell")
    }
}

struct Credentials {
    var name: String
    var pass: String
}

enum SettingsSections: Int {
    case wsCredentials
    case wsInfo
    case other
    case traktCredentials
    case openSubtitlesCredentials
    case subtitlesFontSettings
    
    var headerTitle:String {
        get {
            switch self {
            case .wsCredentials:
                return "provider-settings".localizable
            case .wsInfo:
                return "provider-details".localizable
            case .other:
                return "other_app_settings".localizable
            case .traktCredentials:
                return "trakt-credentials".localizable
            case .openSubtitlesCredentials:
                return "opensubtitles-credentials".localizable
            case .subtitlesFontSettings:
                return "subtitles-font-settings".localizable
            }
        }
    }
}

enum CellTypes {
    case userName
    case password
    case vipExpiration
    case resetWatchedhistory
    case resetButton
    case wsLogout
    case changeButton
    case wsSpeedTestResult
    case wsSpeedTestButton
    case loginTraktButton
    case syncTraktButton
    case traktUserName
    case traktUserIsVIP
    case openSubtitlesUserName
    case loginOpenSubtitles
    case subtitlesFontSize
    
    var cellTitle:String {
        get {
            switch self {
            case .userName,
                 .traktUserName,
                 .openSubtitlesUserName:
                return "username".localizable
            case .password:
                return "password".localizable
            case .vipExpiration:
                return "remaining-days-of-provider-subscription".localizable
            case .changeButton:
                return "button_changeCredentials".localizable
            case .resetButton:
                return "button_reset".localizable
            case .wsSpeedTestButton:
                return "button_startSpeed".localizable
            case .wsSpeedTestResult:
                return "last_speedTest_result".localizable
            case .resetWatchedhistory:
                return "button_resetWatched".localizable
            case .loginTraktButton:
                return "button_changeCredentials".localizable
            case .wsLogout :
                return "button_logout".localizable
            case .traktUserIsVIP:
                return "is_VIP".localizable
            case .loginOpenSubtitles:
                return "button_logon".localizable
            case .subtitlesFontSize:
                return "subtitles-font-size".localizable
            case .syncTraktButton:
                return "trakt_sync".localizable 
            }
        }
    }
}

struct SettingsViewModel {
    var menu:[SettingsMenuItem] = [.webshare,.trakt,.openSubtitles,.subtitles]
    var sections:[SettingsMenuItem:[SettingsSections]] = [.webshare:[.wsCredentials, .wsInfo, .other],
                                                          .trakt:[.traktCredentials],
                                                          .openSubtitles:[.openSubtitlesCredentials],
                                                          .subtitles:[.subtitlesFontSettings]]
    var cells:[SettingsSections:[CellTypes]] = [.wsCredentials:[.userName,.password,.wsLogout],.wsInfo:[.vipExpiration],
                                                .other:[.wsSpeedTestResult,.wsSpeedTestButton,.resetWatchedhistory,.resetButton],
                                                .traktCredentials:[.traktUserName,.traktUserIsVIP,.loginTraktButton,.syncTraktButton],
                                                .openSubtitlesCredentials:[.openSubtitlesUserName,.loginOpenSubtitles],
                                                .subtitlesFontSettings:[.subtitlesFontSize]]
    
    var wsUserModel: UserModel?
    var traktUser: TraktUser?
    
    //MARK: - Settings Menu Table
    func getNumberOfMenuItems() -> Int {
        self.menu.count
    }
    
    func getMenuTitle(at indexPath: IndexPath) -> String {
        return self.menu[indexPath.row].description
    }
    //MARK: - Settings Table
    func getNumberOfSections(for menu:SettingsMenuItem) -> Int {
        if let sections = self.sections[menu] {
            return sections.count
        }
        return 0
    }
    
    func getSectionTitle(for section:SettingsSections) -> String {
        return section.headerTitle
    }
    
    func getNumberOfCells(for section:SettingsSections) -> Int {
        if let cells = self.cells[section] {
            return cells.count
        }
        return 0
    }

    
    func getCell(_ tableView:UITableView, of section:SettingsSections, forRowAt indexPath:IndexPath) -> UITableViewCell? {
        if let cells = self.cells[section] {
            let cellType = cells[indexPath.row]
            switch cellType {
            case .password:
                let cell = SettingsInputTextCell.getCell(tableView)
                cell.type = cellType
                cell.set(name: cellType.cellTitle)
                cell.set(value: LoginManager.passwordHash ?? "")
                return cell
            case .userName:
                let cell = SettingsInputTextCell.getCell(tableView)
                cell.type = cellType
                cell.set(name: cellType.cellTitle)
                cell.set(value: LoginManager.name ?? "")
                return cell
            case .vipExpiration:
                let cell = InfoCell.getCell(tableView)
                cell.type = cellType
                if let days = wsUserModel?.vip_days {
                    cell.set(text: "remaining-days-of-provider-subscription".localizable + " " + days)
                }
                return cell
            case .resetButton,
                 .wsSpeedTestButton,
                 .changeButton,
                 .resetWatchedhistory,
                 .loginTraktButton,
                 .loginOpenSubtitles,
                 .syncTraktButton,
                 .wsLogout:
                let cell = ButtonCell.getCell(tableView)
                cell.type = cellType
                cell.set(text: cellType.cellTitle)
                return cell
            case .wsSpeedTestResult:
                let cell = InfoCell.getCell(tableView)
                cell.type = cellType
                if let speed = (UIApplication.shared.delegate as? AppDelegate)?.appData.speed.lastSpeedString {
                    cell.set(text: cellType.cellTitle + " \(speed)")
                }
                return cell
            case .traktUserName:
                let cell = SettingsInputTextCell.getCell(tableView)
                cell.type = cellType
                cell.set(name: cellType.cellTitle)
                cell.set(value: self.traktUser?.username ?? "")
                return cell
            case .traktUserIsVIP:
                let cell = SettingsInputTextCell.getCell(tableView)
                cell.type = cellType
                cell.set(name: cellType.cellTitle)
                cell.set(value: (self.traktUser?.isVIP ?? false) ? "true".localizable :"false".localizable)
                return cell
            case .openSubtitlesUserName:
                let cell = SettingsInputTextCell.getCell(tableView)
                cell.type = cellType
                cell.set(name: cellType.cellTitle)
                cell.set(value: LoginManager.osName ?? "")
                return cell
            case .subtitlesFontSize:
                let cell = InfoCell.getCell(tableView)
                cell.type = cellType
                cell.set(text: "subtitles-font-size".localizable + ": " + SubtitlesSettings.subtitlesSize.description)
                return cell
            }
        }
        return nil
    }
}

final class SettingsViewController: UITableViewController {
    var type: FilterType?
    
    private var credentials:Credentials = Credentials (name: "", pass: "") {
        didSet {
            self.chceckCredentials()
        }
    }
    
    private var settingsModel: SettingsViewModel = SettingsViewModel() {
        willSet {
            DispatchQueue.main.async {
                if self.tableView != nil {
                    self.tableView.reloadData()
                }
            }
        }
    }
    private var workInProggress:UILabel = UILabel()
    private var alert: UIAlertController?
    public var currentSettings: SettingsMenuItem = .webshare
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.getTraktData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getWSUserData()
    }
    
    private func setupView() {
        self.tableView.register(SettingsInputTextCell.self, forCellReuseIdentifier: "SettingsInputTextCell")
        self.tableView.register(InfoCell.self, forCellReuseIdentifier: "InfoCell")
        self.tableView.register(ButtonCell.self, forCellReuseIdentifier: "ButtonCell")
    }
    
    private func getTraktData() {
        TraktManager.sharedManager.getUserProfile { result in
            switch result {
            case .success(object: let object):
                self.settingsModel.traktUser = TraktUser(user: object)
            case .error(error: _):
                break
            }
        }
    }
    
    private func getWSUserData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let token = LoginManager.tokenHash,
        let uuid = UIDevice.current.identifierForVendor else { return }
        var data:[String:Any] = [:]
            data["device_uuid"] = uuid.uuidString
            data["wst"] = token
        appDelegate.appData.wsService.getUserData(params: data) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let user):
                self.settingsModel.wsUserModel = user
            case .failure(let error):
                error.handleError(on: self)
            }
        }
    }
    
    private func chceckCredentials() {
        if !self.credentials.name.isEmpty, !self.credentials.pass.isEmpty {
            LoginManager().connect(name: self.credentials.name, pass: self.credentials.pass) { result in
                switch result {
                case .success(_):
                    self.getWSUserData()
                case .failure(let error):
                    error.handleError(on: self)
                }
            }
        }
    }
    
    private func showWorkInProgressIfNeeded(count: Int) {
        if count > 0 {
            self.tableView.backgroundView = nil
        } else {
            self.tableView.backgroundView = self.workInProggress
            self.workInProggress.text = "Settings_WorkInProgress".localizable
            self.workInProggress.textAlignment = .center
        }
    }
    
    private func clearWatchedHistoryAppData() {
        let alert = UIAlertController(title: "settings_delete_history_data_title".localizable, message: "settings_delete_history_data_message".localizable, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "button_delete".localizable, style: .destructive, handler: { _ in
            SccWatchedData.deleteAllData()
        }))
        
        alert.addAction(UIAlertAction(title: "button_cancel".localizable, style: .default, handler: nil))
//        alert.addAction(UIAlertAction(title: nil, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func setFontSize() {
        let alert = UIAlertController(title: "subtitles-font-size".localizable, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "subtitles-font-size-small".localizable, style: .default, handler: { _ in
            SubtitlesSettings.subtitlesSize = .small
            self.tableView.reloadData()
        }))
        
        alert.addAction(UIAlertAction(title: "subtitles-font-size-normal".localizable, style: .default, handler: { _ in
            SubtitlesSettings.subtitlesSize = .normal
            self.tableView.reloadData()
        }))
        
        alert.addAction(UIAlertAction(title: "subtitles-font-size-large".localizable, style: .default, handler: { _ in
            SubtitlesSettings.subtitlesSize = .large
            self.tableView.reloadData()
        }))
        
        alert.addAction(UIAlertAction(title: "button_cancel".localizable, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func clearAppData() {
        let alert = UIAlertController(title: "settings_delete_app_data_title".localizable, message: "settings_delete_app_data_message".localizable, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "button_delete".localizable, style: .destructive, handler: { _ in
            SccWatchedData.deleteAllData()
            LoginManager.logout(isEraseAllData: true) { resut in
                switch resut {
                case .success(_):
                    self.tabBarController?.selectedIndex = 0
                case .failure(let error):
                    error.handleError(on: self)
                }
            }
        }))
        
        alert.addAction(UIAlertAction(title: "button_cancel".localizable, style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: nil, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func showNeedCredentials(message: String? = "to-watch-the-content-you-need-to-have-a-vip-accoun".localizable) {
        self.alert = UIAlertController(title: "missing-provider-credentials".localizable,
                                       message: message ,
                                       preferredStyle: .alert)
        self.alert?.addTextField(configurationHandler: { textField in
            textField.placeholder = "username".localizable
        })
        self.alert?.addTextField(configurationHandler: { textField in
            textField.isSecureTextEntry = true
            textField.placeholder = "password".localizable
        })
        
        let action = UIAlertAction(title: "button_continue".localizable, style: .default) { [weak self] _ in
            guard let self = self else { return }
            let name = self.alert?.textFields?[0].text
            let pass = self.alert?.textFields?[1].text
            self.login(name: name, pass: pass)
        }
        self.alert?.addAction(UIAlertAction(title: nil, style: .cancel, handler: nil))
        self.alert?.addAction(action)
        self.present(alert!, animated: true, completion: nil)
    }
    
    private func login(name: String? = nil, pass: String? = nil) {
        
        LoginManager().connect(name: name, pass: pass, completion: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(_):
                if let alert = self.alert {
                    alert.dismiss(animated: false, completion: {
                        self.dismiss(animated: true, completion: nil)
                    })
                    return
                }
                self.dismiss(animated: true, completion: nil)
            case .failure(let error):
                if error == .noDataForRequest {
                    self.showNeedCredentials()
                } else {
                    self.showNeedCredentials(message: "bad_credentials".localizable)
                }
            }
        })
    }
}

extension SettingsViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        let sectionsCount = self.settingsModel.getNumberOfSections(for: self.currentSettings)
        self.showWorkInProgressIfNeeded(count: sectionsCount)
        return sectionsCount
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionsForMenu = self.settingsModel.sections[self.currentSettings]
        if let section = sectionsForMenu?[section] {
            return self.settingsModel.getNumberOfCells(for: section)
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionsForMenu = self.settingsModel.sections[self.currentSettings]
        if let section = sectionsForMenu?[indexPath.section],
            let cell = self.settingsModel.getCell(tableView, of: section, forRowAt: indexPath) {
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? SettingsCell,
            let type = cell.type else { return }
        switch type {
        case .userName,
             .password:
            break
        case .vipExpiration:
            break
        case .changeButton:
            self.showNeedCredentials()
        case .resetButton:
            self.clearAppData()
        case .wsSpeedTestResult:
            break //
        case .wsSpeedTestButton:
            (UIApplication.shared.delegate as? AppDelegate)?.appData.speed.testSpeed()
        case .resetWatchedhistory:
            self.clearWatchedHistoryAppData()
        case .loginTraktButton:
            self.connectToTakt()
        case .wsLogout:
            self.clearAppData()
        case .traktUserName:
            break
        case .traktUserIsVIP:
            break
        case .openSubtitlesUserName:
            break
        case .loginOpenSubtitles:
            self.loginOS()
        case .subtitlesFontSize:
            self.setFontSize()
        case .syncTraktButton:
            self.alert = UIAlertController(title: "wait".localizable, message: nil, preferredStyle: .alert)
            self.present(self.alert!, animated: true, completion: nil)
            (UIApplication.shared.delegate as? AppDelegate)?.appData.traktSync.start { [weak self] success in
                guard let self = self else { return }
                self.alert?.dismiss(animated: false, completion: {
                    let title = success ? "sync_done".localizable : "sync_false".localizable
                    self.alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
                    self.alert?.addAction(UIAlertAction(title: "button_ok".localizable, style: .cancel, handler: nil))
                    self.present(self.alert!, animated: true, completion: nil)
                })
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let section = SettingsSections(rawValue: section) {
            return self.settingsModel.getSectionTitle(for: section)
        }
        return nil
    }
}

extension SettingsViewController: SettingsInputTextCellDelegate {
    func settingsInput(_ cell: SettingsInputTextCell, type: CellTypes, set value: String) {
        switch type {
        case .password:
            self.credentials.pass = value
        case .userName:
            self.credentials.name = value
        default:
            break
        }
    }
}

extension SettingsViewController: MenuViewDelegate {
    
    func menuView(_ menuView: MenuViewController, settingsDid select: SettingsMenuItem) {
        self.currentSettings = select
        self.tableView.reloadData()
        
    }
    
    func menuView(_ menuView: MenuViewController, did select: MenuItem) {    }
}

//MARK: - openSubbtirtles
extension SettingsViewController {
    private func loginOS() {
        alert = UIAlertController(title: "missing-provider-credentials".localizable,
                                       message: "to-watch-the-content-you-need-to-have-a-vip-accoun".localizable ,
                                       preferredStyle: .alert)
        alert?.addTextField(configurationHandler: { textField in
            textField.placeholder = "username".localizable
        })
        alert?.addTextField(configurationHandler: { textField in
            textField.isSecureTextEntry = true
            textField.placeholder = "password".localizable
        })
        
        let action = UIAlertAction(title: "button_continue".localizable, style: .default) { [weak self] _ in
            guard let self = self else { return }
            let name = self.alert?.textFields?[0].text
            let pass = self.alert?.textFields?[1].text
            self.loginToOS(name: name, pass: pass)
        }
        
        alert?.addAction(action)
        self.present(alert!, animated: true, completion: nil)
    }
    
    private func loginToOS(name:String?, pass:String?) {
        LoginManager.osLogin(name: name, pass: pass) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(_):
                self.tableView.reloadData()
            case .failure(let error):
                error.handleError(on: self)
            }
        }
    }
}

//MARK: - trakt kit
extension SettingsViewController: TraktQRViewDelegate {
    func traktQRView(_ traktViewController: TraktQRViewController, isLogin success: Bool) {
        if success {
            self.getTraktData()
        }
    }
    
    func connectToTakt() {
        TraktManager.sharedManager.getAppCode { [weak self] deviceData in
            guard let self = self else { return }
            if let deviceCode = deviceData {
                self.showQRCode(data: deviceCode)
            }
        }
    }
    
    func showQRCode(data: DeviceCode) {
        DispatchQueue.main.async {
            let traktVC = TraktQRViewController()
            traktVC.traktDelegate = self
            traktVC.set(data: data)
            self.present(traktVC, animated: true,completion: nil)
        }
    }
}
