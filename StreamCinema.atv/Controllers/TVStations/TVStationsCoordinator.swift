//
//  TVStationsCoordinator.swift
//  StreamCinema.atv
//
//  Created by SCC on 03/10/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

final class TVStationsCoordinator {
    private let stationListVC = StationListViewController()
    private let stationProgramVC = StationProgremListViewController()
    private var selectedStation:TVStations?
    private var selectedDate: Date?
    private var movieCoordinator: MovieViewCoordinator?
    
    public func start(endpoint:AppTabs) -> UIViewController {
        stationListVC.stationDelegate = self
        stationProgramVC.programDelegate = self
        let split = UISplitViewController()
        split.viewControllers = [stationListVC,stationProgramVC]
        split.title = endpoint.description
        split.preferredPrimaryColumnWidthFraction = 2/7
        return split
    }
    
    private func getData(station:String = "", date:Date = Date(), page: Int = 1) {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.appData.scService.getTvProgram(for: station,
                                                    date: date.toDateString,
                                                    page:page) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let data):
                    if page == 1 {
                        self.stationProgramVC.model = data
                    } else {
                        self.stationProgramVC.model?.data?.append(contentsOf: data.data ?? [])
                        self.stationProgramVC.model?.pagination = data.pagination
                    }
                case .failure(let error):
                    error.handleError(on: self.stationProgramVC)
                }
            }
        }
    }
}

extension TVStationsCoordinator: StationListDelegate {
    func stationList(_ stationList: StationListViewController, didSelect station: TVStations, for date: Date) {
        self.selectedStation = station
        self.selectedDate = date
        if station == .All {
            self.getData(date: date)
        } else {
            self.getData(station: station.rawValue, date: date)
        }
    }
}

extension TVStationsCoordinator: StationProgremListDelegate {
    func stationProgremListGetNextPage(_ controller: StationProgremListViewController, getNext: Int) {
        guard let station = self.selectedStation,
              let date = self.selectedDate else { return }
        if station == .All {
            self.getData(date: date, page: getNext)
        } else {
            self.getData(station: station.rawValue, date: date, page: getNext)
        }
    }

    func stationProgremList(_ controller:StationProgremListViewController, didSelect movie:InfoData) {
        guard let mediaType = movie.source?.infoLabels?.getMediaType() else { return }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if mediaType == .movie {
            self.movieCoordinator = MovieViewCoordinator(rootViewController: controller, movie: movie)
        }
        else if mediaType == .tvshow,
            let tvSvc = storyboard.instantiateViewController(withIdentifier: "TWShowViewController") as? TVShowViewController {
            tvSvc.tvShow = movie
            tvSvc.watched = SccWatchedData.getTvShowWatched(by: movie.id)
            controller.present(tvSvc, animated: true, completion: nil)
        }
    }
}
