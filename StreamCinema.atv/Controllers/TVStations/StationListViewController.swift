//
//  StationListViewController.swift
//  StreamCinema.atv
//
//  Created by SCC on 02/10/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

final class TVCell: UITableViewCell {
    private let statoonTitle: UILabel = UILabel()
    private let stationImage: UIImageView = UIImageView()
    
    public var station: TVStations? {
        didSet {
            self.configureViews()
            self.statoonTitle.text = self.station?.rawValue
            if let imageUrl = self.station?.imageURL {
                self.stationImage.setCashedImage(url: imageUrl, type: .emptyLoading)
            }
        }
    }
    
    private func configureViews() {
        self.addSubview(self.statoonTitle)
        self.addSubview(self.stationImage)
        
        self.statoonTitle.translatesAutoresizingMaskIntoConstraints = false
        self.stationImage.translatesAutoresizingMaskIntoConstraints = false
        
        self.statoonTitle.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        self.statoonTitle.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.statoonTitle.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.statoonTitle.rightAnchor.constraint(equalTo: self.stationImage.leftAnchor).isActive = true
        
        self.stationImage.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        self.stationImage.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.stationImage.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
    }
}

protocol StationListDelegate {
    func stationList(_ stationList:StationListViewController, didSelect station:TVStations, for date: Date)
}

final class StationListViewController: UITableViewController {
    public var stationDelegate: StationListDelegate?
    
    let tvStations:[TVStations] = TVStations.allCases
    
    override func viewDidLoad() {
        self.tableView.register(TVCell.self, forCellReuseIdentifier: "TVCell")
    }
    
}

extension StationListViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tvStations.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TVCell") as? TVCell else { return UITableViewCell()}
        cell.station = self.tvStations[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.stationDelegate?.stationList(self, didSelect: self.tvStations[indexPath.row], for: Date())
    }
}

