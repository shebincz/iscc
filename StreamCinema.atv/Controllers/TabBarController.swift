//
//  TabBarController.swift
//  StreamCinema.atv
//
//  Created by SCC on 03/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

final class TabBarController:UITabBarController {
    private var completition: ((UIAlertAction) -> Void)?
    private var movieCoordinator: MovieViewCoordinator?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let image = UIImage(named: "backgroundImage") {
            self.view.backgroundColor = UIColor(patternImage: image)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        (UIApplication.shared.delegate as? AppDelegate)?.appData.speed.testSpeed()
        self.checkCredentials()
    }
    
    public func presentDetailFormOpenIn(type: FilterType, movieID: String, startPlaying: Bool) {
        guard self.isViewLoaded == true,
            !(self.presentedViewController?.isEqual(OnboardingViewController.self) ?? false),
            let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        appDelegate.appData.scService.getMovie(by: movieID, type: type) { result in
            switch result {
            case .success(let value):
                if let mediaType = value.infoLabels?.getMediaType() {
                let data = InfoData(index: nil, type: nil, id: movieID, score: nil, source: value, tvInfo: nil)
                    self.presentDetialVD(type: mediaType, movieID: movieID, infoData: data)
                }
                break
            case .failure(_):
                break
            }
        }
    }
    
    private func checkCredentials() {
        if LoginManager.tokenHash == nil, self.isViewLoaded == true {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "OnboardingViewController")
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    private func presentDetialVD(type:MediaType, movieID:String, infoData: InfoData) {
        if type == .movie {
            self.movieCoordinator = MovieViewCoordinator(rootViewController: self, movie: infoData)
        }
        else if type == .tvshow {
            let tvShowDetailVC = self.storyboard!.instantiateViewController(withIdentifier: "TWShowViewController") as! TVShowViewController
            tvShowDetailVC.tvShow = infoData
            tvShowDetailVC.watched = SccWatchedData.getTvShowWatched(by: movieID)
            self.present(tvShowDetailVC, animated: true, completion: nil)
        }
    }
    
    override var selectedIndex: Int {
        didSet {
            self.checkCredentials()
        }
    }
}
