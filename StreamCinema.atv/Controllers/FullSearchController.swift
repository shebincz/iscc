//
//  FullSearchController.swift
//  StreamCinema.atv
//
//  Created by SCC on 16/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

final class FullSearchController: UISearchController {
    
    static func createSearch(storyboard: UIStoryboard?) -> UIViewController {
        guard let movieListController = storyboard?.instantiateViewController(withIdentifier: "MovieListViewController") as? MovieListViewController else {
            fatalError("Unable to instantiate a NewsController")
        }
        
        let searchController = FullSearchController(searchResultsController: movieListController)
        searchController.searchResultsUpdater = movieListController
        
        let searchContainer = UISearchContainerViewController(searchController: searchController)
        return searchContainer
    }
}
