//
//  ActorMoviesViewController.swift
//  StreamCinema.atv
//
//  Created by SCC on 22/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

final class ActorMoviesViewController: UIViewController {
    
    @IBOutlet weak var actorImageView: UIImageView!
    @IBOutlet weak var actorNameLabel: UILabel!
    @IBOutlet weak var movieCollection: MovieCollectionView!
    @IBOutlet weak var activityIndikator: UIActivityIndicatorView!
    private var movieCoordinator: MovieViewCoordinator?
    private var cast:Cast?
    private var model:FilterResult?
    private var movieTitle:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let cast = self.cast {
            self.actorNameLabel.text = cast.name
            if let thumbnail = cast.thumbnail,
                let actorImgUrl = URL(string: thumbnail) {
                self.actorImageView.setCashedImage(url: actorImgUrl, type: .emptyLoading)
            }
            self.movieCollection.movieDelegate = self
            self.fetchData()
        } else if let model = self.model,
                  let title = self.movieTitle {
            self.movieCollection.model = model
            self.movieCollection.movieDelegate = self
            self.actorNameLabel.text = title
        }
    }
    
    public func prepareData(with cast:Cast) {
        self.cast = cast
    }
    
    public func prepareData(with model:FilterResult, releated title:String) {
        self.movieTitle = title
        self.model = model
    }
    
    private func fetchData(for page:Int = 1) {
        if let castName = self.cast?.name,
            let delegate = UIApplication.shared.delegate as? AppDelegate {
            self.activityIndikator.startAnimating()
            let filter = FilterModel(name: .search, type: .all, order: .desc, sort: .playCount, count: nil, lang: nil, value: [castName], service: nil, days: nil, limit: 100, page: page)
            delegate.appData.scService.search(wtih: filter) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let data):
                    self.movieCollection.model = data
                case .failure(let error):
                    error.handleError(on: self)
                }
                self.activityIndikator.stopAnimating()
            }
        }
    }
}

extension ActorMoviesViewController: MovieCollectionDelegate {
    func movieCollection(_ collection: MovieCollectionView, didSelect movie: InfoData) {
        if let mediaType = movie.source?.infoLabels?.getMediaType() {
        
            if mediaType == .movie {
                self.movieCoordinator = MovieViewCoordinator(rootViewController: self, movie: movie)
            }
            else if mediaType == .tvshow {
                let tvShowDetailVC = storyboard!.instantiateViewController(withIdentifier: "TWShowViewController") as! TVShowViewController
                tvShowDetailVC.tvShow = movie
                tvShowDetailVC.watched = SccWatchedData.getTvShowWatched(by: movie.id)
                self.present(tvShowDetailVC, animated: true, completion: nil)
            }
        }
    }
    
    func movieCollection(_ collection: MovieCollectionView, getNext page: Int) {
        if self.cast != nil {
            self.fetchData(for: page)
        }
    }
}
