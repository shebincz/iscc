//
//  MovieListViewController.swift
//  MovieDBTV
//
//  Created by Alfian Losari on 23/03/19.
//  Copyright © 2019 Alfian Losari. All rights reserved.
//

import UIKit

class MovieListViewController: UIViewController {
    
    @IBOutlet var collectionView: MovieCollectionView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var refreshButton: UIButton!

    private var lastSearchText: String = ""
    private var movieCoordinator:MovieViewCoordinator?
    var endpoit:AppTabs? {
        didSet {
            self.type = endpoit?.type
        }
    }
    var actualItem: MenuItem?
    var type: FilterType?
    var model: FilterResult? {
        willSet {
            self.collectionView.model = newValue
            self.activityIndicator.stopAnimating()
        }
    }
    
    var movies = [Movie]() {
        didSet {
            collectionView.reloadData()
            self.activityIndicator.stopAnimating()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.movieDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.movieCoordinator != nil {
            self.movieCoordinator = nil
        }
    }
    
    private func fetchMovies(for item:MenuItem, page: Int = 1) {
        if let delegate = UIApplication.shared.delegate as? AppDelegate,
            let type = self.type {
            if item == .genre {
                self.getGenreData()
                return
            } else {
                self.removeGenereView()
            }
            if page == 1 {
                self.model = nil
            }
            self.activityIndicator.startAnimating()
            delegate.appData.scService.getMovies(type: type, for: item, page: page) {  [weak self] result in
                guard let self = self else { return }
                self.activityIndicator.stopAnimating()
                switch result {
                case .success(let data):
                    self.update(data: data, page: page)
                case .failure(let error):
                    error.handleError(on: self)
                }
            }
        }
    }
    
    private func getGenreData() {
        let genreTable = GenreTableView(frame: self.view.bounds, style: .plain)
        genreTable.type = self.type ?? .movie
        genreTable.genreDelegate = self
        self.model = nil
        self.view.addSubview(genreTable)
    }
    
    private func removeGenereView() {
        for view in self.view.subviews where view.isKind(of: GenreTableView.self) {
            view.removeFromSuperview()
        }
    }
    
    private func update(data:FilterResult, page: Int) {
        if page == 1 {
            self.model = data
        } else {
            if let movieData = data.data, let pagination = data.pagination {
                self.model?.data?.append(contentsOf: movieData)
                self.model?.pagination = pagination
            }
        }
    }
    
    private func showError(_ error: String) {
        infoLabel.text = error
        infoLabel.isHidden = false
        refreshButton.isHidden = false
    }
    
    private func hideError() {
        infoLabel.isHidden = true
        refreshButton.isHidden = true
    }
    
    @IBAction func refreshTapped(_ sender: Any) {
//        guard let item = self.actualItem else { return }
//        self.fetchMovies(for: item)
    }
    
    private func open(movie:InfoData) {
        if let mediaType = movie.source?.infoLabels?.getMediaType() {
        
            if mediaType == .movie
            {
                self.movieCoordinator = MovieViewCoordinator(rootViewController: self, movie: movie)
            }
            else if mediaType == .tvshow {
                let tvShowDetailVC = storyboard!.instantiateViewController(withIdentifier: "TWShowViewController") as! TVShowViewController
                tvShowDetailVC.tvShow = movie
                self.present(tvShowDetailVC, animated: true, completion: nil)
            }
        }
    }
}

extension MovieListViewController: GenreTableDelegate {
    func castCollectionView(_ castView: GenreTableView, didSelect movie: InfoData) {
        self.open(movie: movie)
    }
}

extension MovieListViewController: MenuViewDelegate {
    func menuView(_ menuView: MenuViewController, settingsDid select: SettingsMenuItem) {
        
    }
    
    func menuView(_ menuView: MenuViewController, did select: MenuItem) {
        if self.actualItem != select {
            self.activityIndicator.startAnimating()
            self.hideError()
            self.actualItem = select
            self.fetchMovies(for: select)
        }
    }
}

extension MovieListViewController: MovieCollectionDelegate {
    func movieCollection(_ collection: MovieCollectionView, getNext page: Int) {
        if let item = self.actualItem {
            self.fetchMovies(for: item, page: page)
        } else if !self.lastSearchText.isEmpty {
            self.search(text: self.lastSearchText, page: page)
        }
    }
    
    func movieCollection(_ collection: MovieCollectionView, didSelect movie: InfoData) {
        self.open(movie: movie)
    }
}

extension MovieListViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text, !text.isEmpty else {
            self.movies = []
            self.model = nil
            return
        }
        if self.lastSearchText != text {
            self.search(text: text)
        }
        self.lastSearchText = text
    }
    
    func search(text: String, page: Int = 1) {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        self.activityIndicator.startAnimating()
        let filter = FilterModel.allFillter(value: text, limit: 12, page: page)
        delegate.appData.scService.search(wtih: filter) { [weak self] response in
            guard let self = self  else { return }
            self.activityIndicator.stopAnimating()
            switch response {
            case .success(let data):
                self.update(data: data, page: page)
            case .failure(let error):
                error.handleError(on: self)
            }
        }
    }
    
}


