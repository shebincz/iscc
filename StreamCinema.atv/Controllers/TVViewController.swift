//
//  TVViewController.swift
//  StreamCinema.atv
//
//  Created by SCC on 25/11/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import KSPlayer

final class TVViewController: UITableViewController {
    private var model:[TV] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "TVStationCell", bundle: nil), forCellReuseIdentifier: "TVStationCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getData()
    }
    
    private func getData() {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.appData.isccService.getTVdata { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let tvModel):
                    self.model = tvModel.tvs
                    self.tableView.reloadData()
                case .failure(let err):
                    err.handleError(on: self)
                }
            }
        }
    }
    
    private func loadEpg(for station:String, reload indexPath: IndexPath) {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.appData.isccService.getEPG(for: station) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let data):
                    let index = self.model.firstIndex { tv -> Bool in
                        tv.tvid == station
                    }
                    if let index = index {
                        self.model[index].epg = data
                        if self.model[index].isReloaded == false {
                            self.tableView.reloadRows(at: [indexPath], with: .none)
                            self.model[index].isReloaded = true
                        }
                    }
                case .failure(_):
                    break
                }
            }
        }
    }
}

extension TVViewController {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 158
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TVStationCell") as? TVStationCell else { return UITableViewCell() }
        let model = self.model[indexPath.row]
        if let tvlogo = model.tvlogo,
            let url = URL(string: tvlogo) {
            cell.stationPincon.setCashedImage(url: url, type: .emptyLoading)
        }
        if let epg = model.epg {
            cell.titleLabel.text = epg.currnet?.programName
            cell.nextTitle.text = epg.next?.programName
            if let start = epg.currnet?.getStart(),
               let end = epg.currnet?.getEnd() {
                cell.setProgress(start: start, stop: end, current: Date(), nextStart: epg.next?.getStart(), nextEnd: epg.next?.getEnd())
            }
        } else if let tvID = model.tvid {
            self.loadEpg(for: tvID, reload: indexPath)
        }
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.model[indexPath.row]
        if let url = URL(string:model.url ?? ""),
           let name = model.tvtitle,
           let cover = model.tvlogo {
            let coordinator = VLCCoordinator()
            coordinator.playLiveStream(url, sender: self, title: name, logo: cover, desc: model.epg?.currnet?.desc)
        }
    }
}
