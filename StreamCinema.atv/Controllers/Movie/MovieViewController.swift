//
//  MovieViewController.swift
//  StreamCinema.atv
//
//  Created by SCC on 25/10/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import AVKit
import XCDYouTubeKit

enum MovieViewAction {
    case playMovie
    case continueMovie
    case showCast
    case showRelated
}

protocol MovieViewDelegate: class {
    func movieView(_ movieView:MovieViewController, prerform action:MovieViewAction)
}

final class MovieViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var trailerView: UIView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var blurEfectView: UIView!
    @IBOutlet weak var infoLabelVStack: MovieViewInfoLables!
    
    public weak var delegate:MovieViewDelegate?
    public var movie: InfoData?
    public var showContinueButton:Bool = false {
        didSet {
            if self.showContinueButton == true {
                self.continueButton?.isHidden = true
            }
        }
    }
    
    private var playerTrailer: AVPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag, completion: completion)
        self.stopPlayingTrailer()
    }

    @objc private func playButtonPressed() {
        self.stopPlayingTrailer()
        self.delegate?.movieView(self, prerform: .playMovie)
    }
    
    @objc private func continueButtonPressed() {
        self.stopPlayingTrailer()
        self.delegate?.movieView(self, prerform: .continueMovie)
    }
    
    deinit {
        print("MovieViewController deinit")
        self.stopPlayingTrailer()
    }
}

extension MovieViewController {
    
    private func configureView() {
        self.titleImageView.kf.indicatorType = .none
        self.backgroundImageView.kf.indicatorType = .none
        
        if let fanAart = self.movie?.source?.getInfoLabels().art?.fanartOirg,
           let url = URL(string: fanAart) {
            self.backgroundImageView.kf.setImage(with: url)
        } else if let poster = self.movie?.source?.getInfoLabels().art?.getPoster(),
                  let url = URL(string: poster){
            self.backgroundImageView.kf.setImage(with: url)
        }
        
        if let logo = self.movie?.source?.getInfoLabels().art?.clearlogo,
           let url = URL(string: logo) {
            self.titleImageView.kf.setImage(with: url)
            self.titleLabel.text = nil
        } else {
            self.titleLabel.text = self.movie?.source?.getInfoLabels().title
        }
        
        self.descLabel.text = self.movie?.source?.getInfoLabels().plot
        
        if self.showContinueButton == false {
            self.continueButton.isHidden = true
        }
        
        self.playButton.addTarget(self, action: #selector(playButtonPressed), for: .primaryActionTriggered)
        self.continueButton.addTarget(self, action: #selector(continueButtonPressed), for: .primaryActionTriggered)
        
        self.configureSwipe()
        self.configureBlur()
        self.prepareTrailer()
        self.configureInfoView()
    }

    
    private func configureInfoView() {
        if let streams = self.movie?.source?.availableStreams,
           let streamInfo =  self.movie?.source?.streamInfo {
            let audio = streams.languages?.audio?.map?.joined(separator: ", ")
            let srt = streams.languages?.subtitles?.map?.joined(separator: ", ")
            
            var genereString = ""
            if let generes = self.movie?.source?.infoLabels?.genre {
                let genere = generes.map({ name -> String in
                    if let genere = Genere.init(rawValue: name)?.string {
                        return genere
                    }
                    return name
                })
                genereString = genere.joined(separator: ", ")
            }

            let duration = String((self.movie?.source?.infoLabels?.duration ?? 0) / 60)
            let rating = self.movie?.source?.getRating()
            
            var resolution:Resolution = .unknown
            if let w = streamInfo.video?.width, let h = streamInfo.video?.height {
                resolution = CGSize(width: w, height: h).getResolution()
            }
            
            self.infoLabelVStack.set(resolution: resolution, genere: genereString, duration: duration, rating: rating, langs: audio, subs: srt)
        }
    }
    
    private func configureBlur() {
        var style:UIBlurEffect.Style = .light
        if self.traitCollection.userInterfaceStyle == .dark {
            style = .dark
        }
        let blurEffect = UIBlurEffect(style: style)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.blurEfectView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.blurEfectView.addSubview(blurEffectView)

        let gradientMaskLayer = CAGradientLayer()
        gradientMaskLayer.frame = blurEffectView.bounds
        gradientMaskLayer.colors = [UIColor.clear.cgColor, UIColor.white.cgColor, UIColor.white.cgColor, UIColor.clear.cgColor]
        gradientMaskLayer.locations = [0.0, 0.15, 0.85, 1.0]
        blurEffectView.layer.mask = gradientMaskLayer
    }
    
    private func configureSwipe() {
        let swipeDown:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipedDown(sender:)))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        
        let swipeUP:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipedUP(sender:)))
        swipeUP.direction = .up
        self.view.addGestureRecognizer(swipeUP)
    }
    
    @objc private func swipedDown(sender:UISwipeGestureRecognizer) {
        self.delegate?.movieView(self, prerform: .showRelated)
    }

    @objc private func swipedUP(sender:UISwipeGestureRecognizer) {
        self.delegate?.movieView(self, prerform: .showCast)
    }
    
    private func prepareTrailer() {
        if let trailerURL = self.movie?.source?.getTrailer(),
                  let range = trailerURL.range(of: "=") {
            let youtubeID = String(trailerURL[range.upperBound...])
            self.playYoutube(with: youtubeID)
        }
        else if let trailer = self.movie?.source?.getInfoLabels().trailer ?? self.movie?.source?.infoLabels?.trailer,
           trailer.contains("youtube"),
           let range = trailer.range(of: "=")
        {
            let youtubeID = String(trailer[range.upperBound...])
            self.playYoutube(with: youtubeID)
            
        }
    }
    
    private func playYoutube(with youtubeID:String) {
        XCDYouTubeClient.default().getVideoWithIdentifier(youtubeID) { [weak self] (video: XCDYouTubeVideo?, error: Error?) in
           guard let self = self,
                 let video = video,
                 let videoURL = video.streamURL
           else { return }

            if self.backgroundImageView.image == nil,
               let thumbnailURL = video.thumbnailURLs?.last {
                self.backgroundImageView.kf.indicatorType = .none
                self.backgroundImageView.kf.setImage(with: thumbnailURL)
            }
            
            self.addVideoPlayer(videoURL)
        }
    }
    
    private func addVideoPlayer(_ videoUrl: URL) {
        self.playerTrailer = AVPlayer(url: videoUrl)
        let layer: AVPlayerLayer = AVPlayerLayer(player: self.playerTrailer)
        layer.backgroundColor = UIColor.clear.cgColor
        layer.frame = view.bounds
        layer.videoGravity = .resizeAspectFill
        self.backgroundImageView.layer.sublayers?
            .filter { $0 is AVPlayerLayer }
            .forEach { $0.removeFromSuperlayer() }
        self.backgroundImageView.layer.addSublayer(layer)
        self.playerTrailer?.play()
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.playerTrailer?.currentItem, queue: .main) { [weak self] _ in
            guard let self = self else { return }
            self.playerTrailer?.seek(to: CMTime.zero)
            for layer in self.backgroundImageView.layer.sublayers ?? [] {
                if layer.isKind(of: AVPlayerLayer.self) {
                    layer.removeFromSuperlayer()
                }
            }
        }
    }
    
    public func stopPlayingTrailer() {
        self.playerTrailer?.pause()
        self.playerTrailer?.seek(to: CMTime.zero)
        for layer in self.backgroundImageView.layer.sublayers ?? [] {
            if layer.isKind(of: AVPlayerLayer.self) {
                layer.removeFromSuperlayer()
            }
        }
        self.playerTrailer = nil
    }
}
