//
//  MenuViewController.swift
//  StreamCinema.atv
//
//  Created by SCC on 28/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

protocol MenuViewDelegate {
    var type: FilterType? { get set }
    func menuView(_ menuView: MenuViewController, did select:MenuItem)
    func menuView(_ menuView: MenuViewController, settingsDid select:SettingsMenuItem)
}

final class MenuViewController: UITableViewController {
    private let items:[MenuItem] = [.popular, .trending, .news, .mostWatched, .dubbed, .lastAdded, .genre, .watched]
    private let settingsItems:[SettingsMenuItem] = [.webshare, .trakt, .openSubtitles, .subtitles]
    private let traktItems:[MenuItem] = [.traktHistory,.traktWatchList]
    private var selectedItem: IndexPath = IndexPath(row: 0, section: 0)
    
    public var endpoit:AppTabs? {
        willSet {
            self.type = newValue?.type
        }
    }
    public var type:FilterType?
    public var menuDelegate:MenuViewDelegate? {
        didSet {
            self.menuDelegate?.type = self.endpoit?.type
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.selectRow(at: self.selectedItem, animated: false, scrollPosition: .middle)
        if let endpoint = self.endpoit {
            if endpoint == .settings {
                
            } else if endpoint == .trakt {
                self.menuDelegate?.menuView(self, did: self.traktItems[self.selectedItem.row])
            } else {
                self.menuDelegate?.menuView(self, did: self.items[self.selectedItem.row])
            }
        }
    }
}

extension MenuViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        if self.endpoit != nil {
            return 1
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let endpoint = self.endpoit {
            if endpoint == .settings {
                return settingsItems.count
            } else if endpoint == .trakt {
                return traktItems.count
            } else {
                return items.count
            }
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        
        if let endpoint = self.endpoit {
            if endpoint == .settings {
                cell.textLabel?.text = self.settingsItems[indexPath.row].description
            } else if endpoint == .trakt {
                cell.textLabel?.text = self.traktItems[indexPath.row].description
            } else {
                cell.textLabel?.text = self.items[indexPath.row].description
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didUpdateFocusIn context: UITableViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if let indexPath = context.nextFocusedIndexPath,
            self.type == nil {
            self.menuDelegate?.menuView(self, settingsDid: self.settingsItems[indexPath.row])
            self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let type = self.type {
            if type == .trakt {
                self.menuDelegate?.menuView(self, did: self.traktItems[indexPath.row])
                return
            }
            self.menuDelegate?.menuView(self, did: self.items[indexPath.row])
        }
        self.selectedItem = indexPath
    }
}

