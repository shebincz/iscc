//
//  AZSearchController.swift
//  StreamCinema.atv
//
//  Created by SCC on 01/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

enum KeyboardFunc: Int {
    case space = 909
    case backpace = 999
}

struct AZModel {
    var char: String
}

final class AZSearchController: UIViewController {
    @IBOutlet var resultCollection: MovieCollectionView!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var button0: UIButton!
    @IBOutlet var buttonI: UIButton!
    @IBOutlet var typeSegment: UISegmentedControl!
    
    @IBOutlet var keyboardButtons: [UIButton]!
    @IBOutlet var numberKeyboardButtons: [UIButton]!
    
    private let midleDownKeyboard:UIFocusGuide = UIFocusGuide()
    private let midleUPKeyboard:UIFocusGuide = UIFocusGuide()
    private var filterType:FilterType = .movie
    private var movieCoordinator: MovieViewCoordinator?
    
    var model: FilterResult? {
        willSet {
            if self.resultCollection != nil {
                self.resultCollection.model = newValue
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCollectionView()
        self.setupFocusGuides()
        
        self.typeSegment.setTitle("movies".localizable, forSegmentAt: 0)
        self.typeSegment.setTitle("series".localizable, forSegmentAt: 1)
    }
    
    @IBAction func typeSegmentDidChange(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.filterType = .movie
        } else {
            self.filterType = .tvshow
        }
        if let text = self.searchTextField.text {
            self.updateSearch(text: text)
        }
    }
    
    
    private func setupFocusGuides() {
        self.view.addLayoutGuide(self.midleUPKeyboard)
        self.view.addLayoutGuide(self.midleDownKeyboard)
        
        self.midleDownKeyboard.topAnchor.constraint(equalTo: self.buttonI.bottomAnchor, constant: 1).isActive = true
        self.midleDownKeyboard.bottomAnchor.constraint(equalTo: self.midleUPKeyboard.topAnchor, constant: 0).isActive = true
        self.midleDownKeyboard.heightAnchor.constraint(equalToConstant: 4).isActive = true
        self.midleDownKeyboard.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.midleDownKeyboard.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        
        self.midleUPKeyboard.bottomAnchor.constraint(equalTo: self.button0.topAnchor, constant: -1).isActive = true
        self.midleUPKeyboard.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.midleUPKeyboard.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        
        self.updatefocus()
    }
    
    private func updatefocus() {
        self.midleUPKeyboard.preferredFocusEnvironments = self.keyboardButtons
        
        let focusEnvi = self.numberKeyboardButtons.sorted(by: {
            if let title1 = $0.currentTitle, let title2 = $1.currentTitle {
                return title1 < title2
            }
            return false
        }).filter( { $0.isEnabled == true } )
        
        if focusEnvi.count != 0 {
            self.midleDownKeyboard.preferredFocusEnvironments = focusEnvi
        } else {
            self.midleDownKeyboard.preferredFocusEnvironments = [self.resultCollection]
        }
    }
    
    private func setupCollectionView() {
        self.resultCollection.movieDelegate = self
        self.resultCollection.register(UINib(nibName: "MovieCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
    }
    
    @IBAction func keyboardButtonPressed(_ sender: UIButton) {
        if sender.tag == KeyboardFunc.backpace.rawValue, var text = self.searchTextField.text, !text.isEmpty,
            let lastCh = text.last, let index = text.firstIndex(of: lastCh) {
            text.remove(at: index)
            self.updateSearch(text: text)
        } else if sender.tag == KeyboardFunc.space.rawValue,
            var text = self.searchTextField.text {
            text.append(" ")
            self.updateSearch(text: text)
        } else if sender.tag != KeyboardFunc.space.rawValue,
            sender.tag != KeyboardFunc.backpace.rawValue,
            var text = self.searchTextField.text {
            text += (sender.titleLabel?.text)!
            self.updateSearch(text: text)
        }
    }
    
    private func updadateKeyboard(with results: [AZResult]) {
        self.allButtons(isEnabled: false)
        for result in results {
            if let searchedText = self.searchTextField.text, //AA
                let value = result.value { //AAA
                let char = value.replacingOccurrences(of: searchedText, with: "")
                self.enableButton(title: char)
            }
        }
        self.updatefocus()
    }
    
    private func enableButton(title: String) {
        for button in self.keyboardButtons {
            if button.titleLabel?.text == title {
                button.isEnabled = true
                return
            }
        }
        for button in self.numberKeyboardButtons {
            if button.titleLabel?.text == title {
                button.isEnabled = true
                return
            }
        }
    }
    
    private func allButtons(isEnabled: Bool) {
        for button in self.keyboardButtons {
            button.isEnabled = isEnabled
        }
        for button in self.numberKeyboardButtons {
            button.isEnabled = isEnabled
        }
    }
    
    private func updateSearch(text: String, page: Int = 1) {
        self.searchTextField.text = text
        self.model = nil
        if self.searchTextField.text?.isEmpty ?? false {
            self.allButtons(isEnabled: true)
            return
        }
        self.activityIndicator.startAnimating()
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            let filter = FilterModel.azInit(type: self.filterType, value: text, page: page)
            delegate.appData.scService.azSearch(model: filter) { result in
                switch result {
                case .success(let azResult):
                    if let data = azResult.data {
                        self.updadateKeyboard(with: data)
                    }
                case .failure(let error):
                    error.handleError(on: self)
                }
            }
            let find = FilterModel.searchStartSimpleInit(type: self.filterType, value: text, limit: 12, page: page)
            delegate.appData.scService.search(wtih: find) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let azResult):
                    self.model = azResult
                case .failure(let error):
                    error.handleError(on: self)
                }
                self.activityIndicator.stopAnimating()
            }
        }
    }
}

extension AZSearchController: MovieCollectionDelegate {
    func movieCollection(_ collection: MovieCollectionView, getNext page: Int) {
        
    }
    
    func movieCollection(_ collection: MovieCollectionView, didSelect movie: InfoData) {
        if let mediaType = movie.source?.infoLabels?.getMediaType() {
        
            if mediaType == .movie {
                self.movieCoordinator = MovieViewCoordinator(rootViewController: self, movie: movie)
            }
            else if mediaType == .tvshow,
                let tvSvc = storyboard!.instantiateViewController(withIdentifier: "TWShowViewController") as? TVShowViewController {
                tvSvc.tvShow = movie
                tvSvc.watched = SccWatchedData.getTvShowWatched(by: movie.id)
                self.present(tvSvc, animated: true, completion: nil)
            }
        }
    }
}

