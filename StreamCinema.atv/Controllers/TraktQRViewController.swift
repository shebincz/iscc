//
//  TraktQRViewController.swift
//  StreamCinema.atv
//
//  Created by SCC on 02/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import TraktKit

protocol TraktQRViewDelegate {
    func traktQRView(_ traktViewController:TraktQRViewController, isLogin success:Bool)
}

class TraktQRViewController: UIViewController {
    var traktDelegate:TraktQRViewDelegate?
    private var data: DeviceCode?
    private let codeLabel: UILabel = UILabel()
    private let imageView: UIImageView = UIImageView()
    private let progress: UIProgressView = UIProgressView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        self.start()
    }
    
    
    public func set(data: DeviceCode) {
        self.data = data
        self.imageView.image = data.getQRCode()
        self.codeLabel.numberOfLines = 0
        self.codeLabel.font = UIFont.systemFont(ofSize: 26, weight: .medium)
        self.codeLabel.text = String(format: "trakt_qr_text".localizable, data.verification_url, data.user_code)
    }
    
    private func configureView() {
        self.view.addSubview(self.imageView)
        self.view.addSubview(self.codeLabel)
        self.view.addSubview(self.progress)
        
        self.imageView.translatesAutoresizingMaskIntoConstraints = false
        self.codeLabel.translatesAutoresizingMaskIntoConstraints = false
        self.progress.translatesAutoresizingMaskIntoConstraints = false
        
        self.imageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.imageView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        self.imageView.heightAnchor.constraint(equalToConstant: 500).isActive = true
        self.imageView.widthAnchor.constraint(equalToConstant: 500).isActive = true
        
        self.codeLabel.centerXAnchor.constraint(equalTo: self.imageView.centerXAnchor).isActive = true
        self.codeLabel.topAnchor.constraint(equalTo: self.imageView.bottomAnchor , constant: 10).isActive = true
        self.codeLabel.widthAnchor.constraint(equalToConstant: 1000).isActive = true
        self.codeLabel.textAlignment = .center
        
        self.progress.centerXAnchor.constraint(equalTo: self.imageView.centerXAnchor).isActive = true
        self.progress.widthAnchor.constraint(equalToConstant: 500).isActive = true
        self.progress.topAnchor.constraint(equalTo: self.codeLabel.bottomAnchor, constant: 20).isActive = true
    }
    
    private func start() {
        guard let expireIn = self.data?.expires_in else { return }
        TraktManager.sharedManager.getTokenFromDevice(code: data) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success:
                self.traktDelegate?.traktQRView(self, isLogin: true)
                self.dismiss()
            case .fail(let progressValue):
                if progressValue != 0 {
                    let value: Float = Float(progressValue) / Float(expireIn)
                    self.updateProgress(value)
                } else {
                    self.traktDelegate?.traktQRView(self, isLogin: false)
                    self.dismiss()
                }
            }
        }
    }
    
    private func updateProgress(_ value:Float) {
        DispatchQueue.main.async {
            self.progress.progress = value
        }
    }
    
    private func dismiss() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
