//
//  MovieDetailCell.swift
//  MovieDBTV
//
//  Created by Alfian Losari on 23/03/19.
//  Copyright © 2019 Alfian Losari. All rights reserved.
//

import UIKit

class MovieDetailCell: UITableViewCell {

    @IBOutlet weak var taglineLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var adultLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var castLabel: UILabel!
    @IBOutlet weak var crewLabel: UILabel!
    
    public static var nib: UINib {
        return UINib(nibName: "MovieDetailCell", bundle: Bundle(for: MovieDetailCell.self))
    }
    
    public static let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY"
        return dateFormatter
    }()
    
    public var info: InfoData? {
        didSet {
            guard let info = info else { return }
            
            let infoLabel = info.source?.getInfoLabels()
            
            self.taglineLabel.text = infoLabel?.title
            self.overviewLabel.text = infoLabel?.plot
            if let year = info.source?.infoLabels?.year {
                self.yearLabel.text = "\(year)"
            } else { self.yearLabel.text = nil }
            
            if let rating = info.source?.getRating() {
                ratingLabel.isHidden = false
                ratingLabel.text = rating
            } else {
                ratingLabel.isHidden = true
            }
            
            adultLabel.isHidden = !(info.source?.adult ?? false)

            if let duration = info.source?.infoLabels?.duration {
                durationLabel.text = "\(duration) mins"
            } else if let duration = info.source?.streamInfo?.video?.duration {
                durationLabel.text = TimeInterval(duration).minutesString()
            } else { durationLabel.text = nil }
            
            if let genres = info.source?.infoLabels?.genre, genres.count > 0 {
                genreLabel.isHidden = false
                genreLabel.text = genres.map { $0 }.joined(separator: ", ")
            } else {
                genreLabel.isHidden = true
            }
            
            self.castLabel.isHidden = true
            
            if let director = info.source?.infoLabels?.director {
                crewLabel.isHidden = false
                let dir = director.map { $0 }.joined(separator: ", ")
                let text = self.getText(title: "Director:", info: dir)
                crewLabel.attributedText = text
                crewLabel.numberOfLines = 0
            } else {
                crewLabel.isHidden = true
            }
        }
    }
    
    private func getText(title:String, info: String) -> NSAttributedString {
        let text = NSMutableAttributedString(string: "\(title)\n")
        let range = NSRange(location: 0, length: text.length)
        text.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray , range: range)
        let cast = NSMutableAttributedString(string: info)
        let castRange = NSRange(location: 0, length: cast.length)
        cast.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.gray , range: castRange)
        text.append(cast)
        
        return text
    }
}
