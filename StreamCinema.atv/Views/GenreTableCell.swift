//
//  GenreTableCell.swift
//  StreamCinema.atv
//
//  Created by SCC on 22/11/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

protocol GenreTableCellDelegate: class {
    func genreTableCell(_ cell:GenreTableCell, didSelect movie:InfoData)
    func genreTableCell(_ cell:GenreTableCell, dataFor genre:Genere, page: Int)
}

final class GenreTableCell: UITableViewCell {
    @IBOutlet weak var sectionTitleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            self.configure()
        }
    }
    public weak var delegate:GenreTableCellDelegate?
    private(set) var genre: Genere?
    private var model: FilterResult? {
        didSet {
            self.collectionView?.reloadData()
        }
    }
    private var filterType: FilterType = .movie
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func configure() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.alwaysBounceHorizontal = true
        self.collectionView.register(UINib(nibName: "MovieCell", bundle: nil),
                                     forCellWithReuseIdentifier: "MovieCell")
        self.collectionView.backgroundColor = .clear
    }
    
    public func cell(for genre: Genere, type: FilterType, model: FilterResult?) {
        self.model = model
        self.filterType = type
        self.genre = genre
        self.sectionTitleLabel.text = genre.string
        self.fetchData(for: genre)
    }
    
    public func update(model: FilterResult?) {
        self.model = model
    }
    
    private func fetchData(for genre:Genere, page: Int = 1) {
        self.delegate?.genreTableCell(self, dataFor: genre, page: page)
    }
}

extension GenreTableCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = self.model?.data?.count {
            return count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as? MovieCell {
            if let data = self.model?.data?[indexPath.item] {
                cell.configure(data, watched: SccWatchedData.getWatched(by: data.id))
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let count = self.model?.data?.count, count != 0, (count - 1) == indexPath.row,
           let current = self.model?.pagination?.page,
           let pageCount = self.model?.pagination?.pageCount,
           current < pageCount,
           let genre = self.genre {
            self.fetchData(for: genre, page: current + 1)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let data = self.model?.data,
           data.count > indexPath.row {
            self.delegate?.genreTableCell(self, didSelect: data[indexPath.row])
        }
    }
}
