//
//  MovieCell.swift
//  MovieDBTV
//
//  Created by Alfian Losari on 23/03/19.
//  Copyright © 2019 Alfian Losari. All rights reserved.
//

import UIKit
import Kingfisher

final class MovieCell: UICollectionViewCell {
    
    @IBOutlet var watchedImage: UIImageView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var ratingLabel: UILabel!
    
    @IBOutlet var unfocusedConstraint: NSLayoutConstraint!
    var focusedConstraint: NSLayoutConstraint!
    
    public var data:InfoData?
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        
        return formatter
    }()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        focusedConstraint = titleLabel.topAnchor.constraint(equalTo: imageView.focusedFrameGuide.bottomAnchor, constant: 16)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        focusedConstraint?.isActive = isFocused
        unfocusedConstraint?.isActive = !isFocused
    }
    
    private func isWatched(_ isWatched: Bool) {
        self.watchedImage.isHidden = !isWatched
        self.watchedImage.layer.shadowColor = UIColor.darkGray.cgColor
        self.watchedImage.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.watchedImage.layer.shadowOpacity = 1
        self.watchedImage.layer.shadowRadius = 1.0
        self.watchedImage.clipsToBounds = false
    }
    
    func configure(_ movie: InfoData, watched: SccWatched?) {
        self.data = movie
        if let movieInfo = movie.source?.getInfoLabels() {
            imageView.kf.indicatorType = .none
            if let poster = movieInfo.art?.getPoster(),
                let url = URL(string: poster) {
                self.imageView.setCashedImage(url: url, type: .poster)
            } else {
                self.imageView.image = UIImage(named: "poster")
            }
            
            let string:NSMutableAttributedString = NSMutableAttributedString(string: (movieInfo.title ?? "") + " ")

            ratingLabel.textColor = .secondaryLabel
            if let rating = movie.source?.getRating() {
                ratingLabel.text = rating
                ratingLabel.isHidden = false
            } else {
                ratingLabel.isHidden = true
            }
            
            if let streams = movie.source?.availableStreams {
                let audios = streams.getAudio()
                let audioString = NSMutableAttributedString(string: audios.map({$0.name}).joined(separator: ", "))
                let range = NSRange(location: 0, length: audioString.length)
                audioString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: range)
                
                string.append(NSAttributedString(string: "\n"))
                string.append(audioString)
            }
            
            titleLabel.attributedText = string
        }
        if watched == nil {
            self.isWatched(false)
        } else {
            self.isWatched(true)
        }
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        super.didUpdateFocus(in: context, with: coordinator)
        
        setNeedsUpdateConstraints()
        coordinator.addCoordinatedAnimations({
            self.layoutIfNeeded()
        }, completion: nil)
    }    
}
