//
//  GenreTableView.swift
//  StreamCinema.atv
//
//  Created by SCC on 22/11/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

protocol GenreViewModelDelegate: class {
    func genreViewModel(_ viewModel:GenreViewModel, didSelect movie:InfoData)
}

final class GenreViewModel {
    private var model:[Genere] {
        get {
            if self.type == .movie {
                return Genere.allMovie
            } else {
                return Genere.allTvShows
            }
        }
    }
    private var modelData: [Genere:FilterResult] = [:]
    
    public var type: FilterType = .movie
    public weak var genreDelegate:GenreViewModelDelegate?
    public func getNumberOfRows(in section:Int) -> Int {
        return self.model.count
    }
    
    public func getCell(forRowAt indexPath:IndexPath, in tableView:UITableView) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "GenreTableCell") as? GenreTableCell {
            let genre = self.model[indexPath.row]
            let result = self.modelData[genre]
            cell.delegate = self
            cell.cell(for:genre, type: self.type ,model: result)
            cell.selectionStyle = .none
            cell.focusStyle = .custom
            return cell
        }
        return UITableViewCell()
    }
    
    private func fetchData(for genre:Genere, page: Int, cell: GenreTableCell) {
        if let pagination = self.modelData[genre]?.pagination, pagination.page >= page {
            return
        }
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else { return }
        delegate.appData.scService.genre(with: genre, type: self.type, page: page) { result in
            switch result {
            case .success(let model):
                self.update(data: model, genre: genre, page: page, cell: cell)
            case .failure(_):
                break
            }
        }
    }
    
    private func update(data:FilterResult, genre:Genere, page: Int, cell: GenreTableCell) {
        if page == 1 {
            self.modelData[genre] = data
        } else {
            if let movieData = data.data,
               let pagination = data.pagination {
                self.modelData[genre]?.data?.append(contentsOf: movieData)
                self.modelData[genre]?.pagination = pagination
            }
        }
        if cell.genre == genre {
            cell.update(model: self.modelData[genre])
        }
        cell.collectionView.reloadData()
    }
}
extension GenreViewModel: GenreTableCellDelegate {
    func genreTableCell(_ cell: GenreTableCell, dataFor genre: Genere, page: Int) {
        self.fetchData(for: genre, page: page, cell: cell)
    }
    
    func genreTableCell(_ cell: GenreTableCell, didSelect movie: InfoData) {
        self.genreDelegate?.genreViewModel(self, didSelect: movie)
    }
}

protocol GenreTableDelegate: class {
    func castCollectionView(_ castView:GenreTableView, didSelect movie:InfoData)
}

final class GenreTableView: UITableView {
    private var model:GenreViewModel = GenreViewModel()
    public weak var genreDelegate:GenreTableDelegate?
    public var type: FilterType = .movie {
        didSet {
            self.model.type = self.type
        }
    }
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        self.configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        self.model.genreDelegate = self
        self.delegate = self
        self.dataSource = self
        self.register(UINib(nibName: "GenreTableCell", bundle: nil), forCellReuseIdentifier: "GenreTableCell")
        self.rowHeight = UITableView.automaticDimension
    }
}

extension GenreTableView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 640
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.getNumberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.model.getCell(forRowAt: indexPath, in: tableView)
    }
    
    func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}

extension GenreTableView: GenreViewModelDelegate {
    func genreViewModel(_ viewModel: GenreViewModel, didSelect movie: InfoData) {
        self.genreDelegate?.castCollectionView(self, didSelect: movie)
    }
}
